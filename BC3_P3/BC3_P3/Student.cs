﻿// Yevgeni Kamenski
// P3.cs
// 9-8-2015

using System;

namespace BC3_P3 {
    
    /// <summary>Holds student information</summary>
    class Student: IComparable<Student>
    {
        /// <summary>Student ID. </summary>
        public int ID { get; set; }
        
        /// <summary>Student GPA. </summary>
        public double GPA { get; set; }

        /// <summary>IComparable interface implementation. </summary>
        int IComparable<Student>.CompareTo(Student other)
        {
            return GPA.CompareTo(other.GPA);
        }

        /// <summary>Student string representation. </summary>
        public override string ToString()
        {
            return string.Format("Student: ID={0}, GPA={1:F2}", ID, GPA);
        }
    }
}
