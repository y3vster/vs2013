﻿// Yevgeni Kamenski
// P3.cs
// 9-8-2015

using System;

using System.Collections.Generic;
using System.IO;

namespace BC3_P3 {
    class P3 {
        private const string SAMPLE_INPUT = "SampleInput.txt";

        static void Main(string[] args)
        {
            if(File.Exists(SAMPLE_INPUT)) {
                TestHeap(false);
                TestHeap(true);
            } else {
                Console.WriteLine("File not found: " + Directory.GetCurrentDirectory() + @"\" + SAMPLE_INPUT);
            }
            Console.WriteLine("\nPress any key to exit..");
            Console.ReadKey(true);
        }

        static void TestHeap(bool isMinHeap)
        {
            Console.WriteLine("\n" + new string('-', 50));

            Heap<Student> hp = new Heap<Student>(isMinHeap);
            Console.WriteLine("Created new Heap<Student>(isMinHeap = {0})", isMinHeap);
            Console.WriteLine("Heap.IsEmpty() = " + hp.IsEmpty());

            String[] records = File.ReadAllLines(SAMPLE_INPUT);
            for(int i = 0; i < records.Length; i++) {
                String[] entries = records[i].Split(',');
                if(entries.Length == 2) {
                    int id = int.Parse(entries[0].Trim());
                    double gpa = double.Parse(entries[1].Trim());
                    hp.Insert(new Student { GPA = gpa, ID = id });
                }
            }

            Console.WriteLine("\nInserted entries from a file: ");
            hp.Print();

            Console.WriteLine("\nHeap.IsEmpty() = " + hp.IsEmpty());

            Console.WriteLine("\nRemoving a few items from the root:");
            Queue<Student> queue = new Queue<Student>();
            for(int i = 0; i < 4; i++) {
                Student removed = hp.GetRoot();
                hp.RemoveRoot();
                Console.WriteLine(removed);
                queue.Enqueue(removed);
            }

            Console.WriteLine("\nUpdated heap contents:");
            hp.Print();

            Console.WriteLine("\nReinserting removed items..");
            while(queue.Count > 0)
                hp.Insert(queue.Dequeue());

            Console.WriteLine("Performing heapsort. Results: ");
            hp.HeapSort();
            hp.Print();
        }
    }
}
