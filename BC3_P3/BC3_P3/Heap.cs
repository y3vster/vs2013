﻿// Yevgeni Kamenski
// P3.cs
// 9-8-2015

using System;
using System.Collections.Generic;

namespace BC3_P3
{
    //heap implementation
    class Heap<T> where T : IComparable<T>
    {
        // constructors

        //default ctor.
        public Heap() : this(false) { }

        //ctor that specifies type of heap
        public Heap(bool isMinHeap)
        {
            this.isMinHeap = isMinHeap;
            theHeap = new List<T>();
        }

        // public functions

        // return true if empty, otherwise false
        public bool IsEmpty()
        {
            return theHeap.Count == 0;
        }

        //insert an item into the heap
        public void Insert(T item)
        {
            theHeap.Add(item);
            PercolateUp(theHeap.Count - 1);
        }

        //returns root item
        public T GetRoot()
        {
            return theHeap[0];
        }

        //remove the root (max/min) element from the heap
        public void RemoveRoot()
        {
            Exchange(0, theHeap.Count - 1);
            theHeap.RemoveAt(theHeap.Count - 1);
            PercolateDown(0);
        }

        //print contents
        public void Print()
        {
            for (int i = 0; i < theHeap.Count; i++)
                Console.WriteLine("{0,2}. {1}", i + 1, theHeap[i]);
        }

        //perform a heapsort
        public void HeapSort()
        {
            for (int i = theHeap.Count - 1; i > 0; i--)
            {
                Exchange(0, i);
                PercolateDown(0, i);
            }
            //reverse
            for (int i = 0; i < theHeap.Count / 2; i++)
                Exchange(i, theHeap.Count - i - 1);

            ////another way:
            //List<T> sortedList = new List<T>();
            //while (theHeap.Count > 0)
            //{
            //    sortedList.Add(GetRoot());
            //    RemoveRoot();
            //}
            //theHeap = sortedList;

        }

        // private data members/functions

        private List<T> theHeap;    //The heap.

        private bool isMinHeap;     //flag to indicate if it's a min heap

        //returns true if heap property is violated
        private bool HeapPropViol(int upPos, int dnPos)
        {
            bool result = theHeap[dnPos].CompareTo(theHeap[upPos]) > 0;
            if (isMinHeap) return !result;
            return result;
        }

        //exchange elements
        private void Exchange(int p1, int p2)
        {
            T temp = theHeap[p1];
            theHeap[p1] = theHeap[p2];
            theHeap[p2] = temp;
        }

        //percolates an element up
        private void PercolateUp(int pos)
        {
            if (pos == 0) return;
            int prt = (pos - 1) / 2;  //parent
            if (HeapPropViol(prt, pos)) Exchange(prt, pos);
            PercolateUp(prt);
        }

        //percolates an element down through all elements
        private void PercolateDown(int pos)
        {
            PercolateDown(pos, theHeap.Count);
        }

        //percolates an element down to the max specified position
        private void PercolateDown(int pos, int size)
        {
            int cldPos = 2 * pos + 1;   //child position
            if (cldPos >= size) return;

            if ((cldPos + 1 < size) &&
               (HeapPropViol(cldPos, cldPos + 1)))
                cldPos++;

            if (HeapPropViol(pos, cldPos))
                Exchange(pos, cldPos);

            PercolateDown(cldPos, size);
        }
    }
}
