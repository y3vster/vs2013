﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PokerCards {
    /// <summary>
    /// Helper for managing colors and output to console.
    /// </summary>
    class UIConsole {
        const ConsoleColor CONS_MAIN_BACK = ConsoleColor.White;
        const ConsoleColor CONS_MAIN_FORE = ConsoleColor.Black;
        const ConsoleColor CONS_MAIN_RED = ConsoleColor.Red;

        const int BUF_W = 89;  //console window buffer size
        const int BUF_H = 70;

        const int WIND_W = 89; //user's window size in chars
        const int WIND_H = 70; //will be clipped to the max allowed

        private static readonly string[] RedTokens = "♦♥".Select(c => c.ToString()).ToArray();
        private static readonly string RegExTokenMatch = "(" + string.Join("|", RedTokens) + ")";

        /// <summary>Console.Write(s) at a location (t, l)</summary>
        public static void OutAt<T>(int t, int l, T s)
        {
            Console.SetCursorPosition(l, t);
            Console.Write(s);
        }

        static void ClearLine()
        {
            int linePos = Console.CursorTop;
            string blankLine = new string(' ', Console.BufferWidth);
            SetBlk();
            OutAt(linePos, 0, blankLine);
            Console.SetCursorPosition(0, linePos);
        }

        public static void GoUpOneLine()
        {
            ClearLine();
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            ClearLine();
        }

        public static void WriteColor<T>(T userStr)
        {
            string[] tokens = Regex.Split(userStr.ToString(), RegExTokenMatch);

            var tokensWithClrInfo =
                from t in tokens
                select new[] {
                    RedTokens.Contains(t) ? "Red" : "Blk",
                    t
                };

            foreach(var token in tokensWithClrInfo) {
                if(token[0] == "Red")
                {
                    Console.ForegroundColor = CONS_MAIN_RED;
                }
                else {
                    Console.ForegroundColor = CONS_MAIN_FORE;
                }
                Console.Out.WriteAsync(token[1]);
            }
        }

        static void SetBlk()
        {
            Console.BackgroundColor = CONS_MAIN_BACK;
            Console.ForegroundColor = CONS_MAIN_FORE;
        }

        static void SetRed()
        {
            Console.BackgroundColor = CONS_MAIN_BACK;
            Console.ForegroundColor = CONS_MAIN_RED;
        }


        public static void SetConsoleBufferHeight(int numLines)
        {
            if (numLines < BUF_H)
            {
                numLines = BUF_H;
            }else if (numLines > Int16.MaxValue - 1)
            {
                numLines = Int16.MaxValue - 1;
            }
            Console.SetBufferSize(BUF_W, numLines);
        }

        public static void InitConsoleWindow()
        {
            SetBlk();
            Console.Clear();
            try {
                Console.SetWindowSize(1, 1);
                Console.SetBufferSize(BUF_W, BUF_H);
                int w = (Console.LargestWindowWidth < WIND_W) ? Console.LargestWindowWidth : WIND_W;
                int h = (Console.LargestWindowHeight < WIND_H) ? Console.LargestWindowHeight : WIND_H;
                Console.SetWindowSize(w, h);
                Console.OutputEncoding = Encoding.Unicode;
                Console.Clear();
            } catch(Exception e) {
                Debug.WriteLine("Error initializing console window size: " + e);
            }
        }
    }
}
