﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PokerCards {

    class Hand : IComparable<Hand>, IEquatable<Hand>
    {
        private IList<Card> _cards;
        private HandEvaluation _evaluation;
        public string EvaluationStr { get { return _evaluation.Text; } }
        public string HandStr { get { return string.Join(" ", _cards.Select(c => c.ToString().PadLeft(3))); } }
        public Hand(IEnumerable<Card> cardsEnum)
        {
            InitSortedListAndEvaluation(cardsEnum);
        }

        /// <summary>
        /// For debugging. Ex: "10D JD QD 7C 7S"
        /// </summary>
        /// <param name="handRepr"></param>
        public Hand(String handRepr)
        {
            var cardsEnum =
                from cStr in handRepr.Split()
                select CardDisp.StringToNewCard(cStr);
            InitSortedListAndEvaluation(cardsEnum);
        }

        private void InitSortedListAndEvaluation(IEnumerable<Card> cardsEnum)
        {
            _evaluation = new HandEvaluation(cardsEnum);
            _cards = _evaluation.SortedCards;
        }

        public override string ToString()
        {
            return HandStr + "  : " + EvaluationStr;
        }

        public int CompareTo(Hand other)
        {
            return _evaluation.CompareTo(other._evaluation);
        }

        public bool Equals(Hand other)
        {
            return _evaluation.CompareTo(other._evaluation) == 0;
        }
    }
}
