﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PokerCards {

    internal class MainMethod {
        private static void Main(string[] args)
        {
            UIConsole.InitConsoleWindow();

            //ShuffledDecks();
            RandomCardHands();

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);
        }

        private static void ShuffledDecks()
        {
            Console.Write("How many decks? (limit to <2978 due to console window display) ");
            int numDecks = int.Parse(Console.ReadLine());

            //int numDecks = 2978;
            UIConsole.SetConsoleBufferHeight(11 * numDecks);

            Deck deck = new Deck(numDecks);
            //UIConsole.WriteColor("Deck(s): \n" + deck + "\n\n");

            var strFlsh = new Hand("3S 2S 5S 4S 6S"); //debugging
            var regFlsh = new Hand("3D 7S 5S 4S 6S"); //debugging
            var fourOfAKind = new Hand("10S 10C 10D 10H 2C"); //debugging

            Console.WriteLine("Dealing hands and evaluating..\n\n");
            Dealer dealer = new Dealer(deck);

            List<Hand> handsList = new List<Hand>();
            //handsList.Add(strFlsh);     //debugging
            //handsList.Add(regFlsh);     //debugging
            //handsList.Add(fourOfAKind); //debugging
            handsList.AddRange(dealer.DealUntilDeckEmpty(5));
            handsList.Sort();
            handsList.Reverse();

            //used to check if we have a tie
            Hand previousHand = handsList.Last();
            StringBuilder sb = new StringBuilder();

            foreach(Hand curHand in handsList) {
                //UIConsole.WriteColor(
                sb.Append(
                    curHand.ToString() +
                    (curHand.Equals(previousHand) ? " *TIE*\n" : "\n"));
                previousHand = curHand;
            }
            //Console.Out.WriteAsync(sb.ToString());
            UIConsole.WriteColor(sb.ToString());
        }

        private static void RandomCardHands()
        {
            Console.Write("How many random hands would you like? (Display is limited to 32767 lines) ");
            int numHands = int.Parse(Console.ReadLine());

            //int numHands = 100000;
            UIConsole.SetConsoleBufferHeight(numHands + 10);

            Console.WriteLine("Dealing hands and evaluating..\n");

            List<Hand> handsList = new List<Hand>(Dealer.DealRandomHands(numHands));
            handsList.Sort();
            handsList.Reverse();

            //used to check if we have a tie
            Hand previousHand = handsList.Last();
            StringBuilder sb = new StringBuilder();

            int maxLines = Int16.MaxValue - 5;

            foreach(Hand curHand in handsList) {
                if(--maxLines == 0) break;

                //UIConsole.WriteColor(
                sb.Append(
                    curHand.ToString() +
                    (curHand.Equals(previousHand) ? " *TIE*\n" : "\n"));
                previousHand = curHand;
            }
            //Console.Out.WriteAsync(sb.ToString());
            UIConsole.WriteColor(sb.ToString());
            
        }

    }
}
