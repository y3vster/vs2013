using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards_28July2015
{
    class Card
    {
        public enum Rank { Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
        public enum Suit { Club, Diamond, Heart, Spade };

        private Rank rank;
        private Suit suit;

        public Card(Rank r, Suit s)
        {
            rank = r;
            suit = s;
        }

        public override String ToString()
        {
            return rank + ":" + suit;
        }
    }
}
