using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards_28July2015 {
    class Deck {
        private const int ONE_LINE_NUM_CARDS = 13;
        private int topCardIndex;
        private Card[] cards;
        public enum EnumSortAlg {
            EnumRegularAlgorithm,
            EnumFunctionalWay
        }


        public Deck() {
            topCardIndex = 0;
            int cardIndex = 0;

            cards = new Card[52];

            for (Card.Suit s = Card.Suit.Club; s <= Card.Suit.Spade; s++) {
                for (Card.Rank r = Card.Rank.Two; r <= Card.Rank.Ace; r++) {
                    cards[cardIndex++] = new Card(r, s);
                }
            }
        }

        /// <summary>Returns a card from a deck</summary>
        public Card DealCard() {
            Card retVal = cards[topCardIndex];
            topCardIndex++;
            return retVal;
        }

        public void Shuffle() {
            Shuffle_FisherYates();
        }

        public void ShuffleDiffAlorithm() {
            Shuffle_RandomSort();
        }

        public override String ToString() {
            int curColumn = 0;      //keep track of the current column
            string outStr = "";     //output string buffer

            foreach (Card curCard in cards) {
                outStr += curCard.ToString();                   //concat current card string to output
                curColumn = (++curColumn % ONE_LINE_NUM_CARDS); //advance to next col w/mod counter
                if (curColumn == 0) {
                    outStr += "\n";     //insert \n if on column 0 again
                } else {
                    outStr += " ";      //otherwise insert a space between cards
                }
            }

            return outStr;
        }

        //algorithm shown in class
        private void Shuffle_FisherYates() {
            Random rm = new Random();
            int arrLen = cards.Length;

            for (int i = 0; i < arrLen; i++) {
                SwapArrElements(i, rm.Next(i, arrLen));
            }
        }

        ///helper for Shuffle_FisherYates()
        private void SwapArrElements(int index1, int index2) {
            Card temp = cards[index1];
            cards[index1] = cards[index2];
            cards[index2] = temp;
        }

        ///my favorite way:
        ///each card is assigned a random number
        ///deck is sorted by this number
        private void Shuffle_RandomSort() {
            Random rm = new Random();
            cards = cards.OrderBy(e => rm.Next()).ToArray();
        }
    }
}
