﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PokerCards {

    class Dealer {
        private readonly Deck _deckInst;
        public Dealer(Deck d) { _deckInst = d; }
        private static Random _rm = new Random();
        private static Rank[] _ranks = (Rank[])Enum.GetValues(typeof(Rank));
        private static Suit[] _suits = (Suit[])Enum.GetValues(typeof(Suit));
        private static HashSet<Card> randCardsHash = new HashSet<Card>(); 

        /// <summary>Return a hand or null if out of cards in the deck</summary>
        private Hand DealSingleHand(int numCards)
        {
            bool deckHasEnoughCards;
            var cards = _deckInst.TakeCards(numCards, out deckHasEnoughCards);

            if(deckHasEnoughCards) {
                return new Hand(cards);
            }
            else {
                return null;
                //throw new ArgumentOutOfRangeException("numCards");
            }
        }

        public IEnumerable<Hand> DealUntilDeckEmpty(int numCardsEaDeal)
        {
            Hand returnHand;
            while((returnHand = DealSingleHand(numCardsEaDeal)) != null) {
                yield return returnHand;

            }
        }

        private static Card RandCard()
        {
            return new Card
            {
                R = _ranks[_rm.Next(0, 13)], 
                S = _suits[_rm.Next(0, 4)]
            };
        }

        /// <summary>Does not allow card repeats in the hand. </summary>
        private static Hand DealRandomHand()
        {
            randCardsHash.Clear();
            for (int i = 0; i < 5; i++)
            {
                while (!randCardsHash.Add(RandCard())) { }   //hash add returns false if duplicate, so loop again
            }
            return new Hand(randCardsHash.ToList()); 
        }

        public static IEnumerable<Hand> DealRandomHands(int numHands)
        {
            for (int i = 0; i < numHands; i++)
            {
                yield return DealRandomHand();
            }
        } 


    }
}
