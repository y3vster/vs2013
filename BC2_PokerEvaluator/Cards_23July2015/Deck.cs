using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerCards {
    class Deck {

        private List<Card> _cards;
        private static Rank[] _ranks = (Rank[]) Enum.GetValues(typeof (Rank));
        private static Suit[] _suits = (Suit[]) Enum.GetValues(typeof (Suit));

        public Deck(int numberOfDecks = 1, bool shuffled = true)
        {
            List<Card> newCards = new List<Card>(GetNewDeck());
            for (int i = 1; i < numberOfDecks; i++)
            {
                newCards.AddRange(GetNewDeck());
            }

            _cards = newCards.ToList();
            if(shuffled) { DeckShuffle(); }
        }

        private IEnumerable<Card> GetNewDeck()
        {
            return 
                from r in _ranks
                from s in _suits
                select new Card { R = r, S = s };
        }

        public IList<Card> TakeCards(int takeNum, out bool succeeded)
        {
            List<Card> outCards = new List<Card>();

            bool haveEnoughCards = (_cards.Count() >= takeNum);
            if(haveEnoughCards)
            {
                outCards.AddRange(_cards.Take(takeNum));
                _cards.RemoveRange(0,takeNum);
            }

            succeeded = haveEnoughCards;
            return outCards;
        }


        public override String ToString()
        {
            const int lineLen = 20;
            int i = 0;
            var lines =
                from c in _cards
                let str = c.ToString().PadLeft(3)
                group str by i++ / lineLen into line
                select line.Aggregate((lineStr, e) => lineStr + " " + e);

            return string.Join("\n", lines);
        }

        private void DeckShuffle()
        {
            Random rm = new Random();
            _cards = _cards.OrderBy(e => rm.Next()).ToList();
        }
    }
}
