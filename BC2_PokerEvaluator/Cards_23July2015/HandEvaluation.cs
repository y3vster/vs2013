﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PokerCards {
    [DebuggerDisplay("{_DebugDisp,nq}")]
    class HandEvaluation : IComparable<HandEvaluation> {

        /// <summary>Text of the evaluation based on the highest granularity used when sorting</summary>
        public string Text { get { return _evalRecs[IndexDeepestCompared].Text; } }
        /// <summary>Returns cards sorted based on the type of hand. E.g. a Full house should have a different grouping than a straight.</summary>
        public IList<Card> SortedCards { get { return _sortedCardsFromEval; } }

        [DebuggerDisplay("{Value}: {Text}")]
        private struct EvalRec { public int Value; public string Text; }
        private readonly List<EvalRec> _evalRecs = new List<EvalRec>();

        private int _deepestIndexCompared;
        private int IndexDeepestCompared
        {
            get { return _deepestIndexCompared; }
            set { if(value > _deepestIndexCompared) { _deepestIndexCompared = value; } }
        }

        private readonly IList<Card> _sortedCardsFromEval;
        private string _DebugDisp { get { return Text + "(" + String.Join(" ", _sortedCardsFromEval) + ")"; } }

        /// <summary>Ctor</summary>
        public HandEvaluation(IEnumerable<Card> cards)
        {
            _sortedCardsFromEval =  Evaluate(cards);
        }

        /// <summary>Helper for clarity</summary>
        private void PushEval(int val, string text)
        {
            _evalRecs.Add(new EvalRec { Value = val, Text = text });
        }

        private IList<Card> Evaluate(IEnumerable<Card> cardSeq)
        {
            //to avoid multiple enumerations
            IEnumerable<Card> cardSeqList = cardSeq as IList<Card> ?? cardSeq.ToList();

            //order cards by rank, then by suit
            var orderingStraight = cardSeqList.OrderBy(c => -(int)c.R * 15 - (int)c.S).ToList();

            var rankDiffs = orderingStraight.Skip(1).Select(
                (c, i) => (int)(orderingStraight[i].R - c.R));

            bool haveStraight = rankDiffs.Aggregate(true, (b, i) => b & (i == 1));
            int straightMaxRank = (int)orderingStraight[0].R;

            var suitsGroup =
                from c in cardSeqList
                group c by c.S into grp
                orderby grp.Key ascending
                select grp;
            bool allSameSuits = (suitsGroup.First().Count() == 5);

            var ranksGroup =
                from c in cardSeqList
                group c by c.R into grp
                let rnk = grp.Key
                let cnt = grp.Count()
                orderby cnt descending, rnk descending
                select new {
                    count = cnt,
                    rank = (int)rnk,
                    subGrp = grp
                };
            var ranksGroupList = ranksGroup.ToList();

            var orderingGroupRankSuit =
                from g in ranksGroupList
                from c in g.subGrp
                orderby g.count descending, g.rank descending, c.S descending 
                select c;
            

            bool hasFourOfAKind = ranksGroupList.Any(e => e.count >= 4);
            bool hasThreeOfAKind = ranksGroupList.Any(e => e.count == 3);
            int numPairs = ranksGroupList.Count(e => e.count == 2);

            //Used when we have 4OAK, full house, etc..
            bool useGroupedOrdering = false;

            //main eval loop
            if(haveStraight && allSameSuits) {
                PushEval(10, "Straight Flush.");
                PushEval(
                    val: straightMaxRank,
                    text: string.Format("Straight Flush, {0} high.", EnumStrings.RankStrings[straightMaxRank]));
            }
            else if(hasFourOfAKind) {
                useGroupedOrdering = true;
                PushEval(9, "Four of a kind.");

                int handRank = (int)ranksGroupList[0].rank;
                PushEval(
                    val: handRank,
                    text: String.Format("Four of a kind, {0}s.", EnumStrings.RankStrings[handRank]));

                int kickerRank = (int)((ranksGroupList.Count() == 2) ? ranksGroupList[1].rank : handRank);
                PushEval(
                    val: kickerRank,
                    text: String.Format(
                        "Four of a kind, {0}s, {1} kicker.",
                        EnumStrings.RankStrings[handRank],
                        EnumStrings.RankStrings[kickerRank]));
            }
            else if(hasThreeOfAKind && numPairs == 1) {
                useGroupedOrdering = true;
                PushEval(8, "Full House.");

                int rankOfTriple = (int)ranksGroupList.Find(e => e.count == 3).rank;
                int rankOfPair = (int)ranksGroupList.Find(e => e.count == 2).rank;

                PushEval(
                    val: rankOfTriple,
                    text: string.Format("Full House, {0}s", EnumStrings.RankStrings[rankOfTriple]));

                PushEval(
                    val: rankOfPair,
                    text: string.Format(
                        "Full House, {0}s and {1}s.",
                        EnumStrings.RankStrings[rankOfTriple], EnumStrings.RankStrings[rankOfPair]));
            }
            else if(allSameSuits) {
                useGroupedOrdering = true;
                PushEval(7, "Flush.");

                List<string> rankStrings = new List<string>();
                foreach(int cardRank in orderingStraight.Select(c => (int)c.R)) {
                    rankStrings.Add(EnumStrings.RankStrings[cardRank]);
                    PushEval(
                        val: cardRank,
                        text: string.Format(
                            "Flush with high card{0}: {1}.",
                            (rankStrings.Count > 1) ? "s" : "",
                            string.Join(", ", rankStrings)));
                }
            }
            else if(haveStraight) {
                PushEval(6, "Straight.");
                PushEval(
                    val: straightMaxRank,
                    text: string.Format("Straight, {0} high.", EnumStrings.RankStrings[straightMaxRank]));
            }
            else if(hasThreeOfAKind)
            {
                useGroupedOrdering = true;
                PushEval(5, "Three of a kind.");

                int rankOfTriple = ranksGroupList[0].rank;
                PushEval(
                    val: rankOfTriple,
                    text: string.Format("Three of a kind, {0}s.", EnumStrings.RankStrings[rankOfTriple]));

                int rankKicker = ranksGroupList[1].rank;
                PushEval(
                    val: rankKicker,
                    text: string.Format(
                        "Three of a kind, {0}s with {1} kicker.",
                        EnumStrings.RankStrings[rankOfTriple], EnumStrings.RankStrings[rankKicker]));

                int rankKickerSecond = ranksGroupList[2].rank;
                PushEval(
                    val: rankKickerSecond,
                    text: string.Format(
                        "Three of a kind, {0}s with {1} & {2} kickers.",
                        EnumStrings.RankStrings[rankOfTriple], EnumStrings.RankStrings[rankKicker], EnumStrings.RankStrings[rankKickerSecond]));
            }
            else if(numPairs == 2) {
                useGroupedOrdering = true;
                PushEval(4, "Two pair.");

                int rankPairHigh = ranksGroupList[0].rank;
                PushEval(
                    val: rankPairHigh,
                    text: string.Format(
                        "Two pair, {0} high.",
                        EnumStrings.RankStrings[rankPairHigh]));

                int rankPairLow = ranksGroupList[1].rank;
                PushEval(
                    val: rankPairLow,
                    text: string.Format(
                        "Two pair, {0}s & {1}s.",
                        EnumStrings.RankStrings[rankPairHigh], EnumStrings.RankStrings[rankPairLow]));

                int rankKicker = ranksGroupList[2].rank;
                PushEval(
                    val: rankKicker,
                    text: string.Format(
                        "Two pair, {0}s & {1}s with {2} kicker.",
                        EnumStrings.RankStrings[rankPairHigh], EnumStrings.RankStrings[rankPairLow], EnumStrings.RankStrings[rankKicker]));

            }
            else if(numPairs == 1) {
                useGroupedOrdering = true;
                PushEval(3, "Pair.");

                int rankPair = ranksGroupList[0].rank;
                PushEval(
                    val: rankPair,
                    text: string.Format(
                        "Pair of {0}s.", EnumStrings.RankStrings[rankPair]));

                List<string> rankStrings = new List<string>();
                var highCardsAfterPair = ranksGroupList.Skip(1).Select(e => (int)e.rank);
                foreach(int cardRank in highCardsAfterPair) {
                    rankStrings.Add(EnumStrings.RankStrings[cardRank]);
                    PushEval(
                        val: cardRank,
                        text: string.Format(
                            "Pair of {0}s with {1} kicker{2}.",
                            EnumStrings.RankStrings[rankPair],
                            string.Join(", ", rankStrings),
                            (rankStrings.Count > 1) ? "s" : ""));
                }
            }
            else {
                PushEval(2, "High card.");

                List<string> rankStrings = new List<string>();
                foreach(int cardRank in orderingStraight.Select(c => (int)c.R)) {
                    rankStrings.Add(EnumStrings.RankStrings[cardRank]);
                    PushEval(
                        val: cardRank,
                        text: string.Format(
                            "High card{0}: {1}.",
                            (rankStrings.Count > 1) ? "s" : "",
                            string.Join(", ", rankStrings)));
                }
            }

            return useGroupedOrdering ? orderingGroupRankSuit.ToList() : orderingStraight;
        }

        //int IComparable<HandEvaluation>.CompareTo(HandEvaluation other) {
        public int CompareTo(HandEvaluation other)
        {
            var zipSeqs = Enumerable.Zip(
                first: this._evalRecs,
                second: other._evalRecs,
                resultSelector: (fstElem, sndElem) => fstElem.Value.CompareTo(sndElem.Value));

            int i = -1;     //index of the current comparison
            int result = 0;
            foreach(int compareResult in zipSeqs) {
                i++;
                result = compareResult;
                if(compareResult != 0) break;
            }

            this.IndexDeepestCompared = i;
            other.IndexDeepestCompared = i;
            return result;
        }
    }
}


