﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerCards {
    class CardDisp {
        private readonly string[] _suits = "♥♦♣♠".Select(e => e.ToString()).ToArray();
        private readonly string[] _ranks; //{"2", "3", .. , "K", "A"}
        private readonly string[] _suitsStr = "HDCS".Select(e => e.ToString()).ToArray();
        static CardDisp _myInst;
        /// <summary>Private ctor</summary>
        private CardDisp() {
            var numberStrings = 
                from num in Enumerable.Range(2, 9)
                select num.ToString();
            var namedCardStrs = 
                from c in "JQKA"
                select c.ToString();
            _ranks = numberStrings.Concat(namedCardStrs).ToArray();
        }
        private string GetCardString(Card c) {
            return _ranks[(int)c.R] + _suits[(int)c.S];
        }

        private static void CreateInstance()
        {
            if(_myInst == null) { _myInst = new CardDisp(); }
        }

        public static Card StringToNewCard(string rankSuit)
        {
            CreateInstance();
            return _myInst.CreateCardFromString(rankSuit);
        }

        private Card CreateCardFromString(string rankSuit)
        {
            string rankStr = rankSuit.Substring(0, rankSuit.Length - 1);
            string suitStr = rankSuit.Substring(rankSuit.Length - 1);
            
            Rank rank = (Rank)Array.IndexOf(_ranks, rankStr);
            Suit suit = (Suit)Array.IndexOf(_suitsStr, suitStr);
            return new Card {R = rank, S = suit};
        }

        public static string CardToStr(Card c) {
            CreateInstance();
            return _myInst.GetCardString(c);
        }
    }
}
