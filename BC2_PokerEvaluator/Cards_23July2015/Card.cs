﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerCards {
    public enum Rank { Two = 0, Three = 1, Four = 2, Five = 3, Six = 4, Seven = 5, Eight = 6, Nine = 7, Ten = 8, Jack = 9, Queen = 10, King = 11, Ace = 12 };
    public enum Suit { Hearts = 0, Diamonds = 1, Clubs = 2, Spades = 3 };

    public class EnumStrings {
        public static readonly string[] RankStrings;
        static EnumStrings()
        {
            Rank[] ranks = (Rank[]) Enum.GetValues(typeof (Rank));
            RankStrings = (from r in ranks select r.ToString()).ToArray();
        }
    }

    public struct Card : IEqualityComparer<Card> /* : IComparable<Card> */ {
        //private static string[] _suitStrings = Enum.GetValues(typeof (Suit)).Cast<String>().ToArray();

        public Rank R;
        public Suit S;
        public override string ToString() { return CardDisp.CardToStr(this); }

        ////Evaluator determines the ordering of cards in hands
        //int IComparable<Card>.CompareTo(Card other)
        //{
        //    int rankCompare = R.CompareTo(other.R);
        //    int suitCompare = S.CompareTo(other.S);
        //    if(rankCompare == 0) {
        //        return suitCompare;
        //    } else {
        //        return rankCompare;
        //    }
        //}

        bool IEqualityComparer<Card>.Equals(Card x, Card y)
        {
            return x.R == y.R && x.S == y.S;
        }

        int IEqualityComparer<Card>.GetHashCode(Card obj)
        {
            return (int)this.R * 15 + (int)this.S;
        }
    }

}
