﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CPSC5011_HW1
{
    /// <summary>Stack implementation using a dynamic array. </summary>
    class Stack<TElement> : IEnumerable<TElement>
    {
        const int CAPACITY_DEFAULT = 8; //default minimum capacity
        
        private TElement[] _array;      //elements in a stack

        /// <summary>Number of elements in the stack. </summary>
        public int Count { get; private set; }

        /// <summary>Default ctor. </summary>
        public Stack() : this(CAPACITY_DEFAULT) { }

        /// <summary>Ctor with a user-supplied initial capacity. </summary>
        public Stack(int initialCapacity)
        {
            if (initialCapacity < CAPACITY_DEFAULT)
            {
                initialCapacity = CAPACITY_DEFAULT;
            }
            _array = new TElement[initialCapacity];
        }

        /// <summary>Copy ctor. </summary>
        public Stack(Stack<TElement> argStack)
        {
            Count = argStack.Count;
            _array = new TElement[argStack._array.Length];
            Array.Copy(argStack._array, 0, _array, 0, argStack.Count);
        }

        /// <summary>Push value into the stack. </summary>
        public void Push(TElement value)
        {
            //resize the array if necessary
            if (Count == _array.Length)
            {
                TElement[] largerArray = new TElement[_array.Length * 2];
                Array.Copy(_array, 0, largerArray, 0, Count);
                _array = largerArray;
            }
            _array[Count++] = value;
        }

        /// <summary>Returns the value of the element at the top of the stack without removing it. </summary>
        public TElement Peek()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException("Stack is empty. Peek() is not allowed.");
            }
            return _array[Count - 1];
        }

        /// <summary>Remove the value from the top of the stack and return it. </summary>
        public TElement Pop()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException("Pop operation on an empty stack is not allowed.");
            }
            int topIndex = --Count;
            TElement returnElement = _array[topIndex];
            _array[topIndex] = default(TElement);   //allows for GC in case TElement is a ref type
            return returnElement;
        }

        /// <summary>Returns a new stack by combining contents 
        /// of the two stacks. (Nondestructive, immutable)</summary>
        public static Stack<TElement> operator +(Stack<TElement> infixLeft, Stack<TElement> infixRight)
        {
            int totalCount = infixLeft.Count + infixRight.Count;

            //smallest size that will fit all elements
            int newSize = 2 << (int)Math.Log(totalCount, 2);

            //deep-copy the stack array memory directly into the new stack array memory
            Stack<TElement> newStack = new Stack<TElement>(newSize);
            newStack.Count = totalCount;
            Array.Copy(infixLeft._array, 0, newStack._array, 0, infixLeft.Count);
            Array.Copy(infixRight._array, 0, newStack._array, infixLeft.Count, infixRight.Count);
            return newStack;
        }

        /// <summary>Returns an enumerator of the elements, last-in first. </summary>
        public IEnumerator<TElement> GetEnumerator()
        {
            for (int i = Count - 1; i >= 0; i--)
                yield return _array[i];
        }

        /// <summary>Required for the interface, just calls the public method</summary>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
