﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CPSC5011_HW1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program tests the implementation of the generic Stack class.");

            Random r = new Random();

            //generates 10 random numbers
            Func<IEnumerable<int>> randomNums = () =>
                Enumerable.Range(1, 10).Select(e => r.Next(1, 60));
            
            //generates 10 random chars
            Func<string> randomStr = () => {
                char[] randChars = randomNums().Select(n => (char)('A' + n)).ToArray();
                return new string(randChars);
            };

            //generates 10 random strings
            Func<IEnumerable<string>> randomStrings = () => {
                return Enumerable.Range(1, 10).Select(e => randomStr());
            };

            Console.WriteLine("\nCreating an integer Stack..");
            Stack<int> intStack = new Stack<int>();

            StackTests(intStack, randomNums().ToList());

            Console.WriteLine("\n" + new string('-', 60));

            Console.WriteLine("\nCreating a string Stack..");
            Stack<string> strStack = new Stack<string>();

            StackTests(strStack, randomStrings().ToList());

            Console.WriteLine("\nPress any key to continue..");
            Console.ReadKey(true);
        }

        public static void StackTests<T>(Stack<T> stack, IList<T> list)
        {
            Console.Write("\nPushing elements to the first stack: ");
            Console.WriteLine(string.Join(", ", list));
            
            foreach (T n in list)
                stack.Push(n);

            Console.WriteLine("\nStack one contents:");
            Console.WriteLine(string.Join("\n", stack));

            Console.WriteLine("\nCreating a second stack..");
            Stack<T> stackTwo = new Stack<T>();

            Console.WriteLine("\nPopping some elements from stack one and pushing them to stack two:");
            for (int i = 0; i < 5; i++)
                stackTwo.Push(stack.Pop());

            Console.WriteLine("\nStack one contents:");
            Console.WriteLine(string.Join("\n", stack));

            Console.WriteLine("\nStack two contents:");
            Console.WriteLine(string.Join("\n", stackTwo));

            Console.WriteLine("\nStack one + stack two = ");

            Stack<T> combinedStack = stack + stackTwo;
            Console.WriteLine(string.Join("\n", combinedStack));
        }

    }
}
