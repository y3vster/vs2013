﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace LinkedList {
//    public class LinkedList {
//        private class Node {
//            public int Data { get; set; }
//            public Node Next { get; set; }

//            public Node(int data, Node next)
//            {
//                this.Data = data;
//                this.Next = next;
//            }

//            public Node(int data)
//            {
//                this.Data = data;
//                this.Next = null;
//            }

//            public override string ToString()
//            {
//                return Data.ToString();
//            }
//        }

//        private Node head;
//        public int Length
//        {
//            get
//            {
//                int len = 0;
//                Node curNode = head;
//                while(curNode != null) {
//                    curNode = curNode.Next;
//                    len++;
//                }
//                return len;
//            }
//        }
//        public LinkedList() { head = null; }

//        //============================================================
//        //Methods as part of the assignment:

//        /// <summary>Get element at position</summary>
//        public int GetNth(int positIndex)
//        {
//            Node curNode = head;
//            for(int i = 0; i < positIndex; i++) {
//                if(curNode.Next == null) {
//                    throw new ArgumentOutOfRangeException("positIndex");
//                }
//                curNode = curNode.Next;
//            }
//            return curNode.Data;
//        }

//        /// <summary>Insert node at specific position</summary>
//        public void InsertNth(int positionIndex, int nodeData)
//        {   // [0]=2 [1]=5 [2]=Null
//            Node nodeBeforeIndex = head;
//            for(int i = 1; i <= positionIndex - 1; i++) {
//                if(nodeBeforeIndex == null) {
//                    throw new ArgumentOutOfRangeException("positionIndex");
//                }
//                nodeBeforeIndex = nodeBeforeIndex.Next;
//            }

//            Node nodeAtIndex = positionIndex == 0 ? head : nodeBeforeIndex.Next;

//            Node nodeToInsert = new Node(data: nodeData,
//                                         next: nodeAtIndex);
//            if(positionIndex == 0) {
//                head = nodeToInsert;
//            } else {
//                nodeBeforeIndex.Next = nodeToInsert;
//            }
//        }

//        /// <summary>Insert node at the correct position of a sorted list</summary>
//        public void SortedInsert(int nodeData)
//        {
//            //handle cases at the head of the list
//            if(head == null) {
//                head = new Node(nodeData);
//            } else if(nodeData <= head.Data) {
//                head = new Node(data: nodeData,
//                                next: head);
//            } else {
//                SortedInsertTailRecursion(nodeData: nodeData,
//                                          nodeLeft: head,
//                                          nodeRight: head.Next);
//            }
//        }

//        /// <summary>Helper for SortedInsert</summary>
//        private void SortedInsertTailRecursion(int nodeData, Node nodeLeft, Node nodeRight)
//        {
//            if(nodeRight == null) {
//                //base case: on last element, simply append
//                nodeLeft.Next = new Node(data: nodeData); //at the end, so just append
//            } else if(nodeData <= nodeRight.Data) {
//                //found a spot to insert
//                nodeLeft.Next = new Node(data: nodeData, next: nodeRight);
//            } else {
//                //tail-recursion
//                SortedInsertTailRecursion(nodeData: nodeData,
//                                          nodeLeft: nodeRight,
//                                          nodeRight: nodeRight.Next);

//                //// Iterative equivalent of tail recursion:
//                //
//                //while (true)    
//                //{
//                //    if (nodeRight == null)  
//                //    {
//                //        nodeLeft.Next = new Node(data: nodeData);
//                //    }
//                //    else if (nodeData <= nodeRight.Data)
//                //    {
//                //        nodeLeft.Next = new Node(data: nodeData, next: nodeRight);
//                //    }
//                //    else
//                //    {
//                //        nodeLeft = nodeRight;
//                //        nodeRight = nodeRight.Next;
//                //        continue;
//                //    }
//                //    break;
//                //}
//            }
//        }

//        /// <summary>Removes duplicate elements in the list</summary>
//        public void RemoveDuplicates()
//        {
//            if(head == null) { return; }

//            HashSet<int> hashSet = new HashSet<int>();
//            hashSet.Add(head.Data);

//            Node precedingNode = head;         //node preceding the one being analyzed
//            Node examinedNode = head.Next;

//            while(examinedNode != null) {
//                if (hashSet.Contains(examinedNode.Data))
//                {
//                    precedingNode.Next = examinedNode.Next; //seen it before, remove it
//                }
//                else
//                {                                           //case: fresh value
//                    hashSet.Add(examinedNode.Data);         //record it
//                    precedingNode = examinedNode;           //set preceding node to the first encounter with a fresh value
//                }
//                examinedNode = examinedNode.Next;           //advance to examine next node
//            }
//        }

//        //============================================================
//        //Methods that came with the class:


//        //public static void Main(string[] args)
//        //{
//        //    Console.WriteLine("GetNth Test:\n" + new string('-', 60) + "\n");
//        //    LinkedList myList = new LinkedList();
//        //    for(int i = 10; i < 110; i += 10) {
//        //        myList.Push(i);
//        //    }
//        //    for(int i = 0; i < myList.Length; i++) {
//        //        Console.WriteLine("Element {0} is {1}", i, myList.GetNth(i));
//        //    }

//        //    Console.WriteLine("\nInsertNth Test:\n" + new string('-', 60) + "\n");

//        //    LinkedList myList2 = new LinkedList();
//        //    myList2.InsertNth(0, 130);
//        //    myList2.InsertNth(0, 110);
//        //    myList2.InsertNth(0, 100);
//        //    myList2.InsertNth(2, 120);
//        //    myList2.InsertNth(myList2.Length, 1000);
//        //    myList2.InsertNth(myList2.Length, 1100);
//        //    myList2.InsertNth(myList2.Length, 1200);
//        //    Console.WriteLine("{0}", myList2); // output the entire list


//        //    Console.WriteLine("\nSortedInsert Test:\n" + new string('-', 60) + "\n");
//        //    LinkedList myList3 = new LinkedList();
//        //    Random rm = new Random();
//        //    for(int i = 0; i < 20; i++) {
//        //        myList3.SortedInsert(rm.Next(1, 100));
//        //    }
//        //    Console.WriteLine(myList3);


//        //    Console.WriteLine("\nRemoveDuplicates() Test:\n" + new string('-', 60) + "\n");
//        //    LinkedList myList4 = new LinkedList();

//        //    for(int i = 0; i < 32; i++) {
//        //        myList4.Enqueue(rm.Next(1, 17));     //32 values in range of 1 - 16, forcing duplicates
//        //    }

//        //    Console.WriteLine("Generated list of 32 values: \n" + myList4 + "\n");
//        //    myList4.RemoveDuplicates();
//        //    Console.WriteLine("List after RemoveDuplicates() was called: \n" + myList4 + "\n");

//        //    Console.WriteLine("\nPress any key to exit..");
//        //    Console.ReadKey(true);
//        //}

//        //------------------------------------------------------------
//        //methods that came with the class:

//        // place new node at the head of the list
//        public void Push(int data)
//        {
//            Node node = new Node(data, head);
//            this.head = node;
//        }

//        // remove the node from the front of the list
//        // (never call this method on an empty list)
//        public int Pop()
//        {
//            int data = head.Data;
//            head = head.Next;
//            return data;
//        }

//        // place new node at the end of the list
//        public void Enqueue(int data)
//        {
//            // place new node at the end of the list
//            Node newNode = new Node(data);
//            if(head == null) {
//                head = newNode;
//            } else {
//                Traverse(head).Next = newNode;
//            }
//        }

//        // remove the first item from the queue
//        public int Dequeue()
//        {
//            int data = head.Data;
//            head = head.Next;
//            return data;
//        }

//        // return a reference to the last node in the list
//        private Node Traverse(Node node)
//        {
//            if(node.Next != null) {
//                return Traverse(node.Next);
//            }
//            return node;
//        }

//        public override string ToString()
//        {
//            String rtnValue = "";
//            Node node = head;

//            while(node != null) {
//                rtnValue += node.ToString() + " ";
//                node = node.Next;
//            }

//            return rtnValue.Trim();
//        }



//    }
//}
