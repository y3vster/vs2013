﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;

namespace Lab1
{
    public class Lab1
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter file name to read the numbers from (e.g. numbers.txt): ");
            string filePath = Console.ReadLine(); 

            Console.WriteLine("Opening file {0} and creating a linked list..", filePath);
            NonDescendingLinkedList list = CreateListFromFile(filePath);

            if (list != null)
            {
                Console.WriteLine("\nNon-descending linked list contents: ");
                Console.WriteLine(string.Join(", ", list.Values()));

                Console.WriteLine("\nRemoving duplicates..");
                list.DeleteDuplicates();

                Console.WriteLine("\nUpdated list contents: ");
                Console.WriteLine(string.Join(", ", list.Values()));
            }

            Console.WriteLine("\nPress any key to exit..");
            Console.ReadKey(true);
        }

        /// <summary>Returns a new linked list from the numbers in a file. </summary>
        private static NonDescendingLinkedList CreateListFromFile(string fileName)
        {
            string fileContents;
            try
            {
                fileContents = File.ReadAllText(fileName);
            }
            catch (Exception)
            {
                Console.WriteLine("There was an issue reading a file " +
                    Directory.GetCurrentDirectory() + @"\" + fileName);
                return null;
            }

            NonDescendingLinkedList list = new NonDescendingLinkedList();
            Debug.Write("\nAdding nums fm file: ");

            //find integers in a file and add them to the linked list
            foreach (Match m in Regex.Matches(fileContents, @"-?\d+", RegexOptions.Multiline))
            {
                list.Insert(int.Parse(m.Value));
                Debug.Write(m.Value + " ");
            }

            return list;
        }
    }

    /// <summary>Non-descending linked list. </summary>
    public class NonDescendingLinkedList
    {
        sealed class Node
        {
            public int Value { get; set; }
            public Node Next { get; set; }
        }

        private Node _head;  //default ctor: empty list

        /// <summary>Inserts a value in a non-descending order</summary>
        public void Insert(int val)
        {
            Node next = _head;
            Node prev = null;
            
            //find insertion pt
            while (next != null && next.Value < val)
            {
                prev = next;
                next = next.Next;
            }

            if (prev == null)
            {
                _head = new Node { Value = val, Next = _head };
            }
            else
            {
                prev.Next = new Node { Value = val, Next = next };
            }
        }

        /// <summary>Removes duplicates in-place</summary>
        public void DeleteDuplicates()
        {
            Node node = _head;
            while (node != null && node.Next != null)
            {
                if (node.Value == node.Next.Value)
                {
                    node.Next = node.Next.Next;
                }
                else
                {
                    node = node.Next;
                }
            }
        }

        /// <summary>Enumerator for looping through node values.</summary>
        public IEnumerable<int> Values()
        {
            Node node = _head;
            while (node != null)
            {
                yield return node.Value;
                node = node.Next;
            }
        }
    }

}


//Write a function that will read integers from a file and insert them into a linked list in non-descending order. Then write a second function that has as input the linked list of integers. If any of the integers in the list appears more than once, remove all but one copy of that integer. When the function is done, there should be no more than a single copy of any integer on the list and the list should remain in non-descending order.

//Provide a print function to prove your other functions work correctly.

//Call your source file "Lab1.cs" and submit on Canvas.
