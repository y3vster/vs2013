﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Queue
{
    class Lab4
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating a queue..");
            Queue queue = new Queue();

            Test(queue.IsEmpty() , "queue.IsEmpty(): True");

            Console.WriteLine("Enque 1-10");
            for (int i = 1; i <= 10; i++)
            {
                queue.Enqueue(i);
            }

            Test(queue.IsEmpty() == false, "queue.IsEmpty(): False");
            Test(queue.Peek() == 1, "queue.Peek(): 1");

            Console.Write("Dequeue 5 items: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(queue.Dequeue() + " ");
            }
            Console.WriteLine();

            Test(queue.Peek() == 6, "5 items successfully removed, Peek() == 6");

            Console.Write("Remove remaining items: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(queue.Dequeue() + " ");
            }
            Console.WriteLine();

            Test(queue.IsEmpty(), "IsEmpty() returns True as expected");

            Console.WriteLine("Queue tests finished, press any key to exit..");
            Console.ReadKey(true);
        }


        private static void Test(bool condition, string msg)
        {
            Console.WriteLine((condition ? "PASS: " : "FAIL: ") + msg);
        }

    }
}




//For this assignment you will provide a Queue package. You will supply a driver program to test each function of the in the Queue package.

//Specifically, in C#:

//1. Queue package: This package provides a linked list implementation of a queue of integers. It must protect the data from misuse and provide all standard public queue methods, including enqueue, dequeue, and a Boolean method that tests for the empty queue. A “front” method is optional. You should add any other private methods needed in the class, but no other public methods are allowed. Be sure to include a copy constructor. Name the class Queue.

//2. Driver program: This program tests all of the functions provided by the Queue package. The user interface is minimal, but clear. Exactly how the tests are conducted is your decision, but the tests and the results should be clear to the user.

//Files must be called "Queue.cs" and "Lab4.cs". Compress files and submit zip file (i.e. Lab4.zip) on Canvas.
