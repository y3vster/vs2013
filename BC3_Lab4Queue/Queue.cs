﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Queue
{
    class Queue
    {
        internal sealed class Node
        {
            public int Value { get; private set; }
            public Node Next { get; set; } 
            /// <summary>Do not allow default ctor.</summary>
            private Node() { }
            public Node(int value) { Value = value; }
        }

        private Node _head;
        private Node _tail;

        /// <summary>Ctor. </summary>
        public Queue() { }

        /// <summary>Copy ctor. </summary>
        public Queue(Queue copyQueue)
        {
            Node node = copyQueue._head;
            while (node != null)
            {
                Enqueue(node.Value);
                node = node.Next;
            }
        }

        /// <summary>Returns true if the queue is empty</summary>
        public bool IsEmpty()
        {
            return _head == null;
        }


        /// <summary>Appends a value to the tail of the Queue</summary>
        public void Enqueue(int value)
        {
            Node newNode = new Node(value);
            if (_head == null)
            {
                _head = _tail = newNode;
            }
            else
            {
                _tail.Next = newNode;
                _tail = newNode;
            }
        }

        /// <summary>Removes a value from the head of the Queue and returns it</summary>
        public int Dequeue()
        {
            if (_head == null)
            {
                throw new InvalidOperationException();
            }
            Node removedNode = _head;
            _head = _head.Next;
            return removedNode.Value;
        }

        /// <summary>Returns a value from the head of the Queue without removing it</summary>
        public int Peek()
        {
            if (_head == null)
            {
                throw new InvalidOperationException();
            }
            return _head.Value;
        }

    }
}
