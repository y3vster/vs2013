﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace BC3_Lab14 {
    internal class Program {


        private static Stopwatch _stopwatch = new Stopwatch();    //used for timing
        private static Random _rand = new Random();

        /// <summary>Initializes the timer </summary>
        private static void InitStopwatch()
        {
            //Frequency attribute is in ticks/sec
            double muSecPerTick = 1000000.0 / Stopwatch.Frequency;

            Console.WriteLine("Stopwatch is high resolution: " + Stopwatch.IsHighResolution);
            Console.WriteLine("Stopwatch resolution is ~ {0:F2} μs", muSecPerTick);

            //use second processor core for the test
            Process.GetCurrentProcess().ProcessorAffinity = new IntPtr(2);

            //prevent "Normal" processes and threads from forcing me into a context switch mid-test
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
        }


        /// <summary>Types of sorts being tested. </summary>
        enum TSort { Insert, Merge, Heap, Quick, List }

        private static void Main(string[] args)
        {
            InitStopwatch();

            List<Dictionary<int, double>> results = new List<Dictionary<int, double>>();

            foreach (TSort typeSort in Enum.GetValues(typeof(TSort)))
            {
                int n = 100;    //start w/100 elements for each algorithm

                //store current algorithm results
                Dictionary<int, double> curAlgResults = new Dictionary<int, double>();

                while (true)
                {

                    //new size
                    int[] array = new int[n];

                    //store trial info
                    List<double> trials = new List<double>(10);

                    for (int i = 0; i < 10; i++)
                    {
                        //reset the array to sort
                        FillWithRandoms(ref array);

                        //time the code
                        double curTrial = GetSeconds(ref array, typeSort);

                        //I don't want to wait too long..
                        if (curTrial > 0.5) break;

                        //save results
                        trials.Add(curTrial);
                    }

                    //the last timing took too long, move to the next algorithm
                    if (trials.Count != 10) break;

                    //save the results for n
                    double avgTime = trials.Average();
                    curAlgResults.Add(n, avgTime);

                    n *= 2;
                }

                //store current algorithm results
                results.Add(curAlgResults);

            } //next algorithm



            Console.WriteLine("\nRESULTS:\n" + new string('-', 60));
            foreach (TSort typeSort in Enum.GetValues(typeof(TSort)))
            {
                Console.WriteLine(typeSort + ":");
                Dictionary<int, double> algResults = results[(int)typeSort];
                foreach (KeyValuePair<int, double> keyValuePair in algResults)
                {
                    Console.WriteLine("N: {0,5} = {1:F1} μs", keyValuePair.Key, keyValuePair.Value * 1000000);
                }
                Console.WriteLine();
            }

            Console.Write("Write to .CSV? (Y/N) ");
            if ("y" == Console.ReadLine().ToLower())
            {
                List<string> outList = new List<string>();
                TSort curAlgorithmType = 0;
                foreach (Dictionary<int, double> result in results)
                {
                    outList.Add("\nAlgorithm: " + curAlgorithmType + "\n");
                    foreach (KeyValuePair<int, double> pair in result)
                        outList.Add(pair.Key + "," + pair.Value);
                    curAlgorithmType++;
                }

                Console.Write("File name? ");
                File.WriteAllLines(Console.ReadLine() + ".csv", outList);
            }

            Console.WriteLine("Press any key to exit ..");
            Console.ReadKey(true);
        }

        private static void FillWithRandoms(ref int[] array)
        {
            for(int i = 0; i < array.Length; i++)
                array[i] = _rand.Next();
        }

        private static double GetSeconds<T>(ref T[] array, TSort typeSort) where T : IComparable<T>
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //timing section
            _stopwatch.Reset();

            switch(typeSort) {
                case TSort.Insert:
                    _stopwatch.Start();
                    Sort.InsertionSort(ref array);
                    _stopwatch.Stop();
                    break;
                case TSort.Merge:
                    _stopwatch.Start();
                    Sort.MergeSort(ref array);
                    _stopwatch.Stop();
                    break;
                case TSort.Heap:
                    _stopwatch.Start();
                    Sort.HeapSort(ref array);
                    _stopwatch.Stop();
                    break;
                case TSort.Quick:
                    _stopwatch.Start();
                    Sort.QuickSort(ref array);
                    _stopwatch.Stop();
                    break;
                case TSort.List:
                    _stopwatch.Start();
                    System.Array.Sort(array);
                    _stopwatch.Stop();
                    break;
            }

            //results of a single trial
            double microSecs = _stopwatch.Elapsed.TotalSeconds * 1000000.0;
            Console.WriteLine("{0, 6}, N: {1,5} = {2:F1} μs", typeSort, array.Length, microSecs);
            return _stopwatch.Elapsed.TotalSeconds;
        }
    }
}
