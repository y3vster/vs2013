﻿using System;


//------------------------------------------------------------------------------

namespace BC3_Lab14 {
    /// <summary> sorting algorithms implementation (ascending) </summary>
    class Sort {
        /// <summary>Insertion sort implementation</summary>
        public static void InsertionSort<T>(ref T[] array) where T : IComparable<T>
        {
            for(int i = 0; i < array.Length; i++) {
                for(int j = i; j > 0 && array[j].CompareTo(array[j - 1]) < 0; j--)
                    Exchange(ref array, j - 1, j);
            }
        }


        /// <summary>Heapsort. </summary>
        public static void HeapSort<T>(ref T[] array) where T : IComparable<T>
        {
            //heapify
            int size = array.Length;
            for(int i = size / 2; i >= 0; i--)
                PercolateDown(ref array, i, size);
            //sort
            for(int i = size - 1; i > 0; i--) {
                Exchange(ref array, 0, i);
                PercolateDown(ref array, 0, i);
            }
        }

        /// <summary>Mergesort. </summary>
        public static void MergeSort<T>(ref T[] array) where T : IComparable<T>
        {
            T[] scratch = new T[array.Length]; //scratch array
            SplitMerge(ref array, 0, array.Length - 1, ref scratch);
        }


        /// <summary>Mergesort helper</summary>
        private static void SplitMerge<T>(ref T[] array, int left, int right, ref T[] scratch) where T : IComparable<T>
        {
            if(right - left < 1) return;
            int mid = (left + right + 1) / 2; //mid value
            SplitMerge(ref array, left, mid - 1, ref scratch);
            SplitMerge(ref array, mid, right, ref scratch);
            Merge(ref array, left, mid, right, ref scratch);
            Array.Copy(scratch, left, array, left, right - left + 1);
        }


        //left half = srcArr[left .. mid-1]
        //right half= srcArr[mid  .. right]
        private static void Merge<T>(ref T[] srcArr, int left, int mid, int right, ref T[] destArr) where T : IComparable<T>
        {
            int srcInd1 = left; //index of the first source array
            int srcInd2 = mid;  //index of the second source array
            for(int i = left; i <= right; i++) {
                if(srcInd1 < mid && (srcInd2 > right || IsLessOrEqual(ref srcArr, srcInd1, srcInd2))) {
                    destArr[i] = srcArr[srcInd1];
                    srcInd1++;
                } else {
                    destArr[i] = srcArr[srcInd2];
                    srcInd2++;
                }
            }

        }

        /// <summary>Quicksort implementation. </summary>
        public static void QuickSort<T>(ref T[] array) where T : IComparable<T>
        {
            QuickSort(ref array, 0, array.Length - 1);
        }

        /// <summary>Quicksort algorithm implementation. </summary>
        private static void QuickSort<T>(ref T[] array, int left, int rght) where T : IComparable<T>
        {
            if(rght <= left) return;
            int p = QSortPartition(ref array, left, rght);
            QuickSort(ref array, left, p - 1);
            QuickSort(ref array, p + 1, rght);
        }
        /// <summary>Partitioning for quicksort. </summary>
        private static int QSortPartition<T>(ref T[] array, int left, int rght) where T : IComparable<T>
        {
            int pivot = rght;
            int i = left;
            for(int j = left; j < rght; j++) {
                if(IsLess(ref array, j, pivot)) {
                    Exchange(ref array, i, j);
                    i++;
                }
            }
            Exchange(ref array, rght, i);
            return i;
        }


        /// <summary>Exchange entry positions x & y in the array T[] a</summary>
        private static void Exchange<T>(ref T[] a, int x, int y)
        {
            T temp = a[x];
            a[x] = a[y];
            a[y] = temp;
        }

        private static bool IsLessOrEqual<T>(ref T[] a, int posL, int posH) where T : IComparable<T>
        {
            return a[posL].CompareTo(a[posH]) <= 0;
        }

        private static bool IsLess<T>(ref T[] a, int posL, int posH) where T : IComparable<T>
        {
            return a[posL].CompareTo(a[posH]) < 0;
        }


        private static void PercolateDown<T>(ref T[] a, int cur, int size) where T : IComparable<T>
        {
            int ch = 2 * cur + 1;  //child position
            if(ch >= size)
                return;
            if(ch + 1 < size && IsLess(ref a, ch, ch + 1))
                ch++;
            if(IsLess(ref a, cur, ch))
                Exchange(ref a, cur, ch);
            PercolateDown(ref a, ch, size);
        }
    }
}
