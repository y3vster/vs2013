﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter10 {
    class Program {
        static void Main(string[] args) {
            TimedAccount timedAcc = new TimedAccount(amount: 0, rate: 0);
            for (int i = 0; i < 3; i++)
            {
                timedAcc.Deposit(10.0);
                Console.WriteLine("Deposit made.");
            }

            try
            {
                timedAcc.Deposit(10.0);
            }
            catch (Exception e)
            {
                Console.WriteLine("Fourth deposit failed, as expected, with message: {0}", e.Message);
            }

            timedAcc.Withdraw(10.0);
            Console.WriteLine("Withdrawal made.");

            try
            {
                timedAcc.Withdraw(30.0);
            }
            catch (Exception e)
            {
                Console.WriteLine("Withdrawal over amount available failed with message: {0}", e.Message);
            }

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);

        }
    }
}
