//Copyright (c) 2002, Art Gittleman
//This example is provided WITHOUT ANY WARRANTY 
//either expressed or implied.

/* Defines a SavingsAccount class derived from BankAccount.  
 */

public class SavingsAccount : BankAccount
{
    private double interestRate;

    public SavingsAccount(double amount, double rate)
        : base(amount)
    {
        interestRate = rate;
    }
    public void PostInterest()
    {
        double balance = GetBalance();
        double interest = interestRate / 100 * balance;
        Deposit(interest);
    }
}