﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter10
{

    class TimedAccount : SavingsAccount
    {
        const int MAX_DEPOSITS_ALLOWED = 3;
        private static int numDepositsMade = 0;

        /// <summary></summary>
        public TimedAccount(double amount, double rate)
        : base(amount, rate) { }

        public override void Withdraw(double amount)
        {
            if (GetBalance() < amount)
            {
                throw new InvalidOperationException("Withdrawal amount exceeds funds available.");
            }
            base.Withdraw(amount);
        }

        public override void Deposit(double amount)
        {
            if (numDepositsMade >= MAX_DEPOSITS_ALLOWED)
            {
                throw new InvalidOperationException(String.Format("Deposit failed. You have exceeded {0} allowed deposit.", MAX_DEPOSITS_ALLOWED));
            }
            base.Deposit(amount);
            numDepositsMade++;
        }


    }



}
