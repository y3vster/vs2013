﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2
{
    internal class CampGround : Accomodation
    {
        public override void Reserve()
        {
            Console.WriteLine("Find a campground and prepay.");
        }

        public override void CheckIn()
        {
            Console.WriteLine("Pitch a tent and start a fire.");
        }

        public override void TipStaff()
        {
            Console.WriteLine("No tip was needed for a campground.");
        }

        public override void PayBill()
        {
            Console.WriteLine("Campground fees were prepaid at the reservation.");
        }
    }
}