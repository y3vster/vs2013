﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2
{
    class Motel : Accomodation
    {
        public override void Reserve()
        {
            Console.WriteLine("Calling the motel to reserve a room, but they don't do reservations.");
        }
        public override void CheckIn()
        {
            Console.WriteLine("Walking up to the motel desk to get a key.");
        }
        public override void PayBill()
        {
            Console.WriteLine("Paying cash for the motel room.");
        }
        public override void TipStaff()
        {
            Console.WriteLine("There is no one to tip at the motel..");
        }
    }
}
