﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2
{
    class LuxuryHotel : Accomodation
    {
        public override void Reserve()
        {
            Console.WriteLine("Making a reservation for the luxury hotel online.");
        }
        public override void CheckIn()
        {
            Console.WriteLine("Getting a key to the luxury hotel.");
        }
        public override void PayBill()
        {
            Console.WriteLine("Paying for the luxury hotel with a credit card");
        }
        public override void TipStaff()
        {
            Console.WriteLine("Tipping the valet.");
        }
    }
}
