﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2 {
    /// <summary>Template design pattern. Subclasses implement details of how SleepOut() occurs. </summary>
    abstract class Accomodation
    {
        public void SleepOut()
        {
            Reserve();
            CheckIn();
            TipStaff();
            PayBill();
        }
        public abstract void Reserve();
        public abstract void CheckIn();
        public abstract void TipStaff();
        public abstract void PayBill();
    }
}
