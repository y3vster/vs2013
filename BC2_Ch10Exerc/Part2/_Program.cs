﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2 {
    class Program {
        static void Main(string[] args)
        {
            Accomodation[] accomodations = {new CampGround(), new LuxuryHotel(), new Motel()};

            foreach (Accomodation accom in accomodations)
            {
                string accomName = accom.GetType().FullName.Split('.').Last();
                Console.WriteLine(accomName + " SleepOut() call:\n" + new string('-',60));
                accom.SleepOut();
                Console.WriteLine();
            }

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);

        }
    }
}
