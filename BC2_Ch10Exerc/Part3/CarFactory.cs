﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3{
    class CarFactory {
        public static CarKey GetNewCarKey(EnumCarType carType)
        {
            switch (carType)
            {
                case EnumCarType.Compact:
                    return new CarKey(new CarCompact());
                case EnumCarType.Luxury:
                    return new CarKey(new CarLuxury());
                default:
                    throw new ArgumentOutOfRangeException("carType", carType, null);
            }
        }
    }
}
