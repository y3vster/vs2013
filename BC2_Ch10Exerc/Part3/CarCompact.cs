﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3{
    class CarCompact : Car{
        public override void PrintMakeModel()
        {
            Console.WriteLine("I am a Ford Focus");
        }

        public override EnumCarType CarType
        {
            get { return EnumCarType.Compact;}
        }
    }
}
