﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3{
    class Reservation
    {
        private readonly Guid _confirmation;
        private readonly EnumCarType _carType;
        public string ConfirmationNumber { get { return _confirmation.ToString(); } }
        public EnumCarType CarType { get { return _carType; } }

        public Reservation(EnumCarType carType)
        {
            _carType = carType;
            _confirmation = Guid.NewGuid();
        }

        public override string ToString()
        {
            return String.Format(@"Reservation for '{0}' car with confirmation number '{1}'",
                _carType.ToString(), _confirmation);
        }

        public override int GetHashCode()
        {
            return _confirmation.GetHashCode();
        }
    }
}
