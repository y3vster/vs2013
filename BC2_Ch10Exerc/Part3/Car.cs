﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3{
    /// <summary>Base class for car rental. </summary>
    abstract class Car
    {
        public abstract void PrintMakeModel();
        public abstract EnumCarType CarType { get; }
    }
}
