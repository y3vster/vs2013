﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3 {
    /// <summary>
    /// Service agent at the counter responsibilities:
    /// Make a reservation
    /// Cancel a reservation
    /// Check if the keys are available for pick up
    /// </summary>
    class ActorCounterAgent
    {
        private readonly ActorGarageAttendant _garageAttendant;
        private readonly ReservationManager _reservationManager;
        public ActorCounterAgent(ActorGarageAttendant garageAttendant, ReservationManager reservationManager)
        {
            _garageAttendant = garageAttendant;
            _reservationManager = reservationManager;
        }

        public string GetNewReservation(EnumCarType carType)
        {
            Reservation newReservation = _reservationManager.MakeReservation(carType);
            Console.WriteLine("\nService Agent created a new reservation for {0} car with a confirmation {1}", carType, newReservation.ConfirmationNumber );
            return newReservation.ConfirmationNumber;
        }

        public void CancelReservation(string confirmation)
        {
            Console.WriteLine("Service agent cancels reservation " + confirmation);
            _reservationManager.CancelReservation(confirmation);
        }

        public CarKey GetCarKeyForReservation(string confirmation)
        {
            return _garageAttendant.HandOutKeys(_reservationManager.GetReservationFromConfirmation(confirmation).CarType);
        }

        public bool KeysAvailable(string reservation)
        {
            Console.WriteLine("Service agent is calling the garage attendant to check if keys are ready for pick up..");
            Reservation reservInst = _reservationManager.GetReservationFromConfirmation(reservation);
            bool keysAvailable = _garageAttendant.KeysAvailable(reservInst.CarType);
            if (keysAvailable)
            {
                Console.WriteLine("Service agent tells the customer that the car is waiting in the garage."); 
            }
            else
            {
                Console.WriteLine("Service agent tells the customer to wait while the car is being serviced.");
            }
            return keysAvailable;
        }
    }

}
