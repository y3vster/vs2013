﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3 {
    
    /// <summary>
    /// Garage attendant responsibilities:
    /// Check if the keys are available
    /// Receive a rental return from a customer
    /// Service returned cars and make them available
    /// </summary>
    class ActorGarageAttendant
    {

        private Garage _garage;

        public ActorGarageAttendant(Garage garage)
        {
            _garage = garage;
        }

        public bool KeysAvailable(EnumCarType carType)
        {
            return _garage.CarTypeAvailable(carType);
        }

        public CarKey HandOutKeys(EnumCarType carType)
        {
            CarKey carKey = _garage.GetCarKeys(carType);
            Console.WriteLine("\nGarage attendant hands out key id {0} for a {1} car.", carKey.UniqueId, carType);
            return carKey;
        }

        public void AcceptRentalReturn(CarKey returnedKey)
        {
            Console.WriteLine("\nGarage attendant accepts rental return key " + returnedKey.UniqueId);
            _garage.AddCarNeedingServicing(returnedKey);
        }

        public void ServiceCars()
        {
            Console.WriteLine("\nGarage attendant is told to service returned cars.");
            foreach (CarKey key in _garage.GetKeysForServicing())
            {
                Console.Write("Garage attendant is servicing a returned car: ");
                key.PhysicalCar.PrintMakeModel();
                _garage.AddAvailableCarKeys(key);
                Console.WriteLine("Garage attendant made another {0} car available for pickup", key.PhysicalCar.CarType);
            }
        }

    }
}
