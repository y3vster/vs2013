﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3{
    class Program {

        //Actor(s)			System Boundary: Action
        //------------------------------------------------------------
        //Car Renter/		Reservations: Make a reservation
        //Rental Agent		
        //------------------------------------------------------------
        //Car Renter		Reservations: Cancel reservation
        //Rental Agent
        //------------------------------------------------------------
        //Rental Agent		Garage: Check if car keys are ready
        //------------------------------------------------------------
        //Car Renter		Garage: Pick up the keys
        //------------------------------------------------------------
        //Car Renter		Garage: Return the rental
        //------------------------------------------------------------
        //Garage Attendant	Garage: Receive car keys for servicing
        //------------------------------------------------------------
        //Garage Attendant	Garage: Make keys avalable
        //------------------------------------------------------------

        static void Main(string[] args) {
            
            //the model is the car garage that manages the cars            
            Garage carGarage = new Garage();

            //reservation system keeps track of the reservations
            ReservationManager reservationSystem = new ReservationManager();

            //garage attendant interacts with the garage system only
            ActorGarageAttendant garageAttendant = new ActorGarageAttendant(carGarage);
            
            //customer service interacts with the reservation system and the garage attendant only
            ActorCounterAgent counterAgent = new ActorCounterAgent(garageAttendant, reservationSystem);

            //customers interact with the counter agent and the garage attendant 
            ActorCustomer customer = new ActorCustomer(garageAttendant, counterAgent);

            carGarage.Test_ShowInventory();

            //reserve a compact and luxury cars
            string reservCompact = customer.ReserveCar(EnumCarType.Compact);
            string reservLuxury = customer.ReserveCar(EnumCarType.Luxury);
            CarKey carKey = customer.PickUpKeys(reservCompact);

            carGarage.Test_ShowInventory();

            customer.ReturnRental(carKey);
            garageAttendant.ServiceCars();

            Console.WriteLine("\nPress any key to exit..");
            Console.ReadKey(true);


        }



    }
}
