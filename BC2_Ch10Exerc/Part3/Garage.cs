﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3 {
    class Garage {
        /// <summary>Queue of cars Ready For Issue (RFI)</summary>
        private Dictionary<EnumCarType, Queue<CarKey>> _inventoryRFI;

        /// <summary>Queue of cars Not Ready For Issue (NRFI), e.g. needs cleaning, servicing, etc.. </summary>
        private Queue<CarKey> _inventoryNRFI;

        public Garage()
        {
            _inventoryRFI = new Dictionary<EnumCarType, Queue<CarKey>>();
            foreach(EnumCarType carType in (EnumCarType[])Enum.GetValues(typeof(EnumCarType))) {
                _inventoryRFI.Add(carType, new Queue<CarKey>());
            }
            _inventoryNRFI = new Queue<CarKey>();
            Test_InitInventory();
        }

        private void Test_InitInventory()
        {
            //start with 2 luxury and 3 compact cars
            var newCars = new[] {
                CarFactory.GetNewCarKey(EnumCarType.Luxury),
                CarFactory.GetNewCarKey(EnumCarType.Compact),
                CarFactory.GetNewCarKey(EnumCarType.Compact) };

            foreach(CarKey carKey in newCars) {
                AddAvailableCarKeys(carKey);
            }
        }

        public void Test_ShowInventory()
        {
            var carInventory =
                from queue in _inventoryRFI.Values
                from key in queue
                select key.PhysicalCar;
            Console.WriteLine("\nCurrent garage inventory\n" + new string('-', 60));
            foreach(Car car in carInventory) {
                car.PrintMakeModel();
            }
        }


        //Rental Agent		Garage: Check if car keys are ready 
        //------------------------------------------------------------ 
        public bool CarTypeAvailable(EnumCarType carType)
        {
            return _inventoryRFI[carType].Any();
        }

        //Car Renter			Garage: Pick up the keys 
        //------------------------------------------------------------ 
        public CarKey GetCarKeys(EnumCarType carType)
        {
            return _inventoryRFI[carType].Dequeue();
        }

        //Car Renter			Garage: Return the rental 
        //------------------------------------------------------------ 
        public void AddCarNeedingServicing(CarKey carKeyReturned)
        {
            _inventoryNRFI.Enqueue(carKeyReturned);
        }

        //Garage Attendant	Garage: Receive car keys for servicing
        //------------------------------------------------------------ 
        public IEnumerable<CarKey> GetKeysForServicing()
        {
            while(_inventoryNRFI.Any()) {
                yield return _inventoryNRFI.Dequeue();
            }
        }

        //Garage Attendant	Garage: Make keys avalable
        //------------------------------------------------------------ 
        public void AddAvailableCarKeys(CarKey carKey)
        {
            EnumCarType carType = carKey.PhysicalCar.CarType;
            _inventoryRFI[carType].Enqueue(carKey);
        }
    }
}
