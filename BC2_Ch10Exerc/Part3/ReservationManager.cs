﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3 {
    internal class ReservationManager {
        private readonly Dictionary<string, Reservation> _reservations;

        public ReservationManager()
        {
            _reservations = new Dictionary<string, Reservation>();
        }

        /// <summary>Get a new reservation based on the car type. </summary>
        public Reservation MakeReservation(EnumCarType carType)
        {
            Reservation r = new Reservation(carType);
            Console.WriteLine("\nReservation system creates: " + r);
            _reservations.Add(r.ConfirmationNumber, r);
            return r;
        }

        /// <summary>Cancel reservation based on the confirmation number. </summary>
        public void CancelReservation(string confirmation)
        {
            Reservation cancelledReservation = _reservations[confirmation];
            _reservations.Remove(confirmation);
            Console.WriteLine("\nReservation system cancelled: " + cancelledReservation);
        }

        public Reservation GetReservationFromConfirmation(string confirmation)
        {
            return _reservations[confirmation];
        }

    }
}
