﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3 {
    /// <summary>Each car instance has a unique key</summary>
    class CarKey
    {
        /// <summary>give each key a unique serial number</summary>
        public int UniqueId { get; private set; }

        /// <summary>If you have access to the car key, you have access to the car, but not the reverse</summary>
        public Car PhysicalCar { get; private set; }

        public CarKey(Car car)
        {
            PhysicalCar = car;
            Random rm = new Random();
            UniqueId = rm.Next();
        }

        public override int GetHashCode()
        {
            return UniqueId;
        }
    }
}
