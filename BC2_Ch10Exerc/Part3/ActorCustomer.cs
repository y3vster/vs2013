﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Part3 {
    /// <summary>
    /// Customer responsibilities:
    /// Make a reservation through an agent or online
    /// Cancel a reservation through an agent or online
    /// Pick up the keys
    /// Return the car
    /// </summary>
    class ActorCustomer
    {
        private ActorCounterAgent _counterAgent;
        private ActorGarageAttendant _garageAttendant;

        public ActorCustomer(ActorGarageAttendant garageAttendant, ActorCounterAgent counterAgent)
        {
            _garageAttendant = garageAttendant;
            _counterAgent = counterAgent;
        }

        public string ReserveCar(EnumCarType carType)
        {
            string newReservation = _counterAgent.GetNewReservation(carType);
            Console.WriteLine("\nCustomer reserved a {0} car with a confirmation# {1}", carType, newReservation);
            return newReservation;
        }

        public void CancelReservation(string confirmation)
        {
            _counterAgent.CancelReservation(confirmation);
            Console.Write("\nCustomer cancelled reservation based on confirmation " + confirmation);
        }

        public CarKey PickUpKeys(string confirmation)
        {
            //check with the counter if the keys are available for my reservation 
            if (_counterAgent.KeysAvailable(confirmation))
            {
                //pick up the keys from the garage attendant
                CarKey key = _counterAgent.GetCarKeyForReservation(confirmation);
                Console.WriteLine("\nCustomer gets a key {0} from the garage attendant. ",key.UniqueId);
                return key;
            }
            else
            {
                Console.WriteLine("\nCustomer is not happy that the car is not ready yet.");
                return null;
            }
        }

        public void ReturnRental(CarKey key)
        {
            Console.WriteLine("\nCustomer returns the car to the garage attendant.");
            _garageAttendant.AcceptRentalReturn(key);
        }

    }
}


//Actor(s)			System Boundary: Action
//------------------------------------------------------------

//Car Renter/			Reservations: Make a reservation
//Rental Agent		

//------------------------------------------------------------

//Car Renter			Reservations: Cancel reservation
//Rental Agent

//------------------------------------------------------------

//Rental Agent		Garage: Check if car keys are ready

//------------------------------------------------------------

//Car Renter			Garage: Pick up the keys

//------------------------------------------------------------

//Car Renter			Garage: Return the rental

//------------------------------------------------------------

//Garage Attendant	Garage: Receive car keys for servicing

//------------------------------------------------------------

//Garage Attendant	Garage: Make keys avalable

//------------------------------------------------------------
