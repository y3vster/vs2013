﻿//Yevgeni Kamenski
//P1.cs
//8/24/2015

using System;

namespace BC3_P1
{
    class P1
    {
        static void Main(string[] args)
        {
            bool play = true;   //flag to play the game
            while (play)
            {
                Console.WriteLine("Welcome to the card game. Press any key to play and to take turns.");
                PlayGame();
                Console.Write(@"Enter 'y' to play again: ");
                play = Console.ReadLine().ToLower() == "y";
            }

            Console.WriteLine("Thanks for playing. Press any key to exit..");
            Console.ReadKey(true);
        }

        private static void PlayGame()
        {
            Queue[] plQueues = {new Queue(), new Queue()};  //player queues
            string[] names = { "Alice", "Bob" };          //player names

            Stack deal = new Stack(52);     //deal deck 
            Stack discard = new Stack();    //discard deck

            FillRandom(deal);
            InitialPlayerDeal(plQueues, deal);
            discard.Push(deal.Pop());

            int curPl = 0;              //index of current player
            bool keepPlaying = true;    //flag to keep playing
            while (keepPlaying)
            {
                Console.ReadKey(true);

                int playCard = plQueues[curPl].Peek();  //play card
                int discardTop = discard.Peek();        //discard top card

                Console.WriteLine("{0} plays {1} against {2}.",
                    names[curPl], playCard, discardTop);

                discard.Push(plQueues[curPl].Dequeue());

                int takeCards = 0;  //how many cards to take
                switch (playCard.CompareTo(discardTop))
                {
                    case 1:
                        if (plQueues[curPl].IsEmpty())
                        {
                            Console.WriteLine("{0} has an empty hand and wins.",
                                names[curPl]);
                            keepPlaying = false;
                        }
                        break;
                    case 0:
                        takeCards = 1;
                        break;
                    case -1:
                        takeCards = 2;
                        break;
                }

                for (int i = 0; i < takeCards; i++)
                {
                    if (deal.IsEmpty())
                    {
                        TurnOverDiscard(discard, deal);
                    }
                    Console.WriteLine("{0} takes {1} from the deal deck.", 
                        names[curPl], deal.Peek());
                    plQueues[curPl].Enqueue(deal.Pop());
                }

                curPl = ++curPl % plQueues.Length;
            }
        }

        private static void InitialPlayerDeal(Queue[] queues, Stack deal)
        {
            int index = 0;
            for (int i = 0; i < 7 * queues.Length; i++)
            {
                queues[index].Enqueue(deal.Pop());
                index = ++index % queues.Length;
            }
        }

        private static void TurnOverDiscard(Stack discard, Stack deal)
        {
            int discardTop = discard.Pop();
            while (!discard.IsEmpty())
            {
                deal.Push(discard.Pop());
            }
            discard.Push(discardTop);
        }

        private static void FillRandom(Stack deck)
        {
            Random r = new Random();
            int[] randomArr = new int[52];
            int arrIndex = 0;
            for (int i = 1; i <= 13; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    randomArr[arrIndex++] = i;
                }
            }
            for (int i = 0; i < 51; i++)
            {
                int randPosn = r.Next(i + 1, 52);
                int temp = randomArr[randPosn];
                randomArr[randPosn] = randomArr[i];
                randomArr[i] = temp;
            }
            for (int i = 0; i < 52; i++)
            {
                deck.Push(randomArr[i]);
            }
        }
    }
}
//The game company, Silly Little Games, has come up with a new card game for you to simulate. It uses a special set of cards that contain the numbers 1 to 13 and there are four copies of each card in the deck. The first person to play all the cards in their hand wins. The game is played as follows:
//•The cards are shuffled and placed into a stack
//•Each player is dealt 7 cards from the stack in a round-robin fashion and their cards are placed into their queue
//•The next card in the deal stack is placed into the discard stack
//•For their turn, each player plays the next card in his/her queue. 
//If the card the player plays is HIGHER in number than the one on the top of the discard stack, the player's turn is over. 
//If the card the player plays is EQUAL in number to the one on the top of the discard stack, the player must then take one card from the deal stack and the player's turn is over. 
//If the player's card is LOWER in number than the one on the discard stack, the player must take two cards from the deal stack and the player's turn is over.
//•If the deal stack runs out of cards, "turn over" the discard stack and continue the game. Note that you must keep the same card on the top of the discard stack, so you will need to hold onto that card and push it back onto the discard stack before continuing the game.
//•The first player to run out of cards wins the game.
//•Offer to repeat the game as many times as desired.

//Notes:
//•There are only two players.
//•You may ask for names or simply call them by whatever names your game chooses
//•Remember to design your user interface carefully.
//•You must use a stack for the shuffled cards, a stack for the discard pile and a queue for each player - all of type int.
//•If your stack and queue packages from Lab 4 and Lab 5 didn't work, fix them before using them in this project. 
//•Call your files "Stack.cs", "Queue.cs", and "P1.cs". Compress files and submit zip file (i.e. P1.zip) on Canvas. 
