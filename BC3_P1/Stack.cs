﻿//Yevgeni Kamenski
//Stack.cs
//8/24/2015

namespace BC3_P1
{
    class Stack
    {
        private int[] _array;    //elements in a stack
        private int _size;       //num elements in the stack
        const int CAPACITY_DEFAULT = 10;    //default minimum capacity

        /// <summary>Default ctor. </summary>
        public Stack() : this(CAPACITY_DEFAULT) { }
        /// <summary>Ctor with a user-supplied initial capacity. </summary>
        public Stack(int initialCapacity)
        {
            if (initialCapacity < CAPACITY_DEFAULT)
            {
                initialCapacity = CAPACITY_DEFAULT;
            }
            _array = new int[initialCapacity];
        }

        /// <summary>Copy ctor. </summary>
        public Stack(Stack argStack)
        {
            _size = argStack._size;
            _array = new int[argStack._array.Length];
            Copy(argStack._array, 0, _array, 0, argStack._size);
        }


        /// <summary>returns true if the stack is empty</summary>
        public bool IsEmpty()
        {
            return _size == 0;
        }

        /// <summary>Push value into the stack. </summary>
        public void Push(int value)
        {
            if (_size == _array.Length)
            {
                //double the array capacity
                int[] largerArray = new int[2 * _array.Length];
                Copy(_array, 0, largerArray, 0, _size);
                _array = largerArray;
            }
            _array[_size++] = value;
        }

        /// <summary>Returns the value of the element at the top of the stack without removing it. </summary>
        public int Peek()
        {
            return _array[_size - 1];
        }

        /// <summary>Remove the value from the top of the stack and return it. </summary>
        public int Pop()
        {
            return _array[--_size];
        }

        private void Copy(int[] src, int srcIndex, int[] dest, int destIndex,
            int length)
        {
            for (int i = 0; i < length; i++)
            {
                dest[srcIndex + i] = src[destIndex + i];
            }
        }
    }
}

