﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public class LinkedList<TElement> where TElement: IComparable<TElement>
    {
        private class Node
        {
            public TElement Data { get; set; }
            public Node Next { get; set; }

            public Node(TElement data, Node next)
            {
                this.Data = data;
                this.Next = next;
            }

            public Node(TElement data)
            {
                this.Data = data;
                this.Next = null;
            }

            public override string ToString()
            {
                return Data.ToString();
            }
        }

        private Node head;
        public int Length
        {
            get
            {
                int len = 0;
                Node curNode = head;
                while (curNode != null)
                {
                    curNode = curNode.Next;
                    len++;
                }
                return len;
            }
        }
        public LinkedList() { head = null; }


        /// <summary>Get element at position</summary>
        public TElement GetNth(int positIndex)
        {
            Node curNode = head;
            for (int i = 0; i < positIndex; i++)
            {
                if (curNode.Next == null)
                {
                    throw new ArgumentOutOfRangeException("positIndex");
                }
                curNode = curNode.Next;
            }
            return curNode.Data;
        }

        /// <summary>Insert node at specific position</summary>
        public void InsertNth(int positionIndex, TElement nodeData)
        {   
            Node nodeBeforeIndex = head;
            for (int i = 1; i <= positionIndex - 1; i++)
            {
                if (nodeBeforeIndex == null)
                {
                    throw new ArgumentOutOfRangeException("positionIndex");
                }
                nodeBeforeIndex = nodeBeforeIndex.Next;
            }

            Node nodeAtIndex = positionIndex == 0 ? head : nodeBeforeIndex.Next;

            Node nodeToInsert = new Node(data: nodeData,
                                         next: nodeAtIndex);
            if (positionIndex == 0)
            {
                head = nodeToInsert;
            }
            else
            {
                nodeBeforeIndex.Next = nodeToInsert;
            }
        }

        /// <summary>Insert node at the correct position of a sorted list</summary>
        public void SortedInsert(TElement nodeData)
        {
            //handle cases at the head of the list
            if (head == null)
            {
                head = new Node(nodeData);
            }
            else if (nodeData.CompareTo(head.Data) <= 0)
            {
                head = new Node(data: nodeData,
                                next: head);
            }
            else
            {
                SortedInsertTailRecursion(nodeData: nodeData,
                                          nodeLeft: head,
                                          nodeRight: head.Next);
            }
        }

        /// <summary>Helper for SortedInsert</summary>
        private void SortedInsertTailRecursion(TElement nodeData, Node nodeLeft, Node nodeRight)
        {
            if (nodeRight == null)
            {
                //base case: on last element, simply append
                nodeLeft.Next = new Node(data: nodeData); //at the end, so just append
            }
            else if (nodeData.CompareTo(nodeRight.Data) <= 0)
            {
                //found a spot to insert
                nodeLeft.Next = new Node(data: nodeData, next: nodeRight);
            }
            else
            {
                //tail-recursion
                SortedInsertTailRecursion(nodeData: nodeData,
                                          nodeLeft: nodeRight,
                                          nodeRight: nodeRight.Next);

            //// Iterative equivalent of tail recursion:
            //
            //while (true)    
            //{
            //    if (nodeRight == null)  
            //    {
            //        nodeLeft.Next = new Node(data: nodeData);
            //    }
            //    else if (nodeData <= nodeRight.Data)
            //    {
            //        nodeLeft.Next = new Node(data: nodeData, next: nodeRight);
            //    }
            //    else
            //    {
            //        nodeLeft = nodeRight;
            //        nodeRight = nodeRight.Next;
            //        continue;
            //    }
            //    break;
            //}
            }
        }

        /// <summary>Removes duplicate elements in the list</summary>
        public void RemoveDuplicates()
        {
            if (head == null) { return; }

            HashSet<TElement> hashSet = new HashSet<TElement>();
            hashSet.Add(head.Data);

            Node precedingNode = head;         //node preceding the one being analyzed
            Node examinedNode = head.Next;

            while (examinedNode != null)
            {
                if (hashSet.Contains(examinedNode.Data))
                {
                    precedingNode.Next = examinedNode.Next; //seen it before, remove it
                }
                else
                {                                           //case: fresh value
                    hashSet.Add(examinedNode.Data);         //record it
                    precedingNode = examinedNode;           //set preceding node to the first encounter with a fresh value
                }
                examinedNode = examinedNode.Next;           //advance to examine next node
            }
        }

        // place new node at the head of the list
        public void Push(TElement data)
        {
            Node node = new Node(data, head);
            this.head = node;
        }

        // remove the node from the front of the list
        // (never call this method on an empty list)
        public TElement Pop()
        {
            TElement data = head.Data;
            head = head.Next;
            return data;
        }

        // place new node at the end of the list
        public void Enqueue(TElement data)
        {
            // place new node at the end of the list
            Node newNode = new Node(data);
            if (head == null)
            {
                head = newNode;
            }
            else
            {
                Traverse(head).Next = newNode;
            }
        }

        // remove the first item from the queue
        public TElement Dequeue()
        {
            TElement data = head.Data;
            head = head.Next;
            return data;
        }

        // return a reference to the last node in the list
        private Node Traverse(Node node)
        {
            if (node.Next != null)
            {
                return Traverse(node.Next);
            }
            return node;
        }

        public override string ToString()
        {
            String rtnValue = "";
            Node node = head;

            while (node != null)
            {
                rtnValue += node.ToString() + " ";
                node = node.Next;
            }

            return rtnValue.Trim();
        }



    }
}
