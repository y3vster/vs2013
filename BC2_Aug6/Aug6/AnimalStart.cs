﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    interface ILocomotion
    {
        string ModeOfLocomotion();
    }
    abstract class Animal : ILocomotion, IComparable<Animal>
    {
        public abstract String Says();
        string ILocomotion.ModeOfLocomotion()
        {
            throw new NotImplementedException();
        }

        //compare animals by their class name
        int IComparable<Animal>.CompareTo(Animal other)
        {
            return this.GetType().ToString().CompareTo(other.GetType().ToString());
        }
    }

    class Reptile : Animal, ILocomotion
    {
        public override String Says()
        {
            return "I'm a reptile";
        }

        string ILocomotion.ModeOfLocomotion()
        {
            throw new NotImplementedException();
        }
    }

    class Horse : Animal, ILocomotion
    {
        public override String Says()
        {
            return "I'm a horse";
        }
        string ILocomotion.ModeOfLocomotion()
        {
            throw new NotImplementedException();
        }
    }

    class Dog : Animal, ILocomotion
    {
        public override String Says()
        {
            return "I'm a dog";
        }
        string ILocomotion.ModeOfLocomotion()
        {
            throw new NotImplementedException();
        }
    }

    class Fish : Animal, ILocomotion
    {
        public override String Says()
        {
            return "Blub blub blub";
        }
        string ILocomotion.ModeOfLocomotion()
        {
            throw new NotImplementedException();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Horse h = new Horse();
            Dog d = new Dog();
            Fish f = new Fish();
            Reptile r = new Reptile();
            Console.WriteLine("");

            Console.WriteLine("{0}", h.Says());
            Console.WriteLine("{0}", d.Says());
            Console.WriteLine("{0}", f.Says());
            Console.WriteLine("{0}", r.Says());

            Console.WriteLine("");
            Animal[] animalArray = new Animal[4];
            animalArray[0] = h;
            animalArray[1] = d;
            animalArray[2] = f;
            animalArray[3] = r;

            LinkedList<Animal> animalLinkedList = new LinkedList<Animal>();

            foreach (var a in animalArray)
            {
                Console.WriteLine("{0}", a.Says());
                animalLinkedList.Push(a);
            }

            Console.WriteLine();

            int numAnimals = animalLinkedList.Length;
            for (int i = 0; i < numAnimals; i++)
            {
                Animal poppedAnimal = animalLinkedList.Pop();
                Console.WriteLine("An animal popped out of a linked list and said {0}.", poppedAnimal.Says());
            }

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);
        }
    }
}