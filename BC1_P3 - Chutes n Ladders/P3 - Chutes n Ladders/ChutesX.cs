﻿////Yevgeni Kamenski
////ChutesX.cs (P3 Extra Credit - Chutes & Ladders)
////2015-07-08
////P3 Extra Credit - Chutes and Ladders game simulator programming assignment. 
////Keeps a "winner's list" of the names of the last 10 people to have won a game.
////The list will be read in from a file you create and written out again at the end of the program 
////(to the same file for the next time). This is a simple array of strings (not sorted and we don't 
////care how many times they won). The user should be shown the list at the end of each game. Do not 
////put the same name on twice!

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.IO;

//namespace P3___Chutes_n_LaddersX
//{
//    class ChutesX
//    {
//        const string GAME_LOGIC_FILE_PATH = @"p3input.txt"; //path to file with game logic
//        const string GAME_WINNERS_FILE_PATH = @"p3winners.txt"; //path to file with winners
//        const int PLAYERS_MIN = 2;    //min num of players
//        const int PLAYERS_MAX = 6;    //max num of players
//        const int SPIN_MIN = 1;       //spin from 
//        const int SPIN_MAX = 6;       //spin to
//        const int ERROR_SUCCESS = 0x0;        //C-style error checking for file handling
//        const int ERROR_FILE_NOT_FOUND = 0x2; //C-style file not found error code

//        static void Main(string[] args)
//        {
//            int[] boardLogic = new int[100];                //stores logic info read from file
//            int numPlayers;                                 //number of players
//            string[] playerNames;                           //player names array
//            int[] playerLocations = new int[PLAYERS_MAX];   //player locations
//            int curPlayer;                                  //current player turn
//            int curSpin;                                    //spin of the current player
//            int curPlayerLoc;                               //current player location
//            bool someoneWon = false;                        //flag to see if someone won
//            Func<int> GetASpin = SpinnerFactory(SPIN_MIN, SPIN_MAX);    //If I am allowed to init System.Random, then
//            //I am allowed to init this as well
//            Array.Clear(boardLogic, 0, 100);                //default num spaces to jump is zero
//            if (InitBoardLogicFromFile(boardLogic) == ERROR_FILE_NOT_FOUND) return; //file issues case game to exit

//            Console.WriteLine("Welcome to Chutes & Ladders game simulator.");
//            do   //do-while is semantically correct
//            {
//                SetUpPlayers(out numPlayers, out playerNames);                  //aks for # of players and names
//                curPlayer = WhoGoesFirst(numPlayers, playerNames, GetASpin);    //spin to see who goes first
//                Array.Clear(playerLocations, 0, PLAYERS_MAX);                   //start everyone at 0
//                AnnounceAndPause("All players are in the starting position, let's begin!");
//                someoneWon = false;
//                while (!someoneWon)
//                {
//                    AnnounceAndPause(playerNames[curPlayer] + " is ready to spin.");
//                    curSpin = GetASpin();
//                    curPlayerLoc = playerLocations[curPlayer] + curSpin;
//                    if (curPlayerLoc > 100)
//                        curPlayerLoc = 100;
//                    Console.WriteLine("{0} spins a {1} and advances to #{2}", playerNames[curPlayer], curSpin, curPlayerLoc);
//                    ChuteOrLadderMe(ref curPlayerLoc, boardLogic, playerNames[curPlayer]);  //updates cur loc if hit a chute or ladder
//                    if (curPlayerLoc != 100)
//                    {
//                        playerLocations[curPlayer] = curPlayerLoc;      //save cur loc and
//                        curPlayer = ++curPlayer % numPlayers;           //move to next player
//                    }
//                    else
//                    {
//                        AnnounceAndPause("   *** " + playerNames[curPlayer].ToUpper() + " WINS! ***   ");
//                        someoneWon = true;
//                    }
//                }
//                ShowPrevWinners(playerNames[curPlayer]);
//            } while (AskPlayAgain());
//            AnnounceAndPause("Thanks for playing, see you again soon!");
//        }

//        //extra credit method
//        //accepts the name of the last winner and displays
//        static void ShowPrevWinners(string curGameWinner)
//        {
//            string[] winners = new string[0];   //last 10 winners
//            int arrLen;                         //size of the winner array

//            if (File.Exists(GAME_WINNERS_FILE_PATH)) 
//            {
//                winners = File.ReadAllLines(GAME_WINNERS_FILE_PATH);
//            }
//            if (!Array.Exists(winners, s => s.Equals(curGameWinner)))    //if new name, add and save
//            {
//                arrLen = winners.Length;
//                if (winners.Length<10)  //<10 entries: add to end
//                {
//                    Array.Resize(ref winners, arrLen + 1);
//                    winners[arrLen] = curGameWinner;
//                }
//                else    //10 or more entries: discard oldest
//                {
//                    Array.Copy(winners, 1, winners, 0, arrLen - 1); //shift left by one
//                    winners[arrLen - 1] = curGameWinner;            //replace the last entry
//                }
//                File.WriteAllLines(GAME_WINNERS_FILE_PATH, winners);
//            }
//            Console.WriteLine("\nLast 10 winners:");
//            Array.ForEach(winners, s => Console.WriteLine(" > " + s));
//        }

//        //prints text with a prompt to press a key to cont
//        static void AnnounceAndPause(string text)
//        {
//            Console.WriteLine();
//            Console.WriteLine(text + " (Press any key..)");
//            Console.ReadKey(true);
//        }

//        //reads the game logic file into the game logic array. Returns a C-style status code.
//        static int InitBoardLogicFromFile(int[] boardLogic)
//        {
//            string[] fileLines, lineSplits; //text read from the file
//            if (File.Exists(GAME_LOGIC_FILE_PATH))
//            {
//                fileLines = File.ReadAllLines(GAME_LOGIC_FILE_PATH);
//                for (int i = 0; i < fileLines.Length; i++)
//                {
//                    if (fileLines[i].Length > 0)
//                    {
//                        lineSplits = fileLines[i].Split(' ');
//                        if (lineSplits.Length == 2)
//                        {
//                            boardLogic[int.Parse(lineSplits[0])] = int.Parse(lineSplits[1]);
//                        }
//                    }
//                }
//                return ERROR_SUCCESS;
//            }
//            else
//            {
//                AnnounceAndPause("Error: Board logic file " + GAME_LOGIC_FILE_PATH + " was not found.\n" +
//                                 "Cur Working Dir: " + System.IO.Directory.GetCurrentDirectory());
//                return ERROR_FILE_NOT_FOUND;
//            }
//        }

//        //ask for num of players and let the user enter names
//        private static void SetUpPlayers(out int numPlayers, out string[] playerNames)
//        {
//            numPlayers = 0;
//            while (numPlayers < PLAYERS_MIN || numPlayers > PLAYERS_MAX)
//            {
//                Console.Write("How many people are playing? Enter a number 2 to 6: ");
//                numPlayers = int.Parse(Console.ReadLine());
//            }
//            Console.WriteLine();
//            Console.WriteLine("Great! Now enter player names followed by enter.");
//            playerNames = new string[numPlayers];
//            for (int i = 0; i < numPlayers; i++)
//            {
//                Console.Write("Player {0}: ", i + 1);
//                playerNames[i] = Console.ReadLine();
//            }
//        }

//        //determine which player goes first and return index of the player num
//        private static int WhoGoesFirst(int numPlayers, string[] playerNames, Func<int> getASpin)
//        {
//            bool spinTie = false;       //flag if found who goes first
//            int curSpin;                //current spin
//            int curMax = 0;             //current max spin
//            int curMaxPlayer = -1;      //who the cur max spinner person is
//            //do-while is appropriate - understood that will loop at least once
//            do
//            {
//                AnnounceAndPause("Let's spin to see who goes first.");
//                curMax = 0;
//                for (int i = 0; i < numPlayers; i++)
//                {
//                    curSpin = getASpin();
//                    Console.WriteLine("{0} spins {1}.", playerNames[i], curSpin);
//                    if (curSpin > curMax)
//                    {
//                        spinTie = false;
//                        curMax = curSpin;
//                        curMaxPlayer = i;
//                    }
//                    else if (curSpin == curMax)
//                    {
//                        spinTie = true;
//                    }
//                }
//                if (spinTie)
//                {
//                    Console.WriteLine("Looks like we have a tie, let's try this again.");
//                }
//                else
//                {
//                    AnnounceAndPause(playerNames[curMaxPlayer] + " gets to go first.");
//                }

//            } while (spinTie);  //again if there was a tie
//            return curMaxPlayer;
//        }

//        //applies a chute or ladder based on the logicArr and updates curPos
//        private static void ChuteOrLadderMe(ref int curPos, int[] logicArr, string curPlayerName)
//        {
//            if (curPos < 100)
//            {
//                int curLogicArrVal = logicArr[curPos];
//                curPos += curLogicArrVal;
//                if (curLogicArrVal > 0)
//                {
//                    Console.WriteLine("Nooice! {0} climbs a ladder and moves up #{1}.", curPlayerName, curPos);
//                }
//                else if (curLogicArrVal < 0)
//                {
//                    Console.WriteLine("Not good. {0} goes down a chute and back to #{1}", curPlayerName, curPos);
//                }
//            }
//        }

//        //ask user to play again and return T/F based on the response
//        static bool AskPlayAgain()
//        {
//            Console.WriteLine("\nDo you want to play again? (y/n)\n");
//            while (true)
//            {
//                char keyPressed = Console.ReadKey(true).KeyChar;    //don't need outside the cur scope
//                //only accept y/n to prevent accidentally exiting the game
//                if ((keyPressed == 'y') || (keyPressed == 'Y'))
//                {
//                    return true;
//                }
//                else if ((keyPressed == 'n') || (keyPressed == 'N'))
//                {
//                    return false;
//                }
//            }
//        }

//        //closure of a spinner instance
//        static Func<int> SpinnerFactory(int minSpin, int maxSpin)
//        {
//            Random rm = new Random();
//            return () => rm.Next(minSpin, maxSpin + 1);
//        }
//    }
//}

////Extra credit will not be graded unless the original assignment receives at least 85%.


////This extra credit is worth 10 points.

////Call your source file "Chutesx.cs" and submit by dropping the file into this drop box.