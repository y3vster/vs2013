﻿////Yevgeni Kamenski
////Lab8.cs (Array Distribution)
////2015-07-06

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.IO;

//namespace lab8___Array_Distribution
//{
//    class Lab8
//    {
//        const string PATH = "lab8input.txt";           //file to read
//        const int ARRAY_MAX_VAL = 30;                  //assignment assumption of max # of elements
//        static int[] evnNum = new int[ARRAY_MAX_VAL];  //stores even numbers
//        static int evnIndex = 0;                       //even array index
//        static int[] oddNum = new int[ARRAY_MAX_VAL];  //stores odd numbers
//        static int oddIndex = 0;                       //odd array index
//        static int[] negNum = new int[ARRAY_MAX_VAL];  //stores negative numbers
//        static int negIndex = 0;                       //negative array index

//        static void Main(string[] args)
//        {
//            Console.WriteLine("This program reads integers from a file into arrays and displays them.");
//            ReadFileIntoArrays();
//            DispArrContents();
//            Console.WriteLine("Press any key to exit..");
//            Console.Read();
//        }

//        ///--------------------------------------------
//        /// <summary>Displays array contents.</summary>
//        static void DispArrContents()
//        {
//            var dispFormat = new[]{    //select vars to output
//                new {arr = evnNum,     //array variable to output
//                     size = evnIndex,  //size of the data entry
//                     name = "evnNum"}, //variable name
//                new {arr = oddNum,
//                     size = oddIndex,
//                     name = "oddNum"},
//                new {arr = negNum,
//                     size = negIndex,
//                     name = "negNum"}
//            };

//            //I could make 3 separate loops, but this is more fun :)
//            Array.ForEach(dispFormat, x => {
//                for (int i = 0; i < x.size; i++)
//                {
//                    Console.WriteLine("{0}[{1}] = {2}", x.name, i, x.arr[i]);
//                }
//            });
//        }

//        ///--------------------------------------------------------------------
//        /// <summary>Reads a file in PATH, and stores values in appropriate
//        ///     arrays.</summary>
//        static void ReadFileIntoArrays()
//        {
//            StreamReader f; //file stream handle
//            String line;    //current text line read from file

//            if (File.Exists(PATH))
//            {
//                f = new StreamReader(PATH);
//                while ((line = f.ReadLine()) != null)
//                {
//                    if (line.Length > 0)
//                    {
//                        StoreNum(int.Parse(line));
//                    }
//                }
//                f.Close();
//            }
//            else
//            {
//                Console.WriteLine("File {0} not found.", PATH);
//            }
//        }

//        ///--------------------------------------------------------------------
//        /// <summary>Stores a number read from a file in an appropriate array.</summary>
//        /// <param name="num">Integer read from a file.</param>
//        static void StoreNum(int num)
//        {
//            if (num < 0)
//            {
//                negNum[negIndex] = num;
//                negIndex++;
//            }
//            else if ((num & 0x1) == 1)
//            {
//                oddNum[oddIndex] = num;
//                oddIndex++;
//            }
//            else if (num != 0)
//            {
//                evnNum[evnIndex] = num;
//                evnIndex++;
//            }
//        }


//    }
//}


////Assignment description:
////Write a short program in C# to read in a list of integers from a file. Each integer should be placed in one of three arrays – the positive even numbers go into an array called evenNum, the positive odd numbers into an array called oddNum and the negatives into an array called negNum. Print the contents (with indexes) of each array – properly labeled – after the entire file has been read (print only those values you input from the file – not the unused elements). Note that 0 is not positive or negative and should not be stored anywhere, yet is a valid value in the file.
////You may assume the file contains no more than 30 integers – however, they may all be even, odd or negative and the file may contain less than 30 integers.