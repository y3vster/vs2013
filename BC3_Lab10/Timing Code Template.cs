﻿using System;
using System.Diagnostics;
using System.Threading;

namespace BC3_Lab10
{
    class Lab10
    {
        private static Stopwatch _stopwatch;    //used for timing

        /// <summary>Initializes the timer </summary>
        private static void InitStopwatch()
        {
            _stopwatch = new Stopwatch();


            //Frequency attribute is in ticks/sec
            double muSecPerTick = 1000000.0 / Stopwatch.Frequency;

            Console.WriteLine("Stopwatch is high resolution: " + Stopwatch.IsHighResolution);
            Console.WriteLine("Stopwatch resolution is ~ {0:F2} μs", muSecPerTick);

            //use second processor core for the test
            Process.GetCurrentProcess().ProcessorAffinity = new IntPtr(2);

            //prevent "Normal" processes and threads from forcing me into a context switch mid-test
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
        }

        static void Main(string[] args)
        {
            InitStopwatch();

            //first call to the function and timer are really slow
            //so discard the first measurement
            _stopwatch.Start();
            foo(10, 10);
            _stopwatch.Stop();

            for (int n = 2; n < 128; n *= 2)
            {
                //Perform garbage collection and wait until it finishes
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //timing section
                _stopwatch.Reset();
                _stopwatch.Start();

                foo(n, 10);

                _stopwatch.Stop();

                //results
                double microSecs = _stopwatch.Elapsed.TotalMilliseconds * 1000.0;
                Console.WriteLine("N: {0,5} = {1:F1} μs (average)", n, microSecs);

            }

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);
        }

        static void foo(int n, int val)
        {
            int b = 1, c = 0;
            for (int j = 4; j < n; j++)
            {
                for (int i = 0; i < j; i++)
                {
                    b = b * val;
                    for (int k = 0; k < n; ++k)
                        c = b + c;
                }
            }
        }
    }
}
