﻿
//    //Run the program written in Lab 8. Double the size of your input on each run (i.e. 25, 50, 100, 200, 400, 800, 1600, 3200).
//    //Plot the T(N) vs input size N. The easiest way to do this is to enter the values in Excel and use the Insert Chart->Scatter Plot.
//    //Take the ratio of the times. See handout for example.
//    //Take the log of the ratio to find b.
//    //Use power-law relationship, a N b, to estimate a.
//    //      a = Time / (N^b)
//    //Use values of a and b to find T(N) 
//    //Run the program for N=6400 and record the time in seconds. Compare to the result of T(6400).


//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.IO;
//using System.Linq;
//using System.Text.RegularExpressions;
//using System.Threading;

//namespace BC3_Lab10
//{
//    class Lab8
//    {
//        static void foo(int n, int val)
//        {
//            int b = 1, c = 0;
//            for (int j = 4; j < n; j++)
//            {
//                for (int i = 0; i < j; i++)
//                {
//                    b = b * val;
//                    for (int k = 0; k < n; ++k)
//                        c = b + c;
//                }
//            }
//        }

//        private static Stopwatch _stopwatch;    //used for timing foo()

//        static void Main(string[] args)
//        {
//            InitStopwatch();

//            Console.Write("Start value? ");
//            int start = int.Parse(Console.ReadLine());

//            Console.Write("Num steps? ");
//            int steps = int.Parse(Console.ReadLine());

//            Console.Write("How many tries for each step? ");
//            int numTries = int.Parse(Console.ReadLine());

//            //each n entry has a list of multiple time trials
//            List<int> nRecs = new List<int>(steps);
//            List<List<double>> tRecs = new List<List<double>>(steps);

//            //The first call is much slower than others, so discard it
//            double discardedTime = GetFooMicroseconds(10);

//            int n = start;  //func input
//            for (int i = 0; i < steps; i++)
//            {
//                nRecs.Add(n);

//                List<double> times = new List<double>(numTries);
//                for (int j = 0; j < numTries; j++)
//                {
//                    times.Add(GetFooMicroseconds(n));
//                }

//                tRecs.Add(times);
//                Console.WriteLine("N: {0,5} = {1:F1} μs (average)", n, times.Average());

//                n *= 2;
//            }

//            Console.Write("Write to .CSV? (Y/N) ");
//            if ("y" == Console.ReadLine().ToLower())
//            {
//                //wow, functional programming is the best! 
//                IEnumerable<string> results =
//                    from tuple in Enumerable.Zip(nRecs, tRecs, Tuple.Create)
//                    let fInput = tuple.Item1
//                    from time in tuple.Item2
//                    select fInput + ", " + time;

//                Console.Write("File name? ");
//                File.WriteAllLines(Console.ReadLine() + ".csv", results);
//            }

//            Console.WriteLine("Press any key to exit..");
//            Console.ReadKey(true);
//        }

//        private static void InitStopwatch()
//        {
//            _stopwatch = new Stopwatch();
//            double muSecPerTick = 1000000.0 / (double)Stopwatch.Frequency; //Freq is ticks/sec
//            Console.WriteLine("Stopwatch resolution is ~ {0:F2} μs", muSecPerTick);
//            //use second core for the test
//            Process.GetCurrentProcess().ProcessorAffinity = new IntPtr(2);
//            //prevent "Normal" processes and threads from forcing me into a context switch mid-test
//            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
//            Thread.CurrentThread.Priority = ThreadPriority.Highest;
//        }

//        private static double GetFooMicroseconds(int n)
//        {
//            //wait for CLR to get all caught up
//            GC.Collect();
//            GC.WaitForPendingFinalizers();

//            _stopwatch.Reset();
//            _stopwatch.Start();
//            foo(n, 10);
//            _stopwatch.Stop();

//            return _stopwatch.Elapsed.TotalMilliseconds * 1000.0;
//        }


//    }
//}
