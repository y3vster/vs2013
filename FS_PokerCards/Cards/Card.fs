﻿module Card

open System

type CardRank = 
    | Two = 0
    | Three = 1
    | Four = 2
    | Five = 3
    | Six = 4
    | Seven = 5
    | Eight = 6
    | Nine = 7
    | Ten = 8
    | Jack = 9
    | Queen = 10
    | King = 11
    | Ace = 12

type CardSuit = 
    | Clubs = 0
    | Diamonds = 1
    | Hearts = 2
    | Spades = 3

module CardHelper = 
    let cardRankStrings = Enum.GetNames(typeof<CardRank>)
    let cardSuitStrings = Enum.GetNames(typeof<CardSuit>)
    let GetRankFullName(r : CardRank) = cardRankStrings.[int r]
    let GetSuitFullName(s : CardSuit) = cardSuitStrings.[int s]
    let rankNums = [ 2..10 ] |> Seq.map string
    let rankNames = "JQKA" |> Seq.map string
    let symbolsRankDisp = Seq.append rankNums rankNames |> Seq.toArray
    let symbolsSuitNames = "CDHS" |> Seq.map string |> Seq.toArray
    
    let symbolsSuitDisp = 
        "♣♦♥♠"
        |> Seq.map string
        |> Seq.toArray
    
    let GetRankShort(r : CardRank) = symbolsRankDisp.[int r]
    let GetSuitShort(s : CardSuit) = symbolsSuitDisp.[int s]
    let GetRankSuitFmStr (s : string) = 
        let sRankLen = s.Length - 1
        
        let rankStr = s.Substring(0, sRankLen)
        let suitStr = s.Substring(sRankLen, 1)
        
        let rankInt : int = Array.IndexOf(symbolsRankDisp, rankStr)
        let suitInt : int = Array.IndexOf(symbolsSuitNames, suitStr)

        if (rankInt = -1 || suitInt = -1) then 
            failwith (sprintf "%s is not a valid card" s)

        enum<CardRank>(rankInt), enum<CardSuit>(suitInt)



[<Struct>]
[<CustomEquality; CustomComparison>]
type Card = 
    val R : CardRank
    val S : CardSuit
//    new(r, s) =     //constructor that takes rank and suit
//        { R = r
//          S = s }
    new ((r , s) : (CardRank*CardSuit)) =
        {R = r
         S = s}
    new(s : string) =
        Card(CardHelper.GetRankSuitFmStr(s))
    override c.ToString() = (CardHelper.GetRankShort c.R) + (CardHelper.GetSuitShort c.S)
    interface IEquatable<Card> with
        member c.Equals (other:Card) =
            c.R = other.R && c.S = other.S
    interface IComparable<Card> with
        member c.CompareTo (other:Card) =
            compare (int c.R * 15 + int c.S) (int other.R * 15 + int other.S)


let rand = Random()
let randRanks : seq<CardRank> = 
    seq { 
        while true do
            yield enum<CardRank>(rand.Next(0, 13))
    }
let randSuits : seq<CardSuit> = 
    seq { 
        while true do
            yield enum<CardSuit>(rand.Next(0, 4))
    }
let randCardStream = (randRanks, randSuits) ||> Seq.map2 (fun r s -> new Card((r,s)))
