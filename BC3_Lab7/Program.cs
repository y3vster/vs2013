﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BC3_Lab7
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("1+2+3 = " + Sum(3));
            Console.WriteLine("Min [3,2,7,4,2,1,5,8]=" + FindMin(new int[] { 3, 2, 7, 4, 2, 1, 5, 8 }, 8));
            Console.WriteLine("Sum [2,7,3]=" + FindSum(new int[] { 2, 7, 3 }, 3));
            Console.WriteLine("RACECAR is palindrome=" + IsPalindrome("RACECAR".ToCharArray(), "RACECAR".Length));
            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);
        }

        //return the sum 1+ 2+ 3+ ...+ n
        private static int Sum(int n)
        {
            if (n == 1)
            {
                return 1;
            }
            return n + Sum(n - 1);
        }


        //return the minimum element in a[]
        private static int FindMin(int[] a, int n)
        {
            if (n == 1)
            {
                return a[0];
            }
            int top = a[n - 1];
            int restMin = FindMin(a, n - 1);
            if (top < restMin)
            {
                return top;
            }
            return restMin;
        }


        //return the sum of all elements in a[]
        private static int FindSum(int[] a, int n)
        {
            if (n == 1)
            {
                return a[0];
            }
            return a[n - 1] + FindSum(a, n - 1);
        }


        //returns 1 if a[] is a palindrome, 0 otherwise
        private static int IsPalindrome(char[] a, int n)
        {
            if (n < 2)
            {
                return 1;
            }
            if (a[0] == a[n - 1])
            {
                char[] newArr = new char[n - 2];
                Array.Copy(a, 1, newArr, 0, n - 2);
                return 1 * IsPalindrome(newArr, n - 2);
            }
            return 0;
        }
    }
}
