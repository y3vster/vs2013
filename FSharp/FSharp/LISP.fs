﻿module LISP

let (|>) value func =
  let result = func value
  result

type Token =
    | LPAR
    | RPAR
    | STRING_LITERAL of string
    | NUM_LIT_INT of int
    | NUM_LIT_DBL of double
    | QUOTED of Token
    | OTHER

let (|IsQuoted|_|) (str: string) = if str.[0].Equals('\'') then Some(str.Substring(1)) else None

let (|Integer|_|) (str: string) =
    let mutable intVal = 0
    if System.Int32.TryParse(str, &intVal) then Some(intVal)
    else None

let (|Double|_|) (str: string) =
    let mutable floatVal = 0.0
    if System.Double.TryParse(str, &floatVal) then Some (floatVal)
    else None

let parseNumeric str =
    match str with
    | Integer i -> NUM_LIT_INT i
    | Double i -> NUM_LIT_DBL i

let textQuote text:string=
    match text with 
    | IsQuoted quoted -> sprintf "quoted text: %s" quoted 
    | _ -> sprintf "%s : no quote" text


let tokenize (s: string) =
    match 
    