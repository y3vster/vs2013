﻿////quantified boolean formulae (QBF)
////tautology - something that is true regardless of primitive interpretation
////satisfiable - at least one interpretation for which a formula is true
//
////ex: if it's snowing, then it's cold
////Implies(Var("snowing"), Var("cold"))
//
////[<AutoOpen>]
module AutoOpenModule
//
////#if DEBUG
//let (|>) value func =
//  let result = func value
//  result
////#endif
//
//System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
//
//type Var = string
//
//type Prop = 
//    | And of Prop * Prop
//    | Var of Var
//    | Not of Prop
//    | Exists of Var * Prop
//    | False
//
//let True = Not False
//let Or(p, q) = Not(And(Not(p), Not(q)))
//let Iff(p, q) = Or(And(p, q), And(Not(p), Not(q)))
//let Implies(p, q) = Or(Not(p), q)
//let Forall(v, p) = Not(Exists(v, Not(p)))
//
//let (&&&) p q = And(p, q)
//let (|||) p q = Or(p, q)
//let (~~~) p = Not p
//let (<=>) p q = Iff(p, q)
//let (===) p q = (p <=> q)
//let (==>) p q = Implies(p, q)
//let (^^^) p q = Not (p <=> q)
//
//let var (nm: Var) = Var nm
//
////used for generating fresh variables
//let fresh =
//    let count = ref 0
//    fun nm -> incr count; (sprintf "_%s%d" nm !count : Var)
//
////------------------------------------------------------------
////evaluating natively
//
//let rec eval (env: Map<Var, bool>) inp =
//    match inp with
//    | Exists(v, p)  -> eval (env.Add(v, false)) p || eval (env.Add(v, true)) p
//    | And(p1, p2)   -> eval env p1 && eval env p2
//    | Var v         -> if env.ContainsKey(v) then env.[v]
//                       else failwithf "env didn't contain a value for %A" v
//    | Not p         -> not (eval env p)
//    | False         -> false
//
//
////extracts the variables only
//let rec support f =
//    match f with
//    | And(x, y) -> Set.union (support x) (support y)
//    | Exists(v, p) -> (support p).Remove(v)
//    | Var p         -> Set.singleton p
//    | Not x         -> support x
//    | False         -> Set.empty
//
//let rec cases supp =
//    seq {
//        match supp with
//        | [] -> yield Map.empty
//        | v :: rest ->
//            yield! rest |> cases |> Seq.map (Map.add v false)
//            yield! rest |> cases |> Seq.map (Map.add v true)
//    }
//
//let truthTable x =
//    x 
//    |> support 
//    |> Set.toList 
//    |> cases 
//    |> Seq.map (fun env -> env, eval env x)
//
//let satisfiable x =
//    x |> truthTable |> Seq.exists (fun (env, res) -> res)
//
//let tautology x =
//    x |> truthTable |> Seq.forall (fun (env, res) -> res)
//
//let tautologyWithCounterExample x =
//    x |> truthTable |> Seq.tryFind (fun (env, res) -> not res)
//    |> Option.map fst
//
//let printCounterExample = function
//    | None      -> printfn "tautology verified OK"
//    | Some env  -> printfn "tautology failed on %A" (Seq.toList env)
//
////display logic
////-------------------------------------------------------------
//
//let stringOfBit b = if b then "T" else "F"
//
//let stringOfEnv env =
//    Map.fold (fun acc k v -> sprintf "%s=%s;" k (stringOfBit v) + acc) "" env
//
//let stringOfLine (env, res) =
//    sprintf "%20s %s" (stringOfEnv env) (stringOfBit res)
//
//let stringOfTruthTable tt =
//    "\n" + (tt |> Seq.toList |> List.map stringOfLine |> String.concat "\n")
//
//let printTT tt =
//    tt |> stringOfTruthTable |> printfn "%s"
//
////fsi.AddPrinter(fun tt -> tt |> Seq.truncate 20 |> stringOfTruthTable)
//
//truthTable(var "x" &&& var "y") |> printTT
//
//let sumBit x y = (x ^^^ y)
//let carryBit x y = (x &&& y)
//let halfAdder x y sum carry =
//    (sum === sumBit x y) &&&
//    (carry === carryBit x y)
//
//let fullAdder x y z sum carry =
//    let xy = (sumBit x y)
//    (sum === sumBit xy z) &&&
//    (carry === (carryBit x y ||| carryBit xy z))
//
//let twoBitAdder (x1, x2) (y1, y2) (sum1, sum2) carryInner carry =
//    halfAdder x1 y1 sum1 carryInner &&&
//    fullAdder x2 y2 carryInner sum2 carry
//
//type bit = Prop
//type bitvec = bit[]
//
//let Lo : bit = False
//let Hi : bit = True
//let vec n nm:bitvec = Array.init n (fun i -> var (sprintf "%s%d" nm i))
//let bitEq (b1: bit) (b2: bit) = (b1 <=> b2)
//let AndL l = Seq.reduce (fun x y -> And(x, y)) l
//let vecEq (v1: bitvec) (v2: bitvec) = AndL (Array.map2 bitEq v1 v2)
//
//
//System.Console.ReadKey(true) |> ignore