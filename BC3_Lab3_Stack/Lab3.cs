﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3Stack
{
    class Lab3
    {
        static void Main(string[] args)
        {
            Stack stack = new Stack(12);
            Console.WriteLine("Created a stack with initial size of 12.");

            Console.WriteLine("stack.IsEmpty() returns: " + stack.IsEmpty());

            //create 16 random elements
            Random rm = new Random();
            IList<int> randomElements = Enumerable.Range(1, 16).Select(i => rm.Next(1, 100)).ToList();
            Console.WriteLine("Created 16 random elements: " + string.Join(", ",randomElements));

            Console.Write("Calling stack.Push() on every element: ");
            foreach (int randomElement in randomElements)
            {
                stack.Push(randomElement);
                Console.Write(randomElement + " ");
            }
            Console.WriteLine();

            Console.WriteLine("stack.IsEmpty() returns: "+ stack.IsEmpty());
            Console.WriteLine("stack.Peek() returns: " + stack.Peek());

            Console.Write("Calling stack.Pop() 16 times: ");
            for (int i = 0; i < 16; i++)
            {
                Console.Write(stack.Pop() + " ");
            }
            Console.WriteLine();

            Console.WriteLine("stack.IsEmpty() returns: " + stack.IsEmpty());

            Console.Write("calling stack.Pop() on an empty stack generates an exception? ");
            try
            {
                int temp = stack.Pop();
                Console.WriteLine("No.");
            }
            catch (Exception)
            {
                Console.WriteLine("Yes.");
            }
            
            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);
        }
    }
}

//For this assignment you will provide a Stack package. You will supply a driver program to test each function of the package.

//Specifically, in C#:

//1. Stack package: This package provides a dynamic array implementation of a stack of integers. It must protect the data from misuse and provide all standard public stack methods, including push, pop, and a boolean method that tests for the empty stack. A “top” method is optional. You should add any other private methods needed in the class, but no other public methods are allowed. Be sure to include a copy constructor. Name the class Stack.

//2. Driver program: This program tests all of the functions provided by the Stack package. The user interface is minimal, but clear. Exactly how the tests are conducted is your decision, but the tests and the results should be clear to the user.

//Files must be called "Stack.cs" and "Lab3.cs". Compress files and submit zip file (i.e. Lab3.zip) on Canvas.
