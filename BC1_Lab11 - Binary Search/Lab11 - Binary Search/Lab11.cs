﻿//Yevgeni Kamenski
//lab11.cs  (Binary Search)
//2015-07-13

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Remoting.Messaging;

namespace Lab11___Binary_Search {
    class Lab11 {
        const string FILE_PATH = @"lab11input.txt";
        const int ARRAY_MAX_SIZE = 30;          //from assignment description
        const int ERROR_SUCCESS = 0;            //file successfully read 
        const int ERROR_FILE_NOT_FOUND = 2;     //file not found
        const int SEARCH_NOT_FOUND = -1;        //search returns this if not found
        static void Main(string[] args)
        {
            int[] fileArr = new int[ARRAY_MAX_SIZE];  //array of numbers read from file
            int fileArrSize = 0;                      //num elements in array read from file
            string userInput;                         //input from the user
            int searchTarget;                         //value to search
            int searchResult;                         //result of binary search
            bool askUser = true;                      //flag to repeat user prompt

            if(PopulateArrFmFile(ref fileArr, ref fileArrSize, FILE_PATH) != ERROR_SUCCESS) {
                Console.WriteLine("Error reading input file, exiting..");
                Console.ReadKey(true);
                return;
            }

            SelectionSort(ref fileArr, fileArrSize);

            Console.WriteLine("This program reads an array from a file and uses binary search to find values.\n");
            while(askUser) {
                Console.Write("Enter a value to search or \'q\' to exit: ");
                userInput = Console.ReadLine();
                if(!userInput.ToLower().Equals("q")) {
                    searchTarget = int.Parse(userInput);
                    searchResult = BinarySearch(fileArr, fileArrSize, searchTarget);
                    if(!(searchResult == SEARCH_NOT_FOUND)) {
                        Console.WriteLine("{0} was found at index {1}.\n", searchTarget, searchResult);
                    }
                    else {
                        Console.WriteLine("{0} was not found.\n", searchTarget);
                    }
                }
                else    //user entered 'q'
                {
                    askUser = false;
                }
            }
        }

        //reads int values from file into [out] array. returns c-style error code.
        static int PopulateArrFmFile(ref int[] arrBuf, ref int arrSize, string filePath)
        {
            StreamReader f; //file stream
            string line;    //current line read from file

            if(!File.Exists(filePath))
                return ERROR_FILE_NOT_FOUND;

            f = new StreamReader(filePath);
            arrSize = 0;
            while((line = f.ReadLine()) != null) {
                if(line.Length > 0) {
                    arrBuf[arrSize] = int.Parse(line);
                    arrSize++;
                }
            }
            f.Close();
            return ERROR_SUCCESS;
        }

        //sorts contents of the array using selection sort. 
        static void SelectionSort(ref int[] sortArr, int arrSize = -1)
        {
            int curMinIndex;    //index of the current smallest item found;  
            int swapVar;        //used for swapping values
            if(arrSize == -1) {
                arrSize = sortArr.Length;   //get arr len if optional param not supplied   
            }
            for(int j = 0; j < arrSize - 1; j++) {
                curMinIndex = j;
                for(int i = j + 1; i < arrSize; i++) {
                    if(sortArr[i].CompareTo(sortArr[curMinIndex]) < 0) {
                        curMinIndex = i;
                    }
                }
                if(curMinIndex != j) {
                    swapVar = sortArr[curMinIndex];
                    sortArr[curMinIndex] = sortArr[j];
                    sortArr[j] = swapVar;
                }
            }
        }


        static int BinarySearch(int[] arr, int size, int target)
        {
            return BinarySearch(arr, 0, size, target);
        }

        static int BinarySearch<T>(T[] arr, int left, int right, T target) where T: IComparable<T>
        {
            if(left <= right) {
                int mid = (right + left) / 2;
                switch(arr[mid].CompareTo(target)) {
                    case 0: return mid;
                    case -1: return BinarySearch(arr, mid + 1, right, target);
                    case 1: return BinarySearch(arr, left, mid - 1, target);
                }
            }
            return -1;
        }
    }
}


//Write a C# program that reads in all the integer values from (lab11input.txt)- 
// there will be no more than 30 values - then sorts them in non-descending order 
// (ascending order with duplicates allowed). Once they are sorted, ask the user 
// for a search value, then use the binary search algorithm to return the location 
// of the first occurrence of that value within the array.

//Print the results of the search and allow the user to repeat it as many times as desired.

//Required methods: Selection Sort, Binary Search

//Call the source code file "lab11.cs" and drop into this drop box.