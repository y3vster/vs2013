﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics2D {
    class Program {
        
        static void Main(string[] args)
        {


            Random rm = new Random();
            Func<Shape> randomRectangle = () => { return new Rectangle(rm.Next(0, 100), rm.Next(0, 100), rm.Next(10, 15), rm.Next(10, 15)); };
            Func<Shape> randomCircle = () => { return new Circle(rm.Next(0, 100), rm.Next(0, 100), rm.Next(7, 15)); };

            List<Shape> randomShapes = Enumerable.Repeat(0, 10).Select(i => randomRectangle()).Concat(
                                       Enumerable.Repeat(0, 10).Select(i => randomCircle()))
                                       .OrderBy(e => rm.Next()).ToList();

            Console.WriteLine("Adding 10 random rectangles & circles in random order:\n" + new string('-', 60));

            DoublyLinkedList list = new DoublyLinkedList();
            foreach(Shape randShape in randomShapes) {
                list.Add(randShape);
                randShape.Draw();
            }

            Console.WriteLine("\nTest random points:\n" + new string('-', 60));

            var randPoints = Enumerable.Repeat(0, 20).Select(i => new {X = rm.Next(0, 100), Y = rm.Next(0, 100)});
            foreach (var pt in randPoints)
            {
                Shape shapeHit = null;
                for(int j = 0; j < list.Count; j++) {
                    Shape curShape = list.GetValue(j);
                    if(curShape.Hit(pt.X, pt.Y)) {
                        shapeHit = curShape;
                        break;
                    }
                }

                Console.Write("Pt ({0}, {1}): ", pt.X, pt.Y);
                if(shapeHit != null) {
                    shapeHit.Draw();
                }
                else {
                    Console.WriteLine("No hits");
                }
            }

            Console.WriteLine("\nPress any key to exit..");
            Console.ReadKey(true);

        }
    }
}
