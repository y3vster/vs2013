﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics2D
{
    public class Rectangle : Shape
    {
        private double _width, _height;
        public Rectangle(double centerX, double centerY, double width, double height)
            : base(centerX - width/2, centerY - height/2, centerX, centerY)
        {
            _width = width;
            _height = height;
        }

        public override bool Hit(double hitX, double hitY)
        {
            return (Math.Abs(hitX - _ulcX) <= _width) && (Math.Abs(hitY - _ulcY) <= _height);
        }
        public override double GetArea()
        {
            return _width * _height;
        }
        public override void Draw()
        {
            Console.WriteLine("Rectangle: centered on ({0}, {1}), Width={2}, Height={3}",
                _ctrX, _ctrY, _width, _height);
        }
    }
}
