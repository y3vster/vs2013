﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics2D
{
    public class DoublyLinkedList
    {
        private class Node
        {
            public Node next;
            public Node prev;

            public Shape value;

            public Node(Shape n)
            {
                next = prev = null;
                value = n;
            }

            public override String ToString()
            {
                return value.ToString();
            }
        }

        private Node head;
        private Node tail;
        private int nodeCount;

        public DoublyLinkedList()
        {
            // probably don't need this
            //head = tail = null;
        }

        // add a node to the tail of the list, initialized with value == n
        public void Add(Shape n)
        {
            add(new Node(n));
        }

        private void add(Node node) 
        {
            if (tail == null)
            {
                // add node to empty list 
                head = tail = node;
            }
            else
            {
                node.prev = tail;
                tail.next = node;
                tail = node;
            }
            nodeCount++;
        }

        // remove the node from the head of the list and return the value
        public Shape Remove()
        {
            if (head == null)
                throw new EmptyListException();

            Shape rtnValue = head.value;
            remove(head);
            return rtnValue;
        }

        private void remove(Node node)
        {
            if (node.prev == null)
                head = node.next;
            else
                node.prev.next = node.next;

            if (node.next == null)
                tail = node.prev;
            else
                node.next.prev = node.prev;

            nodeCount--;
        }

        public override String ToString()
        {
            String rtnString = "";
            for (Node node = head; node != null; node = node.next)
                rtnString += node.value + " ";

            if (head == null)
                    rtnString = "head=null, tail=null, count=0";
            else
                    rtnString += "\nhead=" + head.value.ToString() + ", tail=" + tail.value.ToString() + ", count=" + this.Count;

            return rtnString;
        }

        public bool Empty
        {
            get { return head == null; }
        }

        public int Count
        {
            get { return nodeCount; }
        }

        public Shape GetValue(int index)
        {
            if (index < 0 || index >= nodeCount)
            {
                throw new ArgumentException();
            }

            Node node = head;
            for (int i = 0; i < index; i++)
            {
                node = node.next;
            }
            return node.value;
        }

    }
}