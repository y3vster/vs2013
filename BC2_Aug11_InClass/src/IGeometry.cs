﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics2D
{
    interface IGeometry
    {
        double OriginX { get;set;}
        double OriginY { get;set;}
        //double GetOriginX();
        //double GetOriginY();
        //void SetOriginX(double x);
        //void SetOriginY(double y);
        double GetArea();
    }
}
