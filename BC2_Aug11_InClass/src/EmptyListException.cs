﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics2D
{
    public class EmptyListException : Exception 
    {
        public EmptyListException() : base("Doubly linked list is empty") { }
    }
}
