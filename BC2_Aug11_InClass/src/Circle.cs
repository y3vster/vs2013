﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics2D
{
    public class Circle : Shape
    {
        private double _radius;
        
        public Circle(double centerX, double centerY, double radius) : 
            base(ulcX: centerX - radius,
                 ulcY: centerY - radius, 
                 ctrX: centerX, 
                 ctrY: centerY)
        {
            _radius = radius;
        }
        public override bool Hit(double hitX, double hitY)
        {
            return Math.Sqrt(
                Math.Pow(hitX - _ctrX, 2) + 
                Math.Pow(hitY - _ctrY, 2)) 
                    <= _radius;
        }

        public override double GetArea()
        {
            return Math.PI * Math.Pow(_radius, 2);
        }

        public override void Draw()
        {
            Console.WriteLine("Circle at ({0}, {1}), R = {2}",
                _ctrX, _ctrY, _radius);
        }
    }
}
