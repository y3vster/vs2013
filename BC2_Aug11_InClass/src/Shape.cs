﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics2D
{
    public abstract class Shape : IColor, IGeometry 
    {
        protected int _rgb;
        int IColor.RGB { get { return _rgb; } set { _rgb = value;} }
        protected double _ctrX, _ctrY;
        double IGeometry.OriginX { get { return _ctrX;} set{ _ctrX = value;}}
        double IGeometry.OriginY { get { return _ctrY;} set{ _ctrY = value;}}
        protected double _ulcX, _ulcY;
        public Shape(double ulcX, double ulcY, double ctrX, double ctrY)
        {
            _rgb = 0;
            _ulcX = ulcX;
            _ulcY = ulcY;
            _ctrX = ctrX;
            _ctrY = ctrY;
        }
        public abstract void Draw();

        public abstract bool Hit(double hitX, double hitY);

        public abstract double GetArea();
    }
}
