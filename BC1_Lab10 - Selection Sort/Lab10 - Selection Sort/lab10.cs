﻿//Yevgeni Kamenski
//lab10.cs (Selection Sort)
//2015-07-13

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab10___Selection_Sort
{
    class Lab10
    {
        static void Main(string[] args)
        {
            string[] linesInput = File.ReadAllLines(@"lab10input.txt");
            DispArray(linesInput);
            Console.WriteLine("\n\n=====Sorting the array=====\n");
            SelectionSort(linesInput);
            DispArray(linesInput);
            Console.WriteLine("\n\nPress any key to exit..");
            Console.ReadKey(true);
        }

        //Displays array contents in console. Array elements must implement ToString().
        static void DispArray<T>(T[] lines)
        {
            Console.WriteLine("\nContents of the array:\n");
            for (int i = 0; i < lines.Length; i++)
            {
                Console.WriteLine(lines[i]);
            }
        }
        
        //Sorts array using selection sort. Elements must be comparable to each other.
        static void SelectionSort<T>(T[] sortArr) where T: IComparable
        {
            int curMinInd;    //current min index;  
            T temp;           //var used for swapping
            for (int j = 0; j < sortArr.Length - 1; j++)
            {
                curMinInd = j;
                for (int i = j+1; i < sortArr.Length; i++)
                {
                    if (sortArr[i].CompareTo(sortArr[curMinInd]) < 0)
                    {
                        curMinInd = i;
                    }
                }
                if (curMinInd != j)
                {
                    temp = sortArr[curMinInd];
                    sortArr[curMinInd] = sortArr[j];
                    sortArr[j] = temp;
                }
            }
        }

    }
}

//Write a program in C# that reads in the strings from lab10input.txt 
//(the chapter title and first paragraph of The Hobbit), -
// there will be no more than 60 values,  prints all the values in
//the order they were read, then sorts them and prints them again.

//The program must include a method for printing the values and a method for
//sorting the values. The sorting method must use the selection sort algorithm.
//Note that the string type requires special methods to compare for sorting.

//Call your source code file "lab10.cs" and submit by dropping it into this drop box.