﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Chpt7Chess
{
    /// <summary>
    /// Game model. Processes move commands from controller and returns results.
    /// </summary>
    class ChessBoard
    {
        private ChessSquare[,] sqrArr = new ChessSquare[8, 8];
        private struct RowCol { public int R, C; }

        public ChessBoard()
        {
            createBoardSquares();
            placePieces();
        }

        /// <summary></summary>
        public UISquareDisplay[] GetFullBoardState()
        {
            List<UISquareDisplay> squares = new List<UISquareDisplay>();
            for(int r = 0; r < 8; r++)
            {
                for(int c = 0; c < 8; c++)
                {
                    squares.Add(createSquareUpdate(rowNum: r, colNum: c, sqr: sqrArr[r, c]));
                }
            }
            return squares.ToArray();
        }

        /// <summary>Process a moveCmd from UI class and modify moveCmd with the results of the move. </summary>
        public void Move(ref UIMoveCommand moveCmd)
        {
            string moveCmdText = "";
            RowCol fmRC, toRC;
            ChessSquare fmSqr, toSqr;

            //check if the move is valid
            try
            {
                moveCmdText = moveCmd.RequestedMove.Substring(0, 4);
                string fmAddr = moveCmdText.Substring(0, 2);
                string toAddr = moveCmdText.Substring(2, 2);

                fmRC = getRowColFrmAddr(fmAddr);
                fmSqr = sqrArr[fmRC.R, fmRC.C];

                toRC = getRowColFrmAddr(toAddr);
                toSqr = sqrArr[toRC.R, toRC.C];
            }
            catch(Exception)
            {
                moveCmd.Result = EnumMoveResult.FailedBadAddress;
                return;
            }

            if(fmSqr == toSqr)
            {
                moveCmd.Result = EnumMoveResult.FailedBadAddress;
                return;
            }

            if(fmSqr.IsEmpty)
            {
                moveCmd.Result = EnumMoveResult.FailedSquareEmpty;
                return;
            }

            //OK, we got this far, the moves are sanitized 

            bool moveCapturesPiece = !toSqr.IsEmpty;

            moveCmd.UpdatedPieces =
                moveCapturesPiece ?
                    new[] { fmSqr.Piece, toSqr.Piece } :
                    new[] { fmSqr.Piece };

            //capture subr removes piece from the square, then call regular move subr
            if(moveCapturesPiece) { capturePiece(toSqr); }

            fmSqr.Piece.AddToHistory(moveCmdText);

            movePiece(fmSqr, toSqr);

            moveCmd.Result = 
                moveCapturesPiece ? 
                    EnumMoveResult.MoveAndCapture : 
                    EnumMoveResult.MoveToEmpty;

            moveCmd.UpdatedSquares = 
                new UISquareDisplay[] {
                    createSquareUpdate(fmRC.R, fmRC.C, fmSqr),
                    createSquareUpdate(toRC.R, toRC.C, toSqr)
            };
        }

        /// <summary> Initializes empty ChessSquare containers with an initial light or dark shading. </summary>
        private void createBoardSquares()
        {
            for(int r = 0; r < 8; r++)
            {
                for(int c = 0; c < 8; c++)
                {
                    sqrArr[r, c] = new ChessSquare(getColorAt(r, c));
                }
            }
        }

        /// <summary>Helper for createBoardSquares: return EnumColor of the board square shading based on the row and col index. </summary>
        private static EnumSquareColor getColorAt(int r, int c)
        {
            if((r + c) % 2 == 0)
            {
                return EnumSquareColor.Black;
            }
            else
            {
                return EnumSquareColor.White;
            }
        }

        /// <summary> Places chess pieces on an already-initialized board squares. </summary>
        private void placePieces()
        {
            //the arrangements of the white board pieces: t=<type>, c=<columns>, r=<row>
            var whtPosns = new[]{   
                new {t=EnumPieceType.Rook, c="ah", r=1},
                new {t=EnumPieceType.Knight, c="bg", r=1},
                new {t=EnumPieceType.Bishop, c="cf", r=1},
                new {t=EnumPieceType.Queen, c="d", r=1},
                new {t=EnumPieceType.King, c="e", r=1},
                new {t=EnumPieceType.Pawn, c="abcdefgh", r=2},
            };

            foreach(var e in whtPosns)
            {
                foreach(var col in e.c.AsEnumerable())
                {
                    int blkPieceRowNum = 9 - e.r;   //black pieces are symmetrical
                    createAndSetPiece(addType: e.t,
                                      addClr: EnumPieceColor.White,
                                      addCol: col,
                                      addRow: e.r);
                    createAndSetPiece(addType: e.t,
                                      addClr: EnumPieceColor.Black,
                                      addCol: col,
                                      addRow: blkPieceRowNum);
                }
            }
        }

        /// <summary>Creates a new piece and set it on a passed row and col.</summary>
        private void createAndSetPiece(EnumPieceType addType, EnumPieceColor addClr, char addCol, int addRow)
        {
            string boardAddr = string.Concat(addCol, addRow);
            ChessPiece newPiece = ChessPieceFactory.MakePiece(addClr, addType);
            setPiece(boardAddr, newPiece);
            Debug.WriteLine("Placed {0} on {1}", newPiece.ToString(), boardAddr);
        }

        /// <summary> Set a piece on the square address. </summary>
        private void setPiece(string sqrAddr, ChessPiece piece)
        {
            RowCol rc = getRowColFrmAddr(sqrAddr);
            sqrArr[rc.R, rc.C].Piece = piece;
        }

        private RowCol getRowColFrmAddr(string addr)
        {
            return new RowCol
            {
                R = int.Parse(addr.Substring(1)) - 1,
                C = (int)char.ToLower(addr.ElementAt(0)) - (int)'a'
            };
        }

        private UISquareDisplay createSquareUpdate(int rowNum, int colNum, ChessSquare sqr)
        {
            UISquareDisplay ds = new UISquareDisplay();
            ds.RowNum = rowNum;
            ds.ColumnNum = colNum;
            ds.ShadingColor = sqr.Color;
            ds.IsEmpty = sqr.IsEmpty;
            if(!ds.IsEmpty)
            {
                ds.PieceType = sqr.Piece.Type;
                ds.PieceColor = sqr.Piece.Color;
            }
            return ds;
        }

        private void movePiece(ChessSquare fmSqr, ChessSquare toSqr)
        {
            toSqr.Piece = fmSqr.Piece;
            fmSqr.Clear();
        }

        private void capturePiece(ChessSquare sqr)
        {
            sqr.Piece.IsCaptured = true;
            sqr.Clear();
        }
    }
}
