﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Chpt7Chess
{
    /// <summary>
    /// Proxy for comms between display and game model. Parses user commands, and initiates screen updates.
    /// </summary>
    class UI
    {
        /// <summary>Game Model.</summary>
        private ChessBoard chessBoard;

        /// <summary>Helper for displaying the board. </summary>
        private UIBoard boardDisplay;

        /// <summary>Helper for prompts and status messages. </summary>
        private UITextMsgs consUI;

        public UI(ChessBoard cb, UIBoard boardUI, UITextMsgs textUI)
        {
            this.chessBoard = cb;
            this.boardDisplay = boardUI;
            this.consUI = textUI;
        }

        public void StartMainInterfaceLoop()
        {
            fullBoardRedraw();
            bool askPrompt = true;

            while(askPrompt)
            {
                string moveCmdStr = consUI.UserInput(@"Enter a move or 'q' to exit: ");
                if(!isExitCommand(moveCmdStr))
                {
                    UIMoveCommand moveCmd = new UIMoveCommand { RequestedMove = moveCmdStr };
                    chessBoard.Move(ref moveCmd);
                    switch(moveCmd.Result)
                    {
                        case EnumMoveResult.MoveToEmpty:
                        case EnumMoveResult.MoveAndCapture:
                            consUI.ShowMsg(formatMoveResult(moveCmd.UpdatedPieces));
                            break;
                        case EnumMoveResult.FailedBadAddress:
                            consUI.ShowMsg("Error: Invalid move command. Enter a valid command like D2D4", true);
                            break;
                        case EnumMoveResult.FailedSquareEmpty:
                            consUI.ShowMsg("Error: You tried to move an empty square.", true);
                            break;
                    }
                    updateBoardSquares(moveCmd.UpdatedSquares);
                }
                else    //is an exit command
                {
                    consUI.ShowMsg("Press any key to exit..");
                    Console.ReadKey(true);
                    return;
                }
            }
        }

        private string formatMoveResult(ChessPiece[] pieces)
        {
            string moveStr = "";
            string captureStr = "";

            foreach(var piece in pieces)
            {
                if(piece.IsCaptured)
                {
                    captureStr = piece.ToString() + ".";
                }
                else
                {
                    moveStr =
                        piece.ToString() +
                        " moves from " +
                        piece.LastMove.AddrFrom +
                        " to " +
                        piece.LastMove.AddrTo + ". ";
                }
            }

            return moveStr + captureStr;
        }

        private bool isExitCommand(string s)
        {
            return s.ToLower().Equals("q");
        }

        /// <summary>Redraw the board from scratch. </summary>
        private void fullBoardRedraw()
        {
            boardDisplay.DrawBlankBoard();
            updateBoardSquares(chessBoard.GetFullBoardState());
        }

        /// <summary>Update board squares</summary>
        private void updateBoardSquares(UISquareDisplay[] updates)
        {
            foreach(var update in (updates ?? new UISquareDisplay[0]))
            {
                boardDisplay.updateSquare(
                    rowNum: update.RowNum,
                    colNum: update.ColumnNum,
                    squareIsShaded: update.ShadingColor == EnumSquareColor.Black,
                    squareIsEmpty: update.IsEmpty,
                    pcColor: update.PieceColor,
                    pcType: update.PieceType);
            }
        }




    }
}


