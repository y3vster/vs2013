﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Chpt7Chess
{
    /// <summary>
    /// Single instance of the chess piece created by the chesspiece factory
    /// </summary>
    [DebuggerDisplay("{Color} {Type}")]
    public class ChessPiece
    {
        public EnumPieceType Type { get; private set; }
        public EnumPieceColor Color { get; private set; }
        public bool IsCaptured { get; set; }

        /// <summary>Returns the last move</summary>
        public MoveEntry LastMove { get { return moveHistory.Peek() ?? new MoveEntry("Empty"); } }

        /// <summary>Entries in the piece move history. </summary>
        public class MoveEntry
        {
            string moveStr;
            public MoveEntry(string moveStr) { this.moveStr = moveStr.ToUpper(); }
            public override string ToString() { return moveStr; }
            public string AddrFrom { get { return moveStr.Substring(0, 2); } }
            public string AddrTo { get { return moveStr.Substring(2, 2); } }
        }

        private string cpName;      //full name of the piece
        private int cpWeight = 0;   //value in the game
        private Stack<MoveEntry> moveHistory = new Stack<MoveEntry>(new MoveEntry[] { null });

        /// <summary>Public Constructor.</summary>
        public ChessPiece(string cpName,
                          EnumPieceType cpType,
                          EnumPieceColor cpColor,
                          int cpWeight)
        {
            this.cpName = cpName;
            this.Type = cpType;
            this.Color = cpColor;
            this.cpWeight = cpWeight;
            this.IsCaptured = false;
        }

        /// <summary>Accepts a verified-valid move string and records it in the history. </summary>
        public void AddToHistory(string validMoveCommand)
        {
            moveHistory.Push(new MoveEntry(validMoveCommand));
        }

        public override string ToString()
        {
            if(IsCaptured)
            {
                return "Captured " + cpName;
            }
            else
            {
                return cpName;
            }
        }

    }
}