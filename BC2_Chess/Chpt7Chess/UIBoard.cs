﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chpt7Chess
{
    /// <summary>
    /// Responsible for displaying all of the board graphics.
    /// </summary>
    class UIBoard
    {
        const int SQUARE_W = 7; //box X chars
        const int SQUARE_H = 4; //box Y chars

        const int SPRITE_OFFSET_X = 2; //center the piece in the box
        const int SPRITE_OFFSET_Y = 0;

        const int BOARD_CORNER_X = 1;   //MUST BE > 1!
        const int BOARD_CORNER_Y = 1;   //cursor pos'n of the board top left corner

        const int PROMPT_START_LINE = (SQUARE_H + 1) * 8 + 5;   //where the board ends and interface begins   
        public int PromptStartLine { get { return PROMPT_START_LINE; } }

        /// <summary>lookup of a piece tuple like (EnumWhite, EnumKnight) to a list of char graphics</summary>
        private Dictionary<Tuple<EnumPieceColor, EnumPieceType>, List<string>> pieceIcons;

        /// <summary>Helper for printing to console and managing colors</summary>
        private UIConsole consoleInst;

        public UIBoard(UIConsole cons)
        {
            this.consoleInst = cons;
            generatePieceArt();
        }

        public void DrawBlankBoard()
        {
            consoleInst.SetColorMain();
            Console.Clear();
            printBorders();
            printLabels();
        }

        /// <summary>Display chess board borders. </summary>
        private void printBorders()
        {
            consoleInst.SetColorBorder();
            string[] lines = getBorders().Split('\n');
            for(int i = 0; i < lines.Length; i++)
            {
                consoleInst.outAt(BOARD_CORNER_Y + i, BOARD_CORNER_X, lines[i]);
            }
        }

        /// <summary>Helper to print board labels. </summary>
        private void printLabels()
        {
            consoleInst.SetColorBorder();

            int strtX = BOARD_CORNER_X - 1;
            int strtY = BOARD_CORNER_Y - 1;

            int stepY = SQUARE_H + 1;
            int stepX = SQUARE_W + 1;

            int endX = strtX + stepX * 8 + 2;
            int endY = strtY + stepY * 8 + 2;

            int curX = strtX + stepX / 2 + 1;
            int curY = strtY + stepY / 2 + 1;

            string colLabels = "ABCDEFGH";
            foreach(var lbl in colLabels.AsEnumerable())
            {
                consoleInst.outAt(strtY, curX, lbl);
                consoleInst.outAt(endY, curX, lbl);
                curX += stepX;
            }

            foreach(var lbl in Enumerable.Range(1, 8))
            {
                consoleInst.outAt(curY, strtX, lbl);
                consoleInst.outAt(curY, endX, lbl);
                curY += stepY;
            }
        }

        /// <summary>Draw a single board square</summary>
        public void updateSquare(int rowNum,
                                  int colNum,
                                  bool squareIsShaded,
                                  bool squareIsEmpty,
                                  EnumPieceColor pcColor,
                                  EnumPieceType pcType)
        {
            int t, l;       //cursor positions
            int tBottom;    //cursor square bottom

            rowColToCursorXY(rowNum, colNum, out t, out l);
            tBottom = t + SQUARE_H;

            consoleInst.SetColorSquare(squareIsShaded);

            string blankLine = new string(' ', SQUARE_W);
            if(!squareIsEmpty)
            {
                StringBuilder sb = new StringBuilder();
                foreach(string line in pieceIcons[Tuple.Create(pcColor, pcType)])
                {
                    sb.Clear();
                    sb.Append(blankLine);
                    sb.Remove(SPRITE_OFFSET_X, line.Length);
                    sb.Insert(SPRITE_OFFSET_X, line);
                    consoleInst.outAt(t, l, sb);
                    t++;
                }
                string leftPad = new string(' ', SPRITE_OFFSET_X);

            }
            //remaining blank lines w/o piece sprite
            while((tBottom - t) > 0)
            {
                consoleInst.outAt(t, l, blankLine);
                t++;
            }
        }

        /// <summary>Helper for processSquareUpdate(..)</summary>
        private void rowColToCursorXY(int row, int col, out int t, out int l)
        {
            t = row * SQUARE_H + row + BOARD_CORNER_Y + 1;
            l = col * SQUARE_W + col + BOARD_CORNER_X + 1;
        }

        /// <summary>Returns a string of borders </summary>
        private string getBorders()
        {
            //box drawing chars
            //      0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	A 	B 	C 	D 	E 	F
            //B 				│ 	┤ 	╡ 	╢ 	╖ 	╕ 	╣ 	║ 	╗ 	╝ 	╜ 	╛ 	┐
            //C 	└ 	┴ 	┬ 	├ 	─ 	┼ 	╞ 	╟ 	╚ 	╔ 	╩ 	╦ 	╠ 	═ 	╬ 	╧
            //D 	╨ 	╤ 	╥ 	╙ 	╘ 	╒ 	╓ 	╫ 	╪ 	┘ 	┌ 					

            StringBuilder top = new StringBuilder("╔"); //mutable strings
            StringBuilder vert = new StringBuilder("║");
            StringBuilder mid = new StringBuilder("╟");
            StringBuilder bot = new StringBuilder("╚");

            for(int i = 0; i < 8; i++)
            {
                top.Append('═', SQUARE_W);
                top.Append('╤');
                vert.Append(' ', SQUARE_W);
                vert.Append('│');
                mid.Append('─', SQUARE_W);
                mid.Append('┼');
                bot.Append('═', SQUARE_W);
                bot.Append('╧');
            }

            top.Remove(top.Length - 1, 1);
            vert.Remove(vert.Length - 1, 1);
            mid.Remove(mid.Length - 1, 1);
            bot.Remove(bot.Length - 1, 1);

            top.AppendLine("╗");
            vert.AppendLine("║");
            mid.AppendLine("╢");
            bot.AppendLine("╝");

            string vertBars = String.Concat(Enumerable.Repeat(vert, SQUARE_H));

            for(int i = 0; i < 7; i++)
            {
                top.Append(vertBars);
                top.Append(mid);
            }
            top.Append(vertBars);
            top.Append(bot);

            return top.ToString();
        }

        /// <summary>Populates the pieceIcons dictionary</summary>
        private void generatePieceArt()
        {
            const string ASCII_ART = @"
 _ ¦ _ ¦ _  ¦ _  ¦ o ¦ o ¦uuu¦uuu¦www¦www¦_+_¦_+_ 
( )¦(#)¦( '\¦(#'\¦(/)¦(#)¦| |¦|#|¦\ /¦\#/¦\ /¦\#/
/ \¦/#\¦| \ ¦|#\ ¦/ \¦/#\¦/ \¦/#\¦( )¦(#)¦( )¦(#)
***¦***¦*** ¦*** ¦***¦***¦***¦***¦/ \¦/#\¦/ \¦/#\";

            var iconNames = new Dictionary<int, string>(){
                {0, "White Pawn"},
                {1, "Black Pawn"},
                {2, "White Knight"},
                {3, "Black Knight"},
                {4, "White Bishop"},
                {5, "Black Bishop"},
                {6, "White Rook"},
                {7, "Black Rook"},
                {8, "White Queen"},
                {9, "Black Queen"},
                {10, "White King"},
                {11, "Black King"}
            };

            //allows look up of enum values by a string
            var getEnumClrFmStr = new Dictionary<string, EnumPieceColor>(){
                {"White", EnumPieceColor.White},
                {"Black", EnumPieceColor.Black}
            };
            var getEnumTypeFmStr = Enumerable.Zip(Enum.GetNames(typeof(EnumPieceType)),
                                 (EnumPieceType[])Enum.GetValues(typeof(EnumPieceType)),
                                                  (str, val) => new { str, val })
                                             .ToDictionary(rec => rec.str,
                                                           rec => rec.val);

            //creates a tuple of enums from a string like "White Pawn"
            Func<string, Tuple<EnumPieceColor, EnumPieceType>> tupleFmStr = (str) =>
            {
                string[] entries = str.Split();
                return Tuple.Create(getEnumClrFmStr[entries[0]],
                                    getEnumTypeFmStr[entries[1]]);
            };

            //convert entries to {0, Tuple<EnumPieceColor, EnumPieceType>}, {..
            var iconTuples = iconNames.ToDictionary(pair => pair.Key,
                                                    pair => tupleFmStr(pair.Value));

            //each piece icon can be looked up by a tuple like (EnumWhite, EnumPawn)
            pieceIcons = new Dictionary<Tuple<EnumPieceColor, EnumPieceType>, List<string>>();

            string[] lines = ASCII_ART.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            //build a list of strings for each tuple entry
            foreach(var line in lines)
            {
                var lineSegments = line.Split('¦').Select((txt, index) => new { index, txt });
                foreach(var subSegment in lineSegments)
                {
                    List<string> iconSegments;  //list of segments associated with each key
                    var key = iconTuples[subSegment.index];

                    if(pieceIcons.TryGetValue(key, out iconSegments))
                    {
                        iconSegments.Add(subSegment.txt);
                    }
                    else
                    {
                        iconSegments = new List<string>();
                        iconSegments.Add(subSegment.txt);
                        pieceIcons.Add(key, iconSegments);
                    }
                }//foreach segments
            }//foreach lines
        }
    }
}
