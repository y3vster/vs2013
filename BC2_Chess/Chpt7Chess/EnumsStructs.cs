﻿using System.Diagnostics;

namespace Chpt7Chess {
    public enum EnumSquareColor { White, Black};
    public enum EnumPieceType { Pawn, Knight, Bishop, Rook, Queen, King};
    public enum EnumPieceColor { White, Black};

    /// <summary>Callback from the model: redraw these squares.</summary>
    [DebuggerDisplay("{debuggerDisplay,nq}")]
    public struct UISquareDisplay
    {
        public int RowNum;
        public int ColumnNum;
        public EnumSquareColor ShadingColor;
        public bool IsEmpty;
        public EnumPieceType PieceType;
        public EnumPieceColor PieceColor;
        private string debuggerDisplay { get { return string.Format("Update: ({0}, {1}): {2}", RowNum, ColumnNum, IsEmpty ? "Empty" : (PieceColor + " " + PieceType)); } }
    }

    /// <summary>Passed to the model: requested move from the player. </summary>
    public struct UIMoveCommand
    {
        public string RequestedMove;
        public EnumMoveResult Result;
        public UISquareDisplay[] UpdatedSquares;
        public ChessPiece[] UpdatedPieces;
    }

    /// <summary>Result of the move command</summary>
    public enum EnumMoveResult
    {
        MoveToEmpty,            //Piece successfully moved
        MoveAndCapture,           //Piece moved and captured another piece 
        FailedBadAddress,   //Move's address is invalid
        FailedSquareEmpty   //Move's start square is empty - nothing to move
    }

}