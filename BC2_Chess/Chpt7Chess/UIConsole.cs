﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Chpt7Chess
{
    /// <summary>
    /// Helper for managing colors and output to console.
    /// </summary>
    class UIConsole
    {
        const ConsoleColor CONS_MAIN_BACK = ConsoleColor.Black;
        const ConsoleColor CONS_MAIN_FORE = ConsoleColor.White;
        const ConsoleColor CONS_MAIN_ERROR = ConsoleColor.Red;
        const ConsoleColor CONS_BORDER_BACK = ConsoleColor.Black;
        const ConsoleColor CONS_BORDER_FORE = ConsoleColor.Yellow;
        const ConsoleColor CONS_SQUARE_SHADE_WHT = ConsoleColor.White;
        const ConsoleColor CONS_SQUARE_SHADE_BLK = ConsoleColor.Gray;
        const ConsoleColor CONS_PIECE_FORE = ConsoleColor.Black;
        
        const int BUF_W = 68;  //console window buffer size
        const int BUF_H = 50;

        const int WIND_W = 68; //user's window size in chars
        const int WIND_H = 50; //will be clipped to the max allowed

        private static UIConsole consInst;

        private UIConsole()
        {
            initConsoleWindow(); 
        }

        /// <summary>Console.Write(s) at a location (t, l)</summary>
        public void outAt<T>(int t, int l, T s)
        {
            Console.SetCursorPosition(l, t);
            Console.Write(s);
        }
        
        public void SetColorMain(){
            setClr(CONS_MAIN_BACK, CONS_MAIN_FORE);
        }

        public void SetColorBorder()
        {
            setClr(CONS_BORDER_BACK, CONS_BORDER_FORE);
        }

        public void SetColorSquare(bool isShaded)
        {
            setClr(isShaded ? CONS_SQUARE_SHADE_BLK : CONS_SQUARE_SHADE_WHT, CONS_PIECE_FORE);
        }

        public void SetColorError()
        {
            setClr(CONS_MAIN_BACK, CONS_MAIN_ERROR);
        }

        private void setClr(ConsoleColor back, ConsoleColor fore){
            Console.BackgroundColor = back;
            Console.ForegroundColor = fore;
        }

        private void initConsoleWindow()
        {
            SetColorMain();
            try
            {
                Console.SetWindowSize(1, 1);
                Console.SetBufferSize(BUF_W, BUF_H);
                int w = (Console.LargestWindowWidth < WIND_W) ? Console.LargestWindowWidth : WIND_W;
                int h = (Console.LargestWindowHeight < WIND_H) ? Console.LargestWindowHeight : WIND_H;
                Console.SetWindowSize(w, h);
            }
            catch(Exception e)
            {
                Debug.WriteLine("Error initializing console window size: " + e.ToString());
            }
        }

        /// <summary>Singleton</summary>
        public static UIConsole getConsInstance(){ 
            if(consInst == null)
            {
                consInst = new UIConsole();
            }
            return consInst;
        }
    }
}
