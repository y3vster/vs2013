﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chpt7Chess
{
    /// <summary>
    /// Helper for reading commands and printing messages to user.
    /// </summary>
    class UITextMsgs
    {
        UIConsole consInst; //helper for output and colors
        int startLine;      //where the prompt line is
        
        public UITextMsgs(UIConsole cs, int startLine)
        {
            this.consInst = cs;
            this.startLine = startLine;
        }
        
        public string UserInput(string promptStr)
        {
            clearLine(startLine);
            consInst.outAt(startLine, 0, promptStr);
            return Console.ReadLine();
        }

        public void ShowMsg(string msg, bool isImportant = false)
        {
            clearLine(startLine + 1);
            if(isImportant)
            {
                consInst.SetColorError();
            }
            else
            {
                consInst.SetColorMain();
            }
            consInst.outAt(startLine + 1, 0, msg);
        }

        private void clearLine(int lineNum)
        {
            consInst.SetColorMain();
            consInst.outAt(lineNum, 0, new string(' ', Console.BufferWidth));
        }
    }
}
