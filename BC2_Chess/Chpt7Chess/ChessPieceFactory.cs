﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Piece 	pawn 	knight 	bishop 	rook 	queen
//Value 	1 	    3 	    3 	    5 	    9

namespace Chpt7Chess {

    /// <summary>
    /// Creates and returns a new instance of the piece.
    /// </summary>
    class ChessPieceFactory {

        public static ChessPiece MakePiece(EnumPieceColor pieceColor,
                                           EnumPieceType pieceType,
                                           ChessSquare pieceOnSqr = null) {
            string colorStr = null;
            switch (pieceColor) {
                case EnumPieceColor.White:
                    colorStr = "White ";
                    break;
                case EnumPieceColor.Black:
                    colorStr = "Black ";
                    break;
            }

            string pieceName = null;
            int pieceWeight = 0;
            switch (pieceType) {
                case EnumPieceType.Pawn:
                    pieceName = "Pawn";
                    pieceWeight = 1;
                    break;
                case EnumPieceType.Knight:
                    pieceName = "Knight";
                    pieceWeight = 3;
                    break;
                case EnumPieceType.Bishop:
                    pieceName = "Bishop";
                    pieceWeight = 3;
                    break;
                case EnumPieceType.Rook:
                    pieceName = "Rook";
                    pieceWeight = 5;
                    break;
                case EnumPieceType.Queen:
                    pieceName = "Queen";
                    pieceWeight = 9;
                    break;
            }

            return new ChessPiece(cpName: colorStr + pieceName,
                                  cpColor: pieceColor,
                                  cpWeight: pieceWeight,
                                  cpType: pieceType);

        }


    }


}
