﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Chpt7Chess {

    /// <summary>
    /// single board square instance.
    /// </summary>
    class ChessSquare {
        public EnumSquareColor Color {get; private set;}
        public ChessPiece Piece { get; set; }
        public bool IsEmpty {get {return Piece == null;}}

        /// <summary>Constructor.</summary>
        public ChessSquare(EnumSquareColor myColor, ChessPiece myPiece = null) {
            this.Color = myColor;
            this.Piece = myPiece;
        }

        public void Clear() {
            Piece = null;
        }

    }
}
