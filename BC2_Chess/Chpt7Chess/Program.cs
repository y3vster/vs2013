﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chpt7Chess
{
    class Program
    {
        static void Main(string[] args)
        {
            //Game model class.
            ChessBoard boardInst = new ChessBoard();
            
            //Singleton of the console helper
            UIConsole consoleInst = UIConsole.getConsInstance();
            
            //Responsible for displaying board graphics
            UIBoard uiBoard = new UIBoard(consoleInst);
            
            //Responsible for dispaying messages and parsing user commands
            UITextMsgs uiMsgs = new UITextMsgs(consoleInst, uiBoard.PromptStartLine);

            //Game controller
            UI ui = new UI(boardInst,
                           uiBoard,
                           uiMsgs);

            ui.StartMainInterfaceLoop();
        }
    }
}
