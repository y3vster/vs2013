﻿//Yevgeni Kamenski
//HashTable.cs
//9-2-2015

using System;
using System.Diagnostics;
using System.Linq;

namespace BC3_Lab11
{

    internal class Hashtable<TKey, TValue>
    {

        //when to grow and shrink
        private const double LOADF_MIN = 0.125;
        private const double LOADF_MAX = 0.5;

        //prime for secondary hash
        private const int PRIME_H2 = 17;

        //found on: http://planetmath.org/goodhashtableprimes
        private static readonly int[] PRIMES =
        {
            23, 53, 97, 193, 389, 769, 1543, 3079, 6151, 12289, 24593, 49157,
            98317, 196613, 393241, 786433, 1572869, 3145739, 6291469, 12582917,
            25165843, 50331653, 100663319, 201326611, 402653189, 805306457,
            1610612741
        };

        //hash table record representation
        //sign bit of the Hash value is used to flag collisions
        private class HashEntry
        {
            public TKey Key;
            public TValue Value;
            public int HashDeleted; //store hash code + deleted flag
        }

        //hash entry table
        private HashEntry[] _table;
        //current size of the table

        private int _tableSize;
        //index of the current prime array size 
        private int _currentPrimeArrayIndex = -1;
        //initial capacity
        private int _tableSizeInit;

        //disable resizing flag
        private bool _resizingDisabled = false;

        public int LastCollisions { get; private set; }

        /// <summary> number of elements in the hash table</summary>
        public int Count { get; private set; }

        public int TableSize
        {
            get { return _tableSize; }
        }

        public double LoadFactor { get { return (double) Count/(double) _tableSize; } }

    /// <summary>Default ctor.</summary>
        public Hashtable() : this(10) { }

        public Hashtable(int overrideCapacity, bool disableResizing)
        {
            _resizingDisabled = disableResizing;
            _tableSize = overrideCapacity;
            _table = new HashEntry[_tableSize];
        }

        /// <summary>Ctor with user-supplied initial capacity</summary>
        public Hashtable(int initialCapacity)
        {
            _tableSizeInit = PrimeCeiling(initialCapacity * 2);
            Count = 0;
            _tableSize = _tableSizeInit;
            _table = new HashEntry[_tableSize];

        }


        //public methods
        //==========================================================
        public void Add(TKey key, TValue value)
        {
            HashEntry insertEntry = new HashEntry() {
                HashDeleted = key.GetHashCode(),
                Key = key,
                Value = value
            };
            GrowTableIfNeeded();
            if (InsertEntry(insertEntry)) Count++;
        }

        public void Remove(TKey key)
        {
            Delete(key);
        }

        public bool Contains(TKey key)
        {
            return GetEntry(key) != null;
        }

        public TValue this[TKey key]
        {
            get
            {
                HashEntry entry = GetEntry(key);
                return entry != null ? entry.Value : default(TValue);
            }
            set
            {
                Add(key, value);
            }
        }

        //Helper functions
        //==========================================================

        //return a bool indicating if there was a collision when adding
        private static bool IsDeleted(HashEntry entry)
        {
            return (entry.HashDeleted & 0x80000000) == 0x80000000;
        }

        //set the entry's deleted flag 
        private static void MarkDeleted(HashEntry entry)
        {
            entry.HashDeleted |= unchecked((int)0x80000000);
        }

        ////set the entry's collision flag to false
        private static void ClearDeletedFlag(HashEntry entry)
        {
            entry.HashDeleted &= 0x7FFFFFFF;
        }

        //compare hashes ignoring the collision flag
        private static bool HashesMatch(HashEntry entry, int hash)
        {
            return (entry.HashDeleted & 0x7FFFFFFF) == hash;
        }

        //returns true if keys match. keys are not compared if the hashes don't match
        private static bool KeysMatch(HashEntry entry, int hash, TKey key)
        {
            //avoid key comparison if possible by first checking hashes
            return HashesMatch(entry, hash) && entry.Key.Equals(key);
        }

        //lazy deletes the key
        private void Delete(TKey key)
        {
            int deleteHash = key.GetHashCode();
            int posn;
            int incr;
            CalculatePosn(deleteHash, _tableSize, out posn, out incr);

            //this is clearer than having 2 while predicates & null checks after
            int collisionCount = 0;
            do
            {
                HashEntry entry = _table[posn];

                //case: nothing to delete
                if (entry == null) return; //nothing to delete

                //case: found something to delete
                if (KeysMatch(entry, deleteHash, key))
                {
                    MarkDeleted(entry);
                    Count--;
                    break;
                }

                //case: not found, but something was there in the past
                if (!IsDeleted(entry))
                    return;
                posn = (posn + incr) % _tableSize;

            } while (++collisionCount < _tableSize);

            ShrinkTableIfNeeded();
        }

        /// <summary>Also removes any deleted flags</summary>
        private static void CalculatePosn(int preHash, int size, out int init, out int incr)
        {
            //initial search position, with the sign bit removed
            init = (preHash & 0x7FFFFFFF) % size;

            //secondary hash for the incremental search
            //incr = PRIME_H2 - preHash % PRIME_H2;
            incr = 1;
        }

        //returns an instance of an entry class if present. return null if not found
        private HashEntry GetEntry(TKey key)
        {
            int preHash = key.GetHashCode();
            int posn;
            int incr;
            CalculatePosn(preHash, _tableSize, out posn, out incr);

            HashEntry current;

            int numCollisions = 0;
            int firstAvailablePosn = -1;    //position to move to if previously deleted
            do
            {
                current = _table[posn];

                //nothing to look at
                if (current == null) return null;

                //found it, and it's not deleted 
                if (KeysMatch(current, preHash, key) && !IsDeleted(current))
                    if (IsDeleted(current))
                        return null;
                    else
                    {
                        if (firstAvailablePosn != -1)
                        {
                            //reinsert;
                            InsertEntry(_table[posn]);
                            _table[posn] = null;
                        }
                        return current;
                    }

                if (IsDeleted(current) && firstAvailablePosn == -1)
                {
                    firstAvailablePosn = posn;
                }

                //advance
                posn = (posn + 1) % _tableSize;

            } while (++numCollisions < _tableSize);

            return null;    //the list is full? that's not good..
        }

        //  lazy deletion refers to a method of deleting elements from a hash table that uses open addressing.
        //In this method, deletions are done by marking an element as deleted, rather than erasing it entirely. 
        //Deleted locations are treated as empty when inserting and as occupied during a search.

        //The problem with this scheme is that as the number of delete/insert operations increases, the cost of a successful search increases. To improve this, when an element is searched and found in the table, the element is relocated to the first location marked for deletion that was probed during the search. Instead of finding an element to relocate when the deletion occurs, the relocation occurs lazily during the next search.[1][2]


        //insert an instance of the entry into the table, updates value field if a duplicate is found
        private bool InsertEntry(HashEntry insertEntry)
        {
            //start out clean and set it if collide while inserting
            //ClearCollisionFlag(insertEntry);

            int posn;
            int incr;
            CalculatePosn(insertEntry.HashDeleted, _tableSize, out posn, out incr);

            int numCollisions = 0;
            do
            {
                HashEntry currentEntry = _table[posn];

                //found an open slot, insert & we're done
                //if (currentEntry == null)
                if (currentEntry == null || IsDeleted(currentEntry))
                {
                    _table[posn] = insertEntry;
                    break;
                }

                //do we have the same keys? 
                //if so, it's a duplicate, so update value & we're done
                if (KeysMatch(currentEntry, insertEntry.HashDeleted, insertEntry.Key))
                {
                    currentEntry.Value = insertEntry.Value;
                    break;
                }

                //we have a collision
                numCollisions++;

                //advance
                posn = (posn + incr) % _tableSize;

            } while (numCollisions < _tableSize);
            LastCollisions = numCollisions;
            return true;
            throw new OverflowException();
        }

        //grow the table if loading is exceeded
        private void GrowTableIfNeeded()
        {
            if (_resizingDisabled) return;

            //double table size if 50% full..
            if ((double)Count / (double)_tableSize >= LOADF_MAX)
                Resize(PrimeNext());
        }

        //shrink the table, but don't go below user's minimum
        private void ShrinkTableIfNeeded()
        {
            
            if (_resizingDisabled) return;

            //shrink if 12.5% full or less and greater than min size
            if (Count > 0 && (double)Count / (double)_tableSize <= LOADF_MIN && _tableSize / 2 > _tableSizeInit)
            {
                Resize(PrimePrevious());
            }
        }

        //resize the table
        private void Resize(int size)
        {
            Hashtable<TKey, TValue> tempTable = new Hashtable<TKey, TValue>(size);

            //re-insert entries from the current into the larger temp
            foreach (HashEntry entry in _table.Where(entry => entry != null && !IsDeleted(entry)))
            {
                ClearDeletedFlag(entry);
                tempTable.InsertEntry(entry);
            }

            //update myself with the new table of entries from the temp
            _table = tempTable._table;
            _tableSize = size;
        }

        ////get a hash code w/o the sign bit (collision flag)
        //private int HashCode(TKey key)
        //{
        //    return key.GetHashCode() & 0x7FFFFFFF;
        //}

        private int PrimeCeiling(int num)
        {
            for (int i = 0; i < PRIMES.Length; i++)
            {
                if (num < PRIMES[i])
                {
                    _currentPrimeArrayIndex = i;
                    return PRIMES[i];
                }
            }
            throw new OverflowException("Reached maximum hash table size");
        }

        private int PrimeNext()
        {
            if (++_currentPrimeArrayIndex == PRIMES.Length)
                throw new OverflowException("Reached maximum hash table size");
            return PRIMES[_currentPrimeArrayIndex];
        }

        private int PrimePrevious()
        {
            return PRIMES[--_currentPrimeArrayIndex];
        }

        //get next prime after size
        private int GetCeilingPrime(int size)
        {
            foreach (int p in PRIMES)
                if (p > size) return p;
            return PRIMES.Last();
        }

    }
}
