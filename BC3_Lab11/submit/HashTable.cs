﻿//Yevgeni Kamenski
//HashTable.cs
//9-2-2015

using System;
using System.Diagnostics;
using System.Linq;

namespace BC3_Lab11 {

    class Hashtable<TKey, TValue> {

        //when to grow and shrink
        private const double FL_MIN = 0.125;
        private const double FL_MAX = 0.5;

        //found on: http://planetmath.org/goodhashtableprimes
        private static readonly int[] PRIMES =
        {
            53, 97, 193, 389, 769, 1543, 3079, 6151, 12289, 24593, 49157,
            98317, 196613, 393241, 786433, 1572869, 3145739, 6291469, 12582917,
            25165843, 50331653, 100663319, 201326611, 402653189, 805306457,
            1610612741
        };

        //hash table record representation
        //sign bit of the Hash value is used to flag collisions
        private class HashEntry {
            public TKey Key;
            public TValue Value;
            public int Hash;    //avoid unnecessary value comparisons by storing pre hashes
        }

        private HashEntry[] _table; //hash entry table
        private int _capacity;      //current size of the table
        private int _minCapacity;   //remember initial user size and don't go below

        /// <summary> number of elements in the hash table</summary>
        public int Count { get; private set; }

        /// <summary>Default ctor.</summary>
        public Hashtable() : this(10) { }

        /// <summary>Ctor with user-supplied initial capacity</summary>
        public Hashtable(int initialCapacity)
        {
            _minCapacity = initialCapacity;
            Count = 0;
            _capacity = GetFloorPrime(_minCapacity * 2);
            _table = new HashEntry[_capacity];
        }


        //public methods
        //==========================================================
        public void Add(TKey key, TValue value)
        {
            HashEntry insertEntry = new HashEntry() {
                Hash = HashCode(key),
                Key = key,
                Value = value
            };
            GrowTableIfNeeded();
            PutEntry(insertEntry);
            Count++;
        }

        public void Remove(TKey key)
        {
            Delete(key);
        }

        public bool Contains(TKey key)
        {
            return GetEntry(key) != null;
        }

        public TValue this[TKey key]
        {
            get
            {
                HashEntry entry = GetEntry(key);
                return entry != null ? entry.Value : default(TValue);
            }
            set
            {
                Add(key, value);
            }
        }

        //Helper functions
        //==========================================================

        //return a bool indicating if there was a collision when adding
        private static bool CollisionFlag(HashEntry entry)
        {
            return (entry.Hash & 0x80000000) == 0x80000000;
        }

        //set the entry's collision flag to true
        private static void SetCollisionFlag(HashEntry entry)
        {
            entry.Hash |= unchecked((int)0x80000000);
        }

        //set the entry's collision flag to false
        private static void ClearCollisionFlag(HashEntry entry)
        {
            entry.Hash &= 0x7FFFFFFF;
        }

        //compare hashes ignoring the collision flag
        private static bool HashesMatch(HashEntry entry, int hash)
        {
            return (entry.Hash & 0x7FFFFFFF) == hash;
        }

        //returns true if keys match. keys are not compared if the hashes don't match
        private static bool KeysMatch(HashEntry entry, int hash, TKey key)
        {
            //avoid key comparison if possible by first checking hashes
            return HashesMatch(entry, hash) && entry.Key.Equals(key);
        }

        //deletes the key
        private void Delete(TKey key)
        {
            int deleteHash = HashCode(key);

            //starting position
            int posn = deleteHash % _capacity;

            //did current position have a collision in the past?
            bool collisionFlagAtPosn = false;

            //this is clearer than having 2 while predicates & null checks after
            while(true) {
                HashEntry entry = _table[posn];

                //case: nothing to delete
                if(entry == null) return; //nothing to delete

                //case: found something to delete
                if(KeysMatch(entry, deleteHash, key)) break;

                //case: not found, but there was a collision in the past
                //      so keep looking
                collisionFlagAtPosn = CollisionFlag(entry);
                if(collisionFlagAtPosn)
                    posn = (posn + 1) % _capacity;
            }

            //delete the current value
            _table[posn] = null;
            Count--;

            //is there a history of collision at current posn?
            //if so, rehash the current cluster
            while(collisionFlagAtPosn) {
                posn = (posn + 1) % _capacity;
                HashEntry entry = _table[posn];
                if(entry == null) break;   //end of cluster
                collisionFlagAtPosn = CollisionFlag(entry);
                _table[posn] = null;

                PutEntry(entry);

                Debug.Assert(_table[posn] != entry, "Should not be re-inserting at the same position");
            }

            ShrinkTableIfNeeded();
        }

        //returns an instance of an entry class if present. return null if not found
        private HashEntry GetEntry(TKey key)
        {
            int keyHash = HashCode(key);
            int posn = keyHash % _capacity;
            
            HashEntry current;

            do {
                current = _table[posn];

                //nothing to look at
                if(current == null) return null;

                //found it, stop looking and return
                if(KeysMatch(current, keyHash, key)) {
                    return current;
                }

                //advance
                posn = (posn + 1) % _capacity;
            
            //but only if there was a history of collisions
            } while(CollisionFlag(current));

            return null;    //not found
        }

        //insert an instance of the entry into the table, updates value field if a duplicate is found
        private void PutEntry(HashEntry insertEntry)
        {
            //start out clean and set it if collide while inserting
            ClearCollisionFlag(insertEntry);

            //initial search position
            int posn = insertEntry.Hash % _capacity;

            //I find this clearer than the alternative
            while(true) {

                HashEntry currentEntry = _table[posn];

                //found an open slot, insert & we're done
                if(currentEntry == null) {
                    _table[posn] = insertEntry;
                    return;
                }

                //do we have the same keys? 
                //if so, it's a duplicate, so update value & we're done
                if(KeysMatch(currentEntry, insertEntry.Hash, insertEntry.Key)) {
                    currentEntry.Value = insertEntry.Value;
                    return;
                }

                //occupied slot and it's not a duplicate, mark the offender
                SetCollisionFlag(currentEntry);
                SetCollisionFlag(insertEntry);

                //advance
                posn = (posn + 1) % _capacity;
            }
        }

        //grow the table if loading is exceeded
        private void GrowTableIfNeeded()
        {
            //double table size if 50% full..
            if((double)Count / (double)_capacity >= FL_MAX)
                Resize(GetCeilingPrime(_capacity * 2));
        }

        //shrink the table, but don't go below user's minimum
        private void ShrinkTableIfNeeded()
        {
            //shrink if 12.5% full or less and greater than min size
            if(Count > 0 && (double)Count / (double)_capacity <= FL_MIN && _capacity / 2 > _minCapacity) {
                Resize(GetCeilingPrime(_capacity / 2));
            }
        }

        //resize the table
        private void Resize(int size)
        {
            Hashtable<TKey, TValue> tempTable = new Hashtable<TKey, TValue>(size);

            //re-insert entries from the current into the larger temp
            foreach(HashEntry entry in _table.Where(entry => entry != null))
                tempTable.PutEntry(entry);

            //update myself with the new table of entries from the temp
            _table = tempTable._table;
            _capacity = size;
        }

        //get a hash code w/o the sign bit (collision flag)
        private int HashCode(TKey key)
        {
            return key.GetHashCode() & 0x7FFFFFFF;
        }

        //get prime less than size
        private int GetFloorPrime(int size)
        {
            for(int i = 1; i < PRIMES.Length; i++) {
                if(PRIMES[i - 1] <= size && PRIMES[i] > size)
                    return PRIMES[i - 1];
            }
            throw new OverflowException("Hash table exceeded " + PRIMES.Last());
        }

        //get next prime after size
        private int GetCeilingPrime(int size)
        {
            foreach(int p in PRIMES)
                if(p > size) return p;
            return PRIMES.Last();
        }

    }
}
