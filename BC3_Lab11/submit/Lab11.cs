﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace BC3_Lab11 {
    class Lab11 {
        private static Stopwatch _stopwatch;    //used for timing

        /// <summary>Initializes the timer </summary>
        private static void InitStopwatch()
        {
            _stopwatch = new Stopwatch();

            //Frequency attribute is in ticks/sec
            double muSecPerTick = 1000000.0 / Stopwatch.Frequency;

            Console.WriteLine("Stopwatch is high resolution: " + Stopwatch.IsHighResolution);
            Console.WriteLine("Stopwatch resolution is ~ {0:F2} μs", muSecPerTick);

            //use second processor core for the test
            Process.GetCurrentProcess().ProcessorAffinity = new IntPtr(2);

            //prevent "Normal" processes and threads from forcing me into a context switch mid-test
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
        }

        private static void Main(string[] args)
        {
            InitStopwatch();

            //create a hashtable of size 4096
            Hashtable<int, int> ht = new Hashtable<int, int>(4096);

            Console.WriteLine("How many random nums would you like to add? ");
            int numToAdd = int.Parse(Console.ReadLine());

            //stream of random numbers:
            Random r = new Random();
            Func<int, int, List<int>> getRandoms =
                (nums, ubnd) =>
                    Enumerable.Range(1, nums).Select(i => r.Next(1, ubnd)).ToList();

            List<int> randomKeys = getRandoms(numToAdd, 100000);
            List<int> seqValues = Enumerable.Range(1, numToAdd).ToList();

            IEnumerable<Tuple<int, int>> keyValPairs = Enumerable.Zip(randomKeys, seqValues, Tuple.Create);

            Console.WriteLine("Adding random key value pairs..");
            foreach(Tuple<int, int> keyVal in keyValPairs) {
                ht.Add(keyVal.Item1, keyVal.Item2);
            }

            int[][] customAdd = {
                new[] {1179, 120},
                new[] {9702, 121},
                new[] {7183, 122},
                new[] {50184, 123},
                new[] {4235, 124},
                new[] {644, 125},
                new[] {77, 126},
                new[] {3077, 127},
                new[] {23477, 128},
                new[] {90777, 129}
            };

            Console.WriteLine("Adding and custom 10 additions:");

            foreach(int[] ints in customAdd) {
                int key = ints[0];
                int val = ints[1];

                //Perform garbage collection and wait until it finishes
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //time
                _stopwatch.Reset();
                _stopwatch.Start();
                ht.Add(key,val);
                _stopwatch.Stop();

                //results
                double microSecs = _stopwatch.Elapsed.TotalMilliseconds * 1000.0;
                Console.WriteLine("Add ({0}, {1}): = {2:F1} μs", key, val, microSecs);
            }

            Console.WriteLine("Getting values from keys: ");
            Console.WriteLine("HashTable[{0}] = {1}", 50184, ht[50184]);
            Console.WriteLine("HashTable[{0}] = {1}", 77, ht[77]);

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);
        }



    }
}
