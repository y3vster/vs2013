﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace BC3_Lab11
{
    class Lab11
    {
        private static Stopwatch _stopwatch;    //used for timing

        /// <summary>Initializes the timer </summary>
        private static void InitStopwatch()
        {
            _stopwatch = new Stopwatch();

            //Frequency attribute is in ticks/sec
            double muSecPerTick = 1000000.0 / Stopwatch.Frequency;

            Console.WriteLine("Stopwatch is high resolution: " + Stopwatch.IsHighResolution);
            Console.WriteLine("Stopwatch resolution is ~ {0:F2} μs", muSecPerTick);

            //use second processor core for the test
            Process.GetCurrentProcess().ProcessorAffinity = new IntPtr(2);

            //prevent "Normal" processes and threads from forcing me into a context switch mid-test
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
        }

        private static void Main(string[] args)
        {
            InitStopwatch();

            //int numToTest = 3079;
            //int numToTest = 4085;
            int numToTest = 12289;
            int randBound = numToTest * 2;

            Hashtable<int, int> ht = new Hashtable<int, int>(numToTest, true);

            Console.WriteLine("Starting the test of additions to " + numToTest);

            //results are in the tuple of time / count / table size / load factor

            //stream of random numbers:
            Random r = new Random();
            Func<int, int, List<int>> getRandoms =
                (nums, ubnd) =>
                    Enumerable.Range(1, nums).Select(i => r.Next(1, ubnd)).ToList();

            List<int> keysToAdd = getRandoms(randBound, int.MaxValue).ToList();
            List<int> valsToAdd = getRandoms(randBound, int.MaxValue).ToList();
            //Enumerable.Range(1, numToTest).OrderBy(n => r.Next()).ToList();
            //List<int> valsToAdd = Enumerable.Range(1, numToTest).ToList().OrderBy(n => r.Next()).ToList();

            List<Tuple<double, double, int, int>> results = new List<Tuple<double, double, int, int>>(numToTest);
            int x = 0;
            int lastCount = 0;
            while (ht.Count < numToTest)
            //for (int i = 0; i < numToTest; i++)
            {
                int key = keysToAdd[++x];
                int val = valsToAdd[x];
                //Perform garbage collection and wait until it finishes
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //time
                _stopwatch.Reset();
                _stopwatch.Start();
                ht.Add(key, val);
                _stopwatch.Stop();

                //results
                double microSecs = _stopwatch.Elapsed.TotalMilliseconds * 1000.0;
                if (x % 99 == 0)
                {

                    Console.WriteLine("Add ({0}, {1}): = {2:F1} μs / Count = {3}/{4} , LF = {5}", key, val, microSecs, ht.Count, ht.TableSize, ht.LoadFactor);
                }

                if (lastCount != ht.Count)
                {
                    results.Add(Tuple.Create(ht.LoadFactor, microSecs, ht.Count, ht.LastCollisions));
                    lastCount = ht.Count;
                }
            }


            int seq = 0;
            IEnumerable<string> resultsStr =
                from t in results
                select new { index = ++seq, val = t }
                    into wInd
                    group wInd by wInd.index / 10
                        into grpd
                        select new {
                            lf = grpd.Max(e => e.val.Item1),
                            time = grpd.Sum(e => e.val.Item2),
                            cnt = grpd.Max(e => e.val.Item3),
                            collisions = grpd.Sum(e => e.val.Item4)
                        } into summed
                        select summed.lf + "," + summed.time + "," + summed.cnt + "," + summed.collisions;

            Console.Write("File name? ");
            File.WriteAllLines(Console.ReadLine() + ".csv", resultsStr);



            //Console.WriteLine("How many random nums would you like to add? ");
            //int numToAdd = int.Parse(Console.ReadLine());

            ////stream of random numbers:
            //Random r = new Random();
            //Func<int, int, List<int>> getRandoms =
            //    (nums, ubnd) =>
            //        Enumerable.Range(1, nums).Select(i => r.Next(1, ubnd)).ToList();

            ////List<int> randomKeys = getRandoms(numToAdd, 1000000);
            //List<int> randomKeys = Enumerable.Range(1000000, numToAdd).ToList();
            //List<int> seqValues = Enumerable.Range(1, numToAdd).ToList();

            //IEnumerable<Tuple<int, int>> keyValPairs = Enumerable.Zip(randomKeys, seqValues, Tuple.Create);

            //Console.WriteLine("Table size = " + ht.TableSize);

            //Console.WriteLine("Adding random key value pairs..");
            //foreach(Tuple<int, int> keyVal in keyValPairs) {
            //    ht.Add(keyVal.Item1, keyVal.Item2);
            //}

            //int[][] customAdd = {
            //    new[] {1179, 120},
            //    new[] {9702, 121},
            //    new[] {7183, 122},
            //    new[] {50184, 123},
            //    new[] {4235, 124},
            //    new[] {644, 125},
            //    new[] {77, 126},
            //    new[] {3077, 127},
            //    new[] {23477, 128},
            //    new[] {90777, 129}
            //};

            //Console.WriteLine("Adding and custom 10 additions:");

            //foreach(int[] ints in customAdd) {
            //    int key = ints[0];
            //    int val = ints[1];

            //    //Perform garbage collection and wait until it finishes
            //    GC.Collect();
            //    GC.WaitForPendingFinalizers();

            //    //time
            //    _stopwatch.Reset();
            //    _stopwatch.Start();
            //    ht.Add(key,val);
            //    _stopwatch.Stop();

            //    //results
            //    double microSecs = _stopwatch.Elapsed.TotalMilliseconds * 1000.0;
            //    Console.WriteLine("Add ({0}, {1}): = {2:F1} μs / Count = {3}/{4} , LF = {5}", key, val, microSecs,
            //    ht.Count, ht.TableSize, ht.LoadFactor);
            //}

            //Console.WriteLine("Getting values from keys: ");
            //Console.WriteLine("HashTable[{0}] = {1}", 50184, ht[50184]);
            //Console.WriteLine("HashTable[{0}] = {1}", 77, ht[77]);

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);
        }



    }
}
