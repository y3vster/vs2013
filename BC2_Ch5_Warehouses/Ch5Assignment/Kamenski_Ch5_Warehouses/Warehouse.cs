﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch5Assignment {
    class Warehouse {
        private string whName;  //warehouse name
        private WhItem radios;  //Radios
        private WhItem telev;   //Televisions
        private WhItem comp;    //Computers
        
        /// <summary> RemoveStock method return value </summary>
        public enum EnumERROR {ERROR_SUCCESS = 0, ERROR_INSUFF_QTY = 1} 
        
        /// <summary>Item type used by AddStock / RemoveStock methods</summary>
        public enum EnumStockItem {EnumRadio, EnumTelev, EnumComp }
        
        /// <summary>internal representation of the stock items</summary>
        private sealed class WhItem {
            internal int Qty = 0;
            internal void Add(int addQty) { Qty += addQty; }
            internal EnumERROR TryRemove(int remQty) {
                if(Qty < remQty) 
                    return EnumERROR.ERROR_INSUFF_QTY;
                Qty -= remQty;
                return EnumERROR.ERROR_SUCCESS;
            }
        }

        /// <summary> empty warehouse ctor  </summary>
        public Warehouse(string whName) {
            this.whName = whName;
            radios = new WhItem();
            telev = new WhItem();
            comp = new WhItem();
        }

        /// <summary> ctor w/init vals </summary>
        public Warehouse(string whName,
                         int radiosQty,
                         int televQty,
                         int compQty) : this(whName)  {
            radios.Add(radiosQty);
            telev.Add(televQty);
            comp.Add(compQty);
        }

        /// <summary> Add items of specified type  </summary>
        public void AddStock(EnumStockItem addType, int addQty) {
            getItemInstance(addType).Add(addQty);
        }

        /// <summary> Remove items of specified type and receive status indicator (success/fail) </summary>
        public EnumERROR RemoveStock(EnumStockItem remType, int remQty) {
            return getItemInstance(remType).TryRemove(remQty);
        }

        /// <summary>  Warehouse contents </summary>
        public override string ToString(){
            return String.Format(@"
'{0}' contents:
  > radios:      {1}
  > televisions: {2}
  > computers:   {3}
", whName, radios.Qty, telev.Qty, comp.Qty);
        }

        /// <summary> map client item request to warehouse item instance  </summary>
        private WhItem getItemInstance(EnumStockItem instType) {
            switch(instType) {
                case EnumStockItem.EnumRadio:
                    return radios;
                case EnumStockItem.EnumTelev:
                    return telev;
                case EnumStockItem.EnumComp:
                    return comp;
                default:
                    throw new NotImplementedException("this item type is not supported");
            }
        }

    }
}
