﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch5Assignment {
    class ProgramEntry {
        static void Main(string[] args) {
            ResizeConsoleWindow();

            Warehouse whA = new Warehouse("Amazon");    //start w/empty wh
            Warehouse whW = new Warehouse("Wallmart",
                                          radiosQty: 2,
                                          televQty: 4,
                                          compQty: 7);

            Console.WriteLine("### Created warehouses:");
            Console.WriteLine(whA);
            Console.WriteLine(whW);

            //define some item types for readability
            var radio = Warehouse.EnumStockItem.EnumRadio;
            var tv = Warehouse.EnumStockItem.EnumTelev;
            var computer = Warehouse.EnumStockItem.EnumComp;

            //add some items to amazon
            whA.AddStock(radio, 1);
            whA.AddStock(tv, 2);
            whA.AddStock(computer, 3);

            //add some items to wallmart
            whW.AddStock(radio, 2);
            whW.AddStock(tv, 2);

            Console.WriteLine("### Added some items to each warehouse, new contents:");
            Console.WriteLine(whA);
            Console.WriteLine(whW);

            //try to remove some items
            Console.WriteLine("### Try to remove some items from each warehouse:");
            
            //staus code returned by warehouse
            Warehouse.EnumERROR statCode = whW.RemoveStock(tv, 5);
            Console.WriteLine("Remove 5 TVs from Wallmart.. {0}", statCode);
            
            statCode = whA.RemoveStock(radio, 2);
            Console.WriteLine("Remove 2 radios from Amazon.. {0}\n", statCode);

            Console.WriteLine("### Contents after removal:");
            Console.WriteLine(whA);
            Console.WriteLine(whW);

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);
        }

        ///returns a string representation of the warehouse error indicator
        static string StatCodeToString(Warehouse.EnumERROR sc) {
            switch(sc) {
                case Warehouse.EnumERROR.ERROR_SUCCESS:
                    return "Success";
                case Warehouse.EnumERROR.ERROR_INSUFF_QTY:
                    return "Insuff Qty";
                default:
                    throw new NotImplementedException("Unknown status code");
            }
        }

        static void ResizeConsoleWindow() {
            Console.SetBufferSize(80, 80);

            int wantedW = 80; //avoid an out of bounds exception instead of catching it
            int wantedH = 50;
            wantedW = (Console.LargestWindowWidth < wantedW) ? Console.LargestWindowWidth : wantedW;
            wantedH = (Console.LargestWindowHeight < wantedH) ? Console.LargestWindowHeight : wantedH;

            Console.SetWindowSize(wantedW, wantedH);
        }
    }
}
