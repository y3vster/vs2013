﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

//Tests every function in the implementation, including given and done functions. Be clear about what you're testing and prove that functions work by displaying the tree results.

//------------------------------------------------------------------------------

namespace BC3_Lab8_BST {
    class Program {
        const int BUF_W = 150;
        const int BUF_H = 900;
        const int WIND_W = 80;
        const int WIND_H = 40;


        private static void initConsoleWindow()
        {
            try {
                Console.SetWindowSize(1, 1);
                Console.SetBufferSize(BUF_W, BUF_H);
                int w = (Console.LargestWindowWidth < WIND_W) ? Console.LargestWindowWidth : WIND_W;
                int h = (Console.LargestWindowHeight < WIND_H) ? Console.LargestWindowHeight : WIND_H;
                Console.SetWindowSize(w, h);
            } catch(Exception e) {
                Debug.WriteLine("Error initializing console window size: " + e.ToString());
            }
        }

        static void DispRedBlkTree(BST<int> bst)
        {

            Console.WriteLine();
            var redBlkStrings = Regex.Matches(
                                    bst.ToEncodedString(),
                                    @"[\x7F-\xFF]+|[\x00-\x7F]+",
                                    RegexOptions.Multiline);

            ConsoleColor defaultClr = Console.ForegroundColor;

            foreach(Match m in redBlkStrings) {
                string val = m.Value;
                bool isRed = val[0] > 0x7F;
                if(isRed) {
                    val = new string(val.Select(c => (char)(c ^ 1 << 7)).ToArray());
                    Console.ForegroundColor = ConsoleColor.Red;
                } else {
                    Console.ForegroundColor = defaultClr;
                }
                Console.Write(val);
            }
            Console.ForegroundColor = defaultClr;
            Console.WriteLine("\n");
        }

        static void Main(string[] args)
        {

            initConsoleWindow();

            Console.WriteLine("Creating a BST..");
            BST<int> BST = new BST<int>();

            Console.WriteLine("BST.Empty(): " + BST.Empty());

            //stream of random numbers:
            Random r = new Random();
            Func<int, IList<int>> getRandoms = nums => Enumerable.Range(1, nums).Select(i => r.Next(1, 100)).ToList();

            //get some randoms
            IList<int> randNums = getRandoms(36);

            Console.WriteLine("Inserting values: " + string.Join(" ", randNums));
            foreach(int val in randNums) {
                BST.Add(val);
            }

            Console.WriteLine("Current BST state: ");
            DispRedBlkTree(BST);

            Console.WriteLine("BST.Empty(): " + BST.Empty());

            Console.WriteLine("BST.NumNodes(): " + BST.NumNodes());

            Console.WriteLine("BST.NumLeafNodes():" + BST.NumLeafNodes());

            Console.WriteLine("BST.GetHeight(): " + BST.GetHeight());

            //search for some numbers that should be in the tree
            List<int> shouldContain = randNums.OrderBy(e => r.Next()).Take(3).ToList();

            Console.WriteLine("\nNumbers that should be in the tree: ");
            foreach(int i in shouldContain) {
                Console.WriteLine("Contains({0}): {1} ", i, BST.Contains(i));
            }

            //should not be in the tree
            Console.WriteLine("\nShould not be in the tree: ");
            foreach(int i in (new[] { -3, 0, 101 })) {
                Console.WriteLine("Contains({0}): {1} ", i, BST.Contains(i));
            }
            Console.WriteLine();

            Console.Write("Loop with enumerator:");
            foreach(int i in BST) {
                Console.Write("{0,3}", i);
            }
            Console.WriteLine();

            //appy this function to BST elements when walking
            Action<int> dispElement = e => Console.Write(e.ToString().PadLeft(3));

            Console.Write("\nTree walk IN order:  ");
            BST.WalkInOrder(dispElement);
            Console.WriteLine();

            Console.Write("\nTree walk PRE order: ");
            BST.WalkPreOrder(dispElement);
            Console.WriteLine();


            Console.Write("\nTree walk POST order:");
            BST.WalkPostOrder(dispElement);
            Console.WriteLine();

            //get some numbers that are in the tree and some that are not
            List<int> testNums = randNums.OrderBy(e => r.Next()).Take(5).ToList();
            testNums.Add(100);
            Console.Write("\nTest ancestors:");
            foreach(int i in testNums) {
                Console.Write("\nBST.WalkAncestors({0})= ", i);
                Console.Write(" , returns " + BST.WalkAncestors(i, dispElement));
            }

            Console.WriteLine("\n\nTest Level:");
            foreach(int i in testNums) {
                Console.WriteLine("Level of {0} = {1}", i, BST.Level(i));
            }

            List<int> deleteElements = randNums.OrderBy(e => r.Next()).Take(18).ToList();
            Console.WriteLine("\nDeleting elements: " + string.Join(" ", deleteElements));

            foreach(int i in deleteElements) {
                BST.Delete(i);
            }

            Console.WriteLine("Current BST state: ");
            DispRedBlkTree(BST);

            Console.WriteLine("Contents: " + string.Join(" ", BST));

            Console.WriteLine("BST.Empty(): " + BST.Empty());

            Console.WriteLine("BST.NumNodes(): " + BST.NumNodes());

            Console.WriteLine("BST.NumLeafNodes():" + BST.NumLeafNodes());

            Console.WriteLine("BST.GetHeight(): " + BST.GetHeight());




            Console.WriteLine("\n\nPress any key to Exit..");
            Console.ReadKey(true);




        }
    }
}
