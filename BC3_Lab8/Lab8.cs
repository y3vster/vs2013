﻿// -------------------------------------------------- 
// Q1:
// for all n > 4:
// T(n) = 0.5 n^3 - 0.5 n^2 - 6n

// -------------------------------------------------- 
// Q2:
//      n       T(n)     f(n)     c
//      10  3.90E+02 1.00E+03  0.3900
//      50  6.10E+04 1.25E+05  0.4876
//     100  4.94E+05 1.00E+06  0.4944
//     500  6.24E+07 1.25E+08  0.4990
//    1000  4.99E+08 1.00E+09  0.4995
//    5000  6.25E+10 1.25E+11  0.4999
//   10000  5.00E+11 1.00E+12  0.4999

// -------------------------------------------------- 
// Q3:
// Yes, c stabilizes to 0.5 for large n

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace BC3_Lab8
{
    class Lab8
    {
        static void foo(int n, int val)
        {
            int b = 1, c = 0;
            for (int j = 4; j < n; j++)
            {
                for (int i = 0; i < j; i++)
                {
                    b = b * val;
                    for (int k = 0; k < n; ++k)
                        c = b + c;
                }
            }
        }

        private static Stopwatch _stopwatch;    //used for timing foo()

        static void Main(string[] args)
        {
            InitStopwatch();

            Console.Write("Enter a range (e.g. 10 2 20 means from 10 to 20 step 2): ");
            string rangeStr = Console.ReadLine();
            MatchCollection rangeNums = Regex.Matches(rangeStr, @"\d+");
            int from = int.Parse(rangeNums[0].Value);
            int step = int.Parse(rangeNums[1].Value);
            int to = int.Parse(rangeNums[2].Value);
            int numEntries = (to - from) / step + 1;

            Console.Write("How many tries for each step? ");
            int numTries = int.Parse(Console.ReadLine());

            //each n entry has a list of multiple time trials
            List<int> nRecs = new List<int>(numEntries);
            List<List<double>> tRecs = new List<List<double>>(numEntries);

            //The first call is much slower than others, so discard it
            double discardedTime = GetFooMicroseconds(10);

            for (int n = from; n <= to; n += step)
            {
                nRecs.Add(n);
                List<double> times = new List<double>(numTries);
                for (int j = 0; j < numTries; j++)
                {
                    times.Add(GetFooMicroseconds(n));
                }
                tRecs.Add(times);
                Console.WriteLine("N: {0,5} = {1:F1} μs (average)", n, times.Average());
            }

            Console.Write("Write to .CSV? (Y/N) ");
            if ("y" == Console.ReadLine().ToLower())
            {
                //wow, functional programming is the best! 
                IEnumerable<string> results =
                    from tuple in Enumerable.Zip(nRecs, tRecs, Tuple.Create)
                    let n = tuple.Item1
                    from t in tuple.Item2
                    select n + ", " + t;

                Console.Write("File name? ");
                File.WriteAllLines(Console.ReadLine() + ".csv", results);
            }

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey(true);
        }

        private static void InitStopwatch()
        {
            _stopwatch = new Stopwatch();
            double muSecPerTick = 1000000.0 / (double)Stopwatch.Frequency; //Freq is ticks/sec
            Console.WriteLine("Stopwatch resolution is ~ {0:F2} μs", muSecPerTick);
            //use second core for the test
            Process.GetCurrentProcess().ProcessorAffinity = new IntPtr(2);
            //prevent "Normal" processes and threads from forcing me into a context switch mid-test
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
        }

        private static double GetFooMicroseconds(int n)
        {
            //wait for CLR to get all caught up
            GC.Collect();
            GC.WaitForPendingFinalizers();

            _stopwatch.Reset();
            _stopwatch.Start();
            foo(n, 10);
            _stopwatch.Stop();

            return _stopwatch.Elapsed.TotalMilliseconds * 1000.0;
        }


    }
}
