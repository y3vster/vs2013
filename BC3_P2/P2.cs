﻿//Yevgeni Kamenski
//P2.cs
//9-1-2015

//Tests every function in the implementation, including given and done functions.
//Be clear about what you're testing and prove that functions work by displaying
//the tree results.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace BC3_P2 {
    class Program {

        const int BUF_W = 200;  //buffer width, height
        const int BUF_H = 150;

        const int WIND_W = 80;  //window width, height
        const int WIND_H = 75;

        static void Main(string[] args)
        {
            InitConsoleWindow();

            BST<int> bst = new BST<int>();

            //stream of random numbers:
            Random r = new Random();
            Func<int, List<int>> getRandoms = nums => Enumerable.Range(1, nums).Select(i => r.Next(1, 100)).ToList();

            //get some randoms
            List<int> randNums = getRandoms(75);

            foreach(int val in randNums) {
                bst.Add(val);
                Console.Clear();
                DisplayColorTree(bst);
                Thread.Sleep(60);
            }

            Console.WriteLine("Inserted values: " + string.Join(" ", randNums));

            Console.WriteLine("BST.Empty(): " + bst.Empty());

            Console.WriteLine("BST.NumNodes(): " + bst.NumNodes());

            Console.WriteLine("BST.NumLeafNodes():" + bst.NumLeafNodes());

            Console.WriteLine("BST.GetHeight(): " + bst.GetHeight());

            Console.WriteLine("BST.GetWidth(): " + bst.GetWidth());

            //search for some numbers that should be in the tree
            List<int> shouldContain = randNums.OrderBy(e => r.Next()).Take(3).ToList();

            Console.WriteLine("\nNumbers that should be in the tree: ");
            foreach(int i in shouldContain) {
                Console.WriteLine("Contains({0}): {1} ", i, bst.Contains(i));
            }

            //should not be in the tree
            Console.WriteLine("\nShould not be in the tree: ");
            foreach(int i in (new[] { -3, 0, 101 })) {
                Console.WriteLine("Contains({0}): {1} ", i, bst.Contains(i));
            }
            Console.WriteLine();

            Console.Write("Loop with enumerator:");
            foreach(int i in bst) {
                Console.Write("{0,3}", i);
            }
            Console.WriteLine();

            //appy this function to BST elements when walking
            Action<int> dispElement = e => Console.Write(e.ToString().PadLeft(3));

            Console.Write("\nTree walk IN order:  ");
            bst.WalkInOrder(dispElement);
            Console.WriteLine();

            Console.Write("\nTree walk PRE order: ");
            bst.WalkPreOrder(dispElement);
            Console.WriteLine();

            Console.Write("\nTree walk POST order:");
            bst.WalkPostOrder(dispElement);
            Console.WriteLine();

            Console.Write("\nTree walk LEVEL ordr:");
            bst.WalkLevelOrder(dispElement);
            Console.WriteLine();

            //get some numbers that are in the tree and some that are not
            List<int> testNums = randNums.OrderBy(e => r.Next()).Take(5).ToList();
            testNums.Add(100);

            Console.Write("\nTest ancestors:");
            foreach(int i in testNums) {
                Console.Write("\nBST.WalkAncestors({0})= ", i);
                Console.Write(" , returns " + bst.WalkAncestors(i, dispElement));
            }

            Console.WriteLine("\n\nTest Level:");
            foreach(int i in testNums) {
                Console.WriteLine("Level of {0} = {1}", i, bst.Level(i));
            }

            List<int> deleteElements = randNums.OrderBy(e => r.Next()).Take(66).ToList();
            Console.WriteLine("\nDeleting elements: " + string.Join(" ", deleteElements));

            foreach(int i in deleteElements) {
                bst.Delete(i);
            }

            Console.WriteLine("Current BST state: ");
            DisplayColorTree(bst);

            Console.WriteLine("Contents: " + string.Join(" ", bst));

            Console.WriteLine("BST.Empty(): " + bst.Empty());

            Console.WriteLine("BST.NumNodes(): " + bst.NumNodes());

            Console.WriteLine("BST.NumLeafNodes():" + bst.NumLeafNodes());

            Console.WriteLine("BST.GetHeight(): " + bst.GetHeight());

            Console.WriteLine("BST.GetWidth(): " + bst.GetWidth());

            Console.WriteLine("\n\nPress any key to Exit..");
            Console.ReadKey(true);
        }


        static void DisplayColorTree(BST<int> bst)
        {
            Console.WriteLine();
            var redBlkStrings = Regex.Matches(
                                    bst.ToEncodedString(),
                                    @"[\x7F-\xFF]+|[\x00-\x7F]+",
                                    RegexOptions.Multiline);

            ConsoleColor defaultClr = Console.ForegroundColor;

            foreach(Match m in redBlkStrings) {
                string val = m.Value;
                bool isRed = val[0] > 0x7F;
                if(isRed) {
                    //reset high bit
                    val = new string(val.Select(c => (char)(c & 0x7F)).ToArray());
                    Console.ForegroundColor = ConsoleColor.Red;
                } else {
                    Console.ForegroundColor = defaultClr;
                }
                Console.Write(val);
            }

            Console.ForegroundColor = defaultClr;
            Console.WriteLine("\n");
        }


        //init console
        private static void InitConsoleWindow()
        {
            try {
                Console.SetWindowSize(1, 1);
                Console.SetBufferSize(BUF_W, BUF_H);
                int w = (Console.LargestWindowWidth < WIND_W) ? Console.LargestWindowWidth : WIND_W;
                int h = (Console.LargestWindowHeight < WIND_H) ? Console.LargestWindowHeight : WIND_H;
                Console.SetWindowSize(w, h);
            } catch(Exception e) {
                Debug.WriteLine("Error initializing console window size: " + e);
            }
        }
    }
}
