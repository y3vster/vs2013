﻿//Yevgeni Kamenski
//BST.cs
//8-28-2015

//Implement constructor and Empty() 5 pts
//Implement NumNodes() 15 pts
//Implement NumLeafNodes() 15 pts
//Implement GetHeight() 15 pts
//Implement Level() 15 pts
//Implement WalkAncestors() 15 pts
//Implement all GIVEN and DONE functions 5 pts

//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BC3_P2 {


    /// <summary>Red-black Binary Search Tree Implementation. </summary>
    [DebuggerDisplay("{Count} items")]
    class BST<T> : IEnumerable<T> where T : IComparable<T> {

        private Node _root;  //root of BST

        //------------------------------------------------------------
        // public methods:


        /// <summary>Default Ctor.</summary>
        public BST() { Count = 0; }


        /// <summary>Number of elements in a tree. </summary>
        public int Count { get; private set; }


        /// <summary>Determines if BST is empty; returns true if so, else false</summary>
        public bool Empty() { return _root == null; }


        /// <summary>Insert a value into the tree. </summary>
        public void Add(T val)
        {
            _root = Add(_root, val);
            _root.IsRed = false;
        }
        public Node Add(Node current, T val)
        {
            if(current == null) {
                Count++;
                return new Node(val);
            }

            int compare = val.CompareTo(current.Value);
            if(compare == 0) return current;
            if(compare < 0)
                current.Left = Add(current.Left, val);
            else
                current.Right = Add(current.Right, val);

            //following Sedgewick's Algorithms p438 state diagr

            //red link should be on left by definition
            if(IsBlack(current.Left) && IsRed(current.Right))
                current = LeftRotate(current);

            //might have 2 red left links from last rot or addition
            if(IsRed(current.Left) && IsRed(current.Left.Left))
                current = RightRotate(current);

            // split a 4-node
            if(Has2Red(current))
                FlipColors(current);

            return current;
        }


        /// <summary>Returns true if value is in the tree.</summary>
        public bool Contains(T val)
        {
            Node current = _root;
            while(current != null) {
                int comp = val.CompareTo(current.Value);
                if(comp == 0) return true;  //found
                current = (comp < 0) ? current.Left : current.Right;
            }
            return false;   //not found 
        }


        /// <summary>Removes a value. </summary>
        public void Delete(T val)
        {
            //root and children are 2-nodes, so merge them into a 4-node
            if(Has2Blk(_root))
                _root.IsRed = true;

            _root = Delete(_root, val);

            //change the root back to blk
            if(IsRed(_root))
                _root.IsRed = false;
        }
        private Node Delete(Node current, T val)
        {
            if(current == null) return null;

            int compare = val.CompareTo(current.Value);
            if(compare < 0) {
                if(current.Left != null &&
                    current.Right != null &&
                    IsBlack(current.Left) &&
                    IsBlack(current.Left.Left)) {

                    FlipColors(current);

                    //borrow a red from the right subtree if there is one 
                    if(current.Right != null &&
                        IsRed(current.Right.Left)) {
                        current.Right = RightRotate(current.Right);
                        current = LeftRotate(current);
                    }
                }

                //keep searching after leaving red behind
                current.Left = Delete(current.Left, val);
            } else {
                //is there red to borrow on the left?
                if(IsRed(current.Left))
                    current = RightRotate(current);

                if(val.CompareTo(current.Value) == 0 &&
                    current.Right == null) {
                    return null;
                }

                if(current.Right != null &&
                    current.Left != null &&
                    IsBlack(current.Right) &&
                    IsBlack(current.Right.Left)) {

                    FlipColors(current);
                    //do we now have red to borrow on the left?
                    if(current.Left != null &&
                        IsRed(current.Left.Left)) {
                        current = RightRotate(current);
                    }
                }

                if(val.CompareTo(current.Value) == 0) {
                    Node min = Min(current.Right);
                    if(min != null) {
                        current.Value = min.Value;
                        current.Right = Delete(current.Right, min.Value);
                    }
                } else {
                    current.Right = Delete(current.Right, val);
                }

            }

            //red link should be on left by definition
            if(IsRed(current.Right))
                current = LeftRotate(current);

            //might have 2 red left links from last rot or addition
            if(IsRed(current.Left) && IsRed(current.Left.Left))
                current = RightRotate(current);

            // split a 4-node
            if(Has2Red(current))
                FlipColors(current);

            return current;
        }


        /// <summary>Counts and returns the number of nodes</summary>
        public int NumNodes()
        {
            //there is a Count property, but alternatively, for some fun:

            int num = 0;
            ApplyInOrder(n => num++, _root);
            return num;
        }


        /// <summary>Counts and returns the number of leaf nodes</summary>
        public int NumLeafNodes()
        {
            int num = 0;
            Action<Node> accumulator = n => {
                if(n.Left == null && n.Right == null) num++;
            };
            ApplyInOrder(accumulator, _root);
            return num;
        }


        /// <summary>Returns height of the tree; the height is the number of levels it contains</summary>
        public int GetHeight()
        {
            Func<Node, int> height = null;
            height = n => n == null ? 0 : 1 + Math.Max(height(n.Left), height(n.Right));
            return height(_root);
        }


        /// <summary>Determines and returns the level of an given item in the tree; if the item does not exist on the tree, then return -1</summary>
        public int Level(T val)
        {
            Node n = _root;
            for(int level = 0; n != null; level++) {
                int cmp = val.CompareTo(n.Value);
                if(cmp == 0) return level;
                n = (cmp < 0) ? n.Left : n.Right;
            }
            return -1; // not found
        }


        /// <summary>Apply a function to the node values PRE order.</summary>
        public void WalkPreOrder(Action<T> func)
        {
            WalkPreOrder(_root, func);
        }
        private void WalkPreOrder(Node n, Action<T> func)
        {
            if(n != null) {
                func(n.Value);
                WalkPreOrder(n.Left, func);
                WalkPreOrder(n.Right, func);
            }
        }


        /// <summary>Apply a function to the node value IN order.</summary>
        public void WalkInOrder(Action<T> func)
        {
            WalkInOrder(_root, func);
        }
        private void WalkInOrder(Node n, Action<T> func)
        {
            if(n != null) {
                WalkInOrder(n.Left, func);
                func(n.Value);
                WalkInOrder(n.Right, func);
            }
        }


        /// <summary>Apply a function to the node values POST order. </summary>
        public void WalkPostOrder(Action<T> func)
        {
            WalkPostOrder(_root, func);
        }
        private void WalkPostOrder(Node n, Action<T> func)
        {
            if(n != null) {
                WalkPostOrder(n.Left, func);
                WalkPostOrder(n.Right, func);
                func(n.Value);
            }
        }


        /// <summary>Applies a function to the ancestors of the node, if any
        /// found. Returns true if value was found & false otherwise.</summary>
        public bool WalkAncestors(T val, Action<T> func)
        {
            Stack<T> stack = new Stack<T>();

            Node n = _root;
            while(n != null) {
                int compare = val.CompareTo(n.Value);
                if(compare == 0) break; // found
                stack.Push(n.Value);
                n = (compare < 0) ? n.Left : n.Right;
            }

            if(n == null)
                return false;

            foreach(T value in stack)
                func(value);

            return true;
        }

        //==============================================================
        //Extra credit methods

        /// <summary>Applies a function to node values in level order. </summary>
        public void WalkLevelOrder(Action<T> func)
        {
            Queue<Node> queue = new Queue<Node>();
            Action<Node> enqueueNonNull = n => { if(n != null) queue.Enqueue(n); };
            enqueueNonNull(_root);
            while(queue.Count != 0) {
                Node n = queue.Dequeue();
                func(n.Value);
                enqueueNonNull(n.Left);
                enqueueNonNull(n.Right);
            }
        }

        /// <summary>Returns tree width. </summary>
        public int GetWidth()
        {
            List<int> levels = new List<int>();
            ApplyInOrder(n => levels.Add(Level(n.Value)), _root);
            IEnumerable<int> levelWidths = from l in levels
                                           group l by l
                                               into grp
                                               orderby grp.Count() descending
                                               select grp.Count();
            return levelWidths.First();
        }


        //===============================================================
        // Helper methods

        //true if node is red
        private static bool IsRed(Node n)
        {
            return n != null && n.IsRed;
        }

        //null nodes are black
        private static bool IsBlack(Node n)
        {
            return !IsRed(n);
        }

        //is it a 4-node?
        private static bool Has2Red(Node n)
        {
            return IsRed(n.Left) && IsRed(n.Right);
        }

        //does it have 2 blk children nodes?
        private static bool Has2Blk(Node n)
        {
            return n != null && IsBlack(n.Left) && IsBlack(n.Right);
        }

        //left rotation
        private static Node LeftRotate(Node n)
        {
            Node newRoot = n.Right;
            n.Right = newRoot.Left;
            newRoot.Left = n;
            newRoot.IsRed = n.IsRed;    //root's link color shouldn't change
            n.IsRed = true;
            return newRoot;
        }

        //right rotation
        private static Node RightRotate(Node n)
        {
            Node newRoot = n.Left;
            n.Left = newRoot.Right;
            newRoot.Right = n;
            newRoot.IsRed = n.IsRed;    //root's link color shouldn't change
            n.IsRed = true;
            return newRoot;
        }

        //flip colors of the node and its children
        private static void FlipColors(Node n)
        {
            n.IsRed = !n.IsRed;
            n.Left.IsRed = !n.Left.IsRed;
            n.Right.IsRed = !n.Right.IsRed;
        }

        //returns a minimum node of the subtree
        private static Node Min(Node n)
        {
            if(n == null) return null;
            return (n.Left == null) ? n : Min(n.Left);
        }

        //applies a function to the elements in-order
        private static void ApplyInOrder(Action<Node> func, Node node)
        {
            if(node != null) {
                ApplyInOrder(func, node.Left);
                func(node);
                ApplyInOrder(func, node.Right);
            }
        }

        //===============================================================
        // String representation


        /// <summary>String representation of the BST. </summary>
        public override string ToString()
        {
            //just remove the color information
            return new string(
                ToEncodedString().
                Select(c => (char)(c & 0x7F)).
                ToArray());
        }

        /// <summary>Returns a string representation of the BST
        /// Node color information is encoded by masking the H bit</summary>
        public string ToEncodedString()
        {
            StringBuilder sb = new StringBuilder();

            //sets the high bit of the char
            Func<string, string> makeRed = s => {
                string result = new string(s.AsEnumerable().Select(e => (char)(e | 1 << 7)).ToArray());
                return result;
            };

            //encodes the value with the red color bit if the node is red
            Func<Node, string> encodedVal = n => n.IsRed ? makeRed(n.Value.ToString()) : n.Value.ToString();

            Func<Node, string> nodeVal = n => (n != null) ? " " + encodedVal(n) + "" : " ";

            Func<int, int, string, string> nodeValPadded = (totLen, midPosn, val) => {
                sb.Clear();
                sb.Append(' ', totLen - val.Length);
                int insertPosn = midPosn - val.Length / 2;
                if(insertPosn < 0) insertPosn = 0;
                if(totLen - insertPosn - val.Length < 0) insertPosn = totLen - val.Length;
                sb.Insert(insertPosn, val);
                return sb.ToString();
            };

            Func<Node, bool> isLeaf = n => (n == null) || (n.Left == null && n.Right == null);

            Func<List<string>, List<string>, IList<string>> merge =
                (left, right) => {
                    string lpad = new string(' ', left[0].Length);
                    string rpad = new string(' ', right[0].Length);

                    int addNum = left.Count - right.Count;

                    if(addNum > 0)
                        right.AddRange(Enumerable.Repeat(rpad, addNum));
                    else
                        left.AddRange(Enumerable.Repeat(lpad, -addNum));

                    return Enumerable.Zip(left, right, (l, r) => l + r).ToList();
                };

            Func<Node, List<string>> childrenStr = null;
            childrenStr =
                n => {
                    bool leaf = isLeaf(n);
                    string topNodeStr = nodeVal(n);

                    int totWid, nodePosn;
                    List<String> mergedChildren = null;

                    if(leaf) {
                        totWid = topNodeStr.Length;
                        nodePosn = totWid / 2;
                    } else {
                        List<string> lTree = childrenStr(n.Left);
                        List<string> rTree = childrenStr(n.Right);
                        mergedChildren = merge(lTree, rTree).ToList();
                        int lWid = lTree[0].Length;
                        int rWid = rTree[0].Length;
                        totWid = mergedChildren[0].Length;
                        nodePosn = (3 * lWid + rWid) / 4 - 1;
                        topNodeStr = nodeValPadded(totWid, nodePosn, topNodeStr);
                    }

                    List<string> result = new List<string>();
                    result.Add(topNodeStr);

                    if(!leaf) {
                        string pointers = n.Left != null ? (n.Right != null ? @" /\" : @" / ") : @"  \";
                        result.Add(nodeValPadded(totWid, nodePosn, pointers));
                        result.AddRange(mergedChildren);
                    }

                    return result;
                };

            return string.Join("\n", childrenStr(_root));
        }


        //======================================================================
        //Node class implementation

        /// <summary>Node representation</summary>
        [DebuggerDisplay("{Value}  :  {_debugStr,nq}")]
        internal class Node {

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public T Value;

            public Node Left;
            public Node Right;

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public bool IsRed = true; //Red by default

            /// <summary>Ctor</summary>
            public Node(T value) { Value = value; }

#if DEBUG
            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private string _debugStr
            {
                get { return (IsRed ? "Red " : "Blk ") + (Left == null && Right == null ? "Leaf" : "Node"); }
            }
#endif
        }


        //===============================================================
        // Enumerator interface implementation

        /// <summary>Enumerable interface implementation.</summary>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>Enumerable interface implementation.</summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>BST enumerator implementation</summary>
        internal struct Enumerator : IEnumerator<T> {
            private BST<T> _tree;
            private Node _current;
            private Stack<Node> _stack;
            internal Enumerator(BST<T> bst)
            {
                _tree = bst;
                _stack = new Stack<Node>();
                _current = null;
                InitialState();
            }

            private void InitialState()
            {
                _current = null;
                _stack.Clear();
                //find minimum
                Node n = _tree._root;
                while(n != null) {
                    _stack.Push(n);
                    n = n.Left;
                }
            }

            T IEnumerator<T>.Current
            {
                get
                {
                    if(_current == null) {
                        throw new InvalidOperationException();
                    }
                    return _current.Value;
                }
            }

            void IDisposable.Dispose() { }  //no resources to release

            object IEnumerator.Current
            {
                get
                {
                    if(_current == null) {
                        throw new InvalidOperationException();
                    }
                    return _current.Value;
                }
            }

            bool IEnumerator.MoveNext()
            {
                if(_stack.Count == 0) {
                    _current = null;
                } else {
                    _current = _stack.Pop();
                }

                if(_current == null) {
                    return false;
                }

                //next is the min of the right subtree
                Node n = _current.Right;
                while(n != null) {
                    _stack.Push(n);
                    n = n.Left;
                }
                return true;
            }

            void IEnumerator.Reset() { InitialState(); }
        }
    }
}